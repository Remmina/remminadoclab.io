var struct___remmina_secret_plugin =
[
    [ "delete_password", "struct___remmina_secret_plugin.html#a5c61bfd8ead834bcab75c643514171ef", null ],
    [ "description", "struct___remmina_secret_plugin.html#a175d83898486e37aadd16279f559f659", null ],
    [ "domain", "struct___remmina_secret_plugin.html#a81ff30e7efe61fd0057184640baf545e", null ],
    [ "get_password", "struct___remmina_secret_plugin.html#aaa4bb101e0da125111dedf732a34b629", null ],
    [ "init", "struct___remmina_secret_plugin.html#ad433f24a09d962e66a336eb461cecd56", null ],
    [ "init_order", "struct___remmina_secret_plugin.html#a0155c6684591e0dd843c7145792fef09", null ],
    [ "is_service_available", "struct___remmina_secret_plugin.html#a519a411cb2091da2176258d87bcea6d4", null ],
    [ "name", "struct___remmina_secret_plugin.html#a197b28d8b71ccb0c91ce8bf27690968a", null ],
    [ "store_password", "struct___remmina_secret_plugin.html#a5a925730ef351cc44ddeebad6415c2e6", null ],
    [ "type", "struct___remmina_secret_plugin.html#a3e49bae6984c9bfd714ea8c664ce33a2", null ],
    [ "version", "struct___remmina_secret_plugin.html#ac6927c92267fdb786b01319aeade0daa", null ]
];