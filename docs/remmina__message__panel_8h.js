var remmina__message__panel_8h =
[
    [ "RemminaMessagePanelCallback", "remmina__message__panel_8h.html#ae2923c94c1caf1ac4a7eb48361d2670a", null ],
    [ "REMMINA_MESSAGE_PANEL_USERNAME", "remmina__message__panel_8h.html#ae4d5251432e1a9e6803c0240cc492e18a5c66a04852163126a3b494abe3e07e25", null ],
    [ "REMMINA_MESSAGE_PANEL_PASSWORD", "remmina__message__panel_8h.html#ae4d5251432e1a9e6803c0240cc492e18a1d61170572fba5e968305c2ea3b0f9f6", null ],
    [ "REMMINA_MESSAGE_PANEL_DOMAIN", "remmina__message__panel_8h.html#ae4d5251432e1a9e6803c0240cc492e18a9efe2bb3f139ed0baf3833880fc2369a", null ],
    [ "REMMINA_MESSAGE_PANEL_SAVEPASSWORD", "remmina__message__panel_8h.html#ae4d5251432e1a9e6803c0240cc492e18aa20e0ac03321f7742184c8228b3a8f2e", null ],
    [ "REMMINA_MESSAGE_PANEL_BUTTONTOFOCUS", "remmina__message__panel_8h.html#ae4d5251432e1a9e6803c0240cc492e18a9439963721ca9d03d1a2371161336725", null ],
    [ "REMMINA_MESSAGE_PANEL_CACERTFILE", "remmina__message__panel_8h.html#ae4d5251432e1a9e6803c0240cc492e18a10531307d859706670fb6ce4aa7308c7", null ],
    [ "REMMINA_MESSAGE_PANEL_CACRLFILE", "remmina__message__panel_8h.html#ae4d5251432e1a9e6803c0240cc492e18a703afb1dece981649f9497a9e910b779", null ],
    [ "REMMINA_MESSAGE_PANEL_CLIENTCERTFILE", "remmina__message__panel_8h.html#ae4d5251432e1a9e6803c0240cc492e18a26a6d6b3ac3f4e64662b0c72dc3982f7", null ],
    [ "REMMINA_MESSAGE_PANEL_CLIENTKEYFILE", "remmina__message__panel_8h.html#ae4d5251432e1a9e6803c0240cc492e18a34c1441f93d50321cbabcfb45918c6e3", null ],
    [ "REMMINA_MESSAGE_PANEL_MAXWIDGETID", "remmina__message__panel_8h.html#ae4d5251432e1a9e6803c0240cc492e18ae335afb8ce18f75a427da98b1d84f10a", null ],
    [ "G_DECLARE_DERIVABLE_TYPE", "remmina__message__panel_8h.html#a558c1459208a2a314734a1cf92e8e156", null ],
    [ "remmina_message_panel_field_get_filename", "remmina__message__panel_8h.html#ab89dba73bc6832c6c5061a0d8bd0fbb4", null ],
    [ "remmina_message_panel_field_get_string", "remmina__message__panel_8h.html#a44054d029d4615092ae8a2ee1bf81db4", null ],
    [ "remmina_message_panel_field_get_switch_state", "remmina__message__panel_8h.html#a80bc2fcb39c236da739593c2134c73fa", null ],
    [ "remmina_message_panel_field_set_filename", "remmina__message__panel_8h.html#aec9dcd96d0148fec9cdc9e94cc8dc1ff", null ],
    [ "remmina_message_panel_field_set_string", "remmina__message__panel_8h.html#a0e20944d484c95036eeb6b5d7775c901", null ],
    [ "remmina_message_panel_field_set_switch", "remmina__message__panel_8h.html#a67a48217d44350e028ed95b1b55f0b0f", null ],
    [ "remmina_message_panel_focus_auth_entry", "remmina__message__panel_8h.html#aacb1f47e5ad88086c1bda1b0a156c34d", null ],
    [ "remmina_message_panel_new", "remmina__message__panel_8h.html#adf4a7707360c836ba312a2c7286db294", null ],
    [ "remmina_message_panel_response", "remmina__message__panel_8h.html#af7db7b9f49fe6b83b17471116d363d40", null ],
    [ "remmina_message_panel_setup_auth", "remmina__message__panel_8h.html#ae2cec8bc8216154388727b7e35200667", null ],
    [ "remmina_message_panel_setup_auth_x509", "remmina__message__panel_8h.html#ac10f450f856ca7d9c0b6896261e9c407", null ],
    [ "remmina_message_panel_setup_message", "remmina__message__panel_8h.html#ae01d27ae9f678dc5a4fa32f0b401f434", null ],
    [ "remmina_message_panel_setup_progress", "remmina__message__panel_8h.html#afae4072b7d8b54392ec08da7d6ead620", null ],
    [ "remmina_message_panel_setup_question", "remmina__message__panel_8h.html#ae5b1825b5d4e56ecc2f25c28bb042c32", null ]
];