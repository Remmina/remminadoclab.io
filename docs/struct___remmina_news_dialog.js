var struct___remmina_news_dialog =
[
    [ "builder", "struct___remmina_news_dialog.html#a04d61af97919ea321ca384711edd2015", null ],
    [ "dialog", "struct___remmina_news_dialog.html#a652393d5775f6abb7a3b97e1d250797f", null ],
    [ "retval", "struct___remmina_news_dialog.html#aa44d2c89a170a509ebb082082efac813", null ],
    [ "rmnews_button_close", "struct___remmina_news_dialog.html#a24dd17da12072e324ce165a9873d59ad", null ],
    [ "rmnews_defaultcl_button", "struct___remmina_news_dialog.html#a5f1f924669db7285fac6087e08d6ce19", null ],
    [ "rmnews_defaultcl_label", "struct___remmina_news_dialog.html#a041c5c6f7776a3cdfd1fbcc700dfc749", null ],
    [ "rmnews_label", "struct___remmina_news_dialog.html#ae4b71b4e789e65063db67f51ce4feb26", null ],
    [ "rmnews_stats_switch", "struct___remmina_news_dialog.html#a68e9f8a20af9ff18a4446005042c35f7", null ],
    [ "rmnews_text_view", "struct___remmina_news_dialog.html#a94584de47018f068c242e5f527e3b84f", null ]
];