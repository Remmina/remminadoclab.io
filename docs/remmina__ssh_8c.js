var remmina__ssh_8c =
[
    [ "remmina_ssh_auth", "remmina__ssh_8c.html#aad84c6609cb431d48121ae5e0902441b", null ],
    [ "remmina_ssh_auth_agent", "remmina__ssh_8c.html#a6e57a1b5ba4a3886c4cb79ece8894796", null ],
    [ "remmina_ssh_auth_auto_pubkey", "remmina__ssh_8c.html#aa8aee72091b09cde34c0e0efadde719a", null ],
    [ "remmina_ssh_auth_gssapi", "remmina__ssh_8c.html#a0eb19f950b34b360b28243a0b1883d67", null ],
    [ "remmina_ssh_auth_interactive", "remmina__ssh_8c.html#a976d3c1a549c17f00a64eb05d4e0823c", null ],
    [ "remmina_ssh_auth_password", "remmina__ssh_8c.html#ad6f3f5c196175412049af0c440414e65", null ],
    [ "remmina_ssh_auth_pubkey", "remmina__ssh_8c.html#a553ee462228e3b6284297b5ab89c6156", null ],
    [ "remmina_ssh_find_identity", "remmina__ssh_8c.html#ab6d8bdad543b3caead2b2b0339484798", null ],
    [ "remmina_ssh_identity_path", "remmina__ssh_8c.html#a72a58e416c4eeb9d17eaf76360ebee42", null ],
    [ "remmina_ssh_set_application_error", "remmina__ssh_8c.html#a1f3297263703b4259c1c9607d710995d", null ],
    [ "remmina_ssh_set_error", "remmina__ssh_8c.html#abc556f0a4335083436f6e19c204fa553", null ],
    [ "common_identities", "remmina__ssh_8c.html#a8920b4663823da39661a108f04b33694", null ]
];