var struct___remmina_plugin_w_w_w_data =
[
    [ "authenticated", "struct___remmina_plugin_w_w_w_data.html#aaaf9e6e616dea9d366b151e575ddedcd", null ],
    [ "box", "struct___remmina_plugin_w_w_w_data.html#a6ff4ea7c6ceb2351c6248427cf83a4db", null ],
    [ "context", "struct___remmina_plugin_w_w_w_data.html#a22010474535695604f1e05498e726f3e", null ],
    [ "credentials", "struct___remmina_plugin_w_w_w_data.html#aa8ef80ab8bb8cae8d56890720dd58959", null ],
    [ "data_mgr", "struct___remmina_plugin_w_w_w_data.html#a9d54a3dc838528c104eaea91d4f777ab", null ],
    [ "document_type", "struct___remmina_plugin_w_w_w_data.html#aa3e07f92f32ee3bab1b0e33c33e619db", null ],
    [ "formauthenticated", "struct___remmina_plugin_w_w_w_data.html#a80b04541339086a453fad32295806f84", null ],
    [ "load_event", "struct___remmina_plugin_w_w_w_data.html#ab4e4fcc5e49437cb5c2c1d949aaa9436", null ],
    [ "request", "struct___remmina_plugin_w_w_w_data.html#ac7390cf88e14b015713ea1f0980909f0", null ],
    [ "settings", "struct___remmina_plugin_w_w_w_data.html#a3f740cb467c00cb8f17ff2546498fa54", null ],
    [ "url", "struct___remmina_plugin_w_w_w_data.html#a6d2341e2d7c157b0ff64835d1ade6fbb", null ],
    [ "webview", "struct___remmina_plugin_w_w_w_data.html#a32adefd1fb55dd7237b907923d2a3690", null ]
];