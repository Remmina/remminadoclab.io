var searchData=
[
  ['cacert',['cacert',['../struct___remmina_protocol_widget_priv.html#a2c5f3bd1f990157bef034920db49dc6e',1,'_RemminaProtocolWidgetPriv']]],
  ['cachedir',['cachedir',['../remmina__file__manager_8c.html#ace1098aad45256ac0b0155cb042472e9',1,'remmina_file_manager.c']]],
  ['cacrl',['cacrl',['../struct___remmina_protocol_widget_priv.html#af9db6b4306852214f3dc2cdc06a635b4',1,'_RemminaProtocolWidgetPriv']]],
  ['cairo_5fformat',['cairo_format',['../structrf__context.html#aa13bb4f72f3024d81e9051de4bd65ec0',1,'rf_context']]],
  ['call_5ffeature',['call_feature',['../struct___remmina_protocol_plugin.html#a4908abc6c70c59c42e2757aecaa25cf1',1,'_RemminaProtocolPlugin']]],
  ['callback',['callback',['../struct___remmina_s_s_h.html#aa8e75d0a635b3390100218bee6b49378',1,'_RemminaSSH']]],
  ['callback_5fdata',['callback_data',['../struct___remmina_s_s_h_tunnel.html#a6f2cd8a2535e180e7bd428e84030ffb6',1,'_RemminaSSHTunnel']]],
  ['called_5ffrom_5fsubthread',['called_from_subthread',['../structremmina__protocol__widget__dialog__mt__data__t.html#a81eb96409c1543bcf2d9474d4d89907f',1,'remmina_protocol_widget_dialog_mt_data_t']]],
  ['cancel',['cancel',['../struct___remmina_plugin_spice_xfer_widgets.html#adfef2a32ccfa7152eed3b8e65b48bb53',1,'_RemminaPluginSpiceXferWidgets']]],
  ['cancel_5fconnect_5fxport_5fcb',['cancel_connect_xport_cb',['../remmina__protocol__widget_8c.html#a05f9694efc84de6273efa46d232b84f9',1,'remmina_protocol_widget.c']]],
  ['cancel_5finit_5ftunnel_5fcb',['cancel_init_tunnel_cb',['../remmina__protocol__widget_8c.html#a99ba55ddcb17c9e87ca233fa83787227',1,'remmina_protocol_widget.c']]],
  ['cancel_5fopen_5fconnection_5fcb',['cancel_open_connection_cb',['../remmina__protocol__widget_8c.html#a4fb4c2252289e8e891376db6279ef8f5',1,'remmina_protocol_widget.c']]],
  ['cancel_5fstart_5fdirect_5ftunnel_5fcb',['cancel_start_direct_tunnel_cb',['../remmina__protocol__widget_8c.html#a0a93da80662ff18b93270ced59243ce3',1,'remmina_protocol_widget.c']]],
  ['cancel_5fstart_5freverse_5ftunnel_5fcb',['cancel_start_reverse_tunnel_cb',['../remmina__protocol__widget_8c.html#aeffe07c6412286d99ba04f05fe74aeac',1,'remmina_protocol_widget.c']]],
  ['cancel_5ftask',['cancel_task',['../struct___remmina_f_t_p_client_class.html#a99fedf4b912c0bb38662644fe4798a93',1,'_RemminaFTPClientClass']]],
  ['cancel_5ftask_5fsignal',['CANCEL_TASK_SIGNAL',['../remmina__ftp__client_8c.html#abc6126af1d45847bc59afa0aa3216b04a53f24253e039f2efdaa4c7830958df03',1,'remmina_ftp_client.c']]],
  ['cancelled',['cancelled',['../structremmina__masterthread__exec__data.html#ac390f60a6ecc7312c7e4cbc1c4de0a63',1,'remmina_masterthread_exec_data::cancelled()'],['../structon_main_thread__cb__data.html#a75eab61445fbabacaa34f95e2437ea75',1,'onMainThread_cb_data::cancelled()']]],
  ['cb_5fchild_5fwatch',['cb_child_watch',['../exec__plugin_8c.html#ae5fe6643dc64e9250dc2a33e9f266e71',1,'exec_plugin.c']]],
  ['cb_5fclosewidget',['cb_closewidget',['../remmina__exec_8c.html#a0ffd51ed283da4640c920e75e89e694a',1,'remmina_exec.c']]],
  ['cb_5ferr_5fwatch',['cb_err_watch',['../exec__plugin_8c.html#ab43db37b96c68093747620fbde879327',1,'exec_plugin.c']]],
  ['cb_5ffunc_5fdata',['cb_func_data',['../structrs_sched_data.html#a6f5ced4584fcb4e1f6379249ef967a6b',1,'rsSchedData']]],
  ['cb_5ffunc_5fptr',['cb_func_ptr',['../structrs_sched_data.html#aa3dd8912a9d32cb7e5600caea5cc2646',1,'rsSchedData']]],
  ['cb_5flasterror_5fconfirmed',['cb_lasterror_confirmed',['../rcw_8c.html#a56e026ede7a3e50374579ad538e33db7',1,'rcw.c']]],
  ['cb_5fout_5fwatch',['cb_out_watch',['../exec__plugin_8c.html#aa6294507761837488c3777f21096d219',1,'exec_plugin.c']]],
  ['cellrenderertext_5fitem1',['cellrenderertext_item1',['../struct___remmina_string_list.html#a57ee0849e69606d61879c095b736b256',1,'_RemminaStringList']]],
  ['cellrenderertext_5fitem2',['cellrenderertext_item2',['../struct___remmina_string_list.html#a7fb48d319c2798f16123a8f0585e8bbe',1,'_RemminaStringList']]],
  ['changed_5fpasswords_5fcount',['changed_passwords_count',['../structmpchanger__params.html#a44429b8b620590ed1e1e015af6191719',1,'mpchanger_params']]],
  ['changelog_2emd',['CHANGELOG.md',['../_c_h_a_n_g_e_l_o_g_8md.html',1,'']]],
  ['changenext',['changenext',['../remmina__mpchange_8c.html#abea63e74b83d3545308d05dfe88e2839',1,'remmina_mpchange.c']]],
  ['channel',['channel',['../struct___remmina_s_s_h_shell.html#a7c546634f855c217faa75ab4d4c7c2e2',1,'_RemminaSSHShell::channel()'],['../struct___remmina_n_x_session.html#a18ace4f039f5580e24147bb5db1b2aad',1,'_RemminaNXSession::channel()'],['../struct___remmina_tp_channel_handler.html#a3eb504e13624f27e97b61042554d2ece',1,'_RemminaTpChannelHandler::channel()']]],
  ['channel_5fpath',['channel_path',['../struct___remmina_tp_channel_handler.html#a70118dbcd13ffba2652135859fd10769',1,'_RemminaTpChannelHandler']]],
  ['channel_5fproperties',['channel_properties',['../struct___remmina_tp_channel_handler.html#ac0fc6e931ac466c6a14f1974bf353632',1,'_RemminaTpChannelHandler']]],
  ['channels',['channels',['../struct___remmina_s_s_h_tunnel.html#ab11c1bcc8549ce8b154f65f9009c65d4',1,'_RemminaSSHTunnel']]],
  ['channels_5fout',['channels_out',['../struct___remmina_s_s_h_tunnel.html#a37244b9fafd3ab48c0f11ad0b8cfdf91',1,'_RemminaSSHTunnel']]],
  ['charset',['charset',['../struct___remmina_s_s_h.html#a0b35a5a573abb5e8b8a27a91434a00a5',1,'_RemminaSSH']]],
  ['chat_5freceive',['chat_receive',['../structremmina__masterthread__exec__data.html#a384e675528400649161ea3ea41292020',1,'remmina_masterthread_exec_data']]],
  ['chat_5fwindow',['chat_window',['../struct___remmina_protocol_widget_priv.html#a006867628dec1d7b23404a690c967f4e',1,'_RemminaProtocolWidgetPriv']]],
  ['check_5ffor_5fendianness',['check_for_endianness',['../vnc__plugin_8c.html#a781dfc8cb5371260bbf358fa7d9e3751',1,'vnc_plugin.c']]],
  ['checkbutton_5fappearance_5ffullscreen_5fon_5fauto',['checkbutton_appearance_fullscreen_on_auto',['../struct___remmina_pref_dialog.html#ad87514b86ed3f9b5980bf6c565343b87',1,'_RemminaPrefDialog']]],
  ['checkbutton_5fappearance_5fhide_5fsearchbar',['checkbutton_appearance_hide_searchbar',['../struct___remmina_pref_dialog.html#a41f551c6f21f4a448e4c5ec1186240fd',1,'_RemminaPrefDialog']]],
  ['checkbutton_5fappearance_5fhide_5ftoolbar',['checkbutton_appearance_hide_toolbar',['../struct___remmina_pref_dialog.html#a4c6b01300d82ee50a036fc9fb758cb1f',1,'_RemminaPrefDialog']]],
  ['checkbutton_5fappearance_5fshow_5ftabs',['checkbutton_appearance_show_tabs',['../struct___remmina_pref_dialog.html#a120e306368b8fc06841c2fc2d48778c5',1,'_RemminaPrefDialog']]],
  ['checkbutton_5fapplet_5fdisable_5ftray',['checkbutton_applet_disable_tray',['../struct___remmina_pref_dialog.html#acaf49bcad1a8975b1ab0de2c241ff912',1,'_RemminaPrefDialog']]],
  ['checkbutton_5fapplet_5fhide_5ftotals',['checkbutton_applet_hide_totals',['../struct___remmina_pref_dialog.html#aa6e591a542ea2a29a5a00e2bf411d2c6',1,'_RemminaPrefDialog']]],
  ['checkbutton_5fapplet_5flight_5ftray',['checkbutton_applet_light_tray',['../struct___remmina_pref_dialog.html#a277ea20324585713697f7d9572b965f1',1,'_RemminaPrefDialog']]],
  ['checkbutton_5fapplet_5fnew_5fconnection_5fon_5ftop',['checkbutton_applet_new_connection_on_top',['../struct___remmina_pref_dialog.html#a7e7d1f3f754e55464a80fcd8d448c47c',1,'_RemminaPrefDialog']]],
  ['checkbutton_5fapplet_5fstart_5fin_5ftray',['checkbutton_applet_start_in_tray',['../struct___remmina_pref_dialog.html#a3a830a938ddb7dde8a580e108a7ad028',1,'_RemminaPrefDialog']]],
  ['checkbutton_5foptions_5fsave_5fsettings',['checkbutton_options_save_settings',['../struct___remmina_pref_dialog.html#a26ad416987608d6c6f0a98ba9d611119',1,'_RemminaPrefDialog']]],
  ['checkbutton_5foptions_5fssh_5fparseconfig',['checkbutton_options_ssh_parseconfig',['../struct___remmina_pref_dialog.html#aad290cb37b80d5295452ad1fd4322acc',1,'_RemminaPrefDialog']]],
  ['ciphers',['ciphers',['../struct___remmina_s_s_h.html#a0b3e071eb2c6011efdcea3f65f8e7fa0',1,'_RemminaSSH']]],
  ['client',['client',['../struct___remmina_avahi_priv.html#ad98e9c27645d39dd063259aaa7e81040',1,'_RemminaAvahiPriv::client()'],['../structremmina__masterthread__exec__data.html#a0725f553412b93c289f10bd2a49a112b',1,'remmina_masterthread_exec_data::client()'],['../structremmina__masterthread__exec__data.html#a02bfd27b9d74423a89c656e4ebbfa77d',1,'remmina_masterthread_exec_data::client()'],['../struct___remmina_s_f_t_p_client.html#a42cfd0524388c2c05ea4b926dbe966e1',1,'_RemminaSFTPClient::client()'],['../struct___remmina_plugin_sftp_data.html#aef8517c9af56f33ac9933815930e335f',1,'_RemminaPluginSftpData::client()'],['../struct___remmina_plugin_vnc_data.html#a33bc751b1d5728b624bfa57814504f90',1,'_RemminaPluginVncData::client()']]],
  ['clientbuild_5flist',['clientbuild_list',['../rdp__plugin_8c.html#a7565748ab340f2e7c66bcb755e006bd5',1,'rdp_plugin.c']]],
  ['clientbuild_5ftooltip',['clientbuild_tooltip',['../rdp__plugin_8c.html#a6910d98a00cac88fb257d9bb2d080243',1,'rdp_plugin.c']]],
  ['clientcert',['clientcert',['../struct___remmina_protocol_widget_priv.html#a37bd29fa334e378eff4367d61ee59e05',1,'_RemminaProtocolWidgetPriv']]],
  ['clientkey',['clientkey',['../struct___remmina_protocol_widget_priv.html#ae76f074f8613e3f3b01c26eb3ce194f0',1,'_RemminaProtocolWidgetPriv']]],
  ['clipboard',['clipboard',['../structremmina__plugin__rdp__ui__object.html#ab65c695a6089cb396a239ceb3b39416a',1,'remmina_plugin_rdp_ui_object::clipboard()'],['../structremmina__plugin__rdp__ui__object.html#a6c993ee64cd27392efb8bc6f53e0dca0',1,'remmina_plugin_rdp_ui_object::clipboard()'],['../structrf__context.html#a72d2a3d60347327237692947df93aea7',1,'rf_context::clipboard()']]],
  ['clipboard_5fformatdatarequest',['clipboard_formatdatarequest',['../structremmina__plugin__rdp__event.html#a20b531c3036273bb4f2de64ac33c9bf3',1,'remmina_plugin_rdp_event']]],
  ['clipboard_5fformatdataresponse',['clipboard_formatdataresponse',['../structremmina__plugin__rdp__event.html#a4ce27482e2571a90b222cbc9cb7f30f6',1,'remmina_plugin_rdp_event']]],
  ['clipboard_5fformatlist',['clipboard_formatlist',['../structremmina__plugin__rdp__event.html#a663e2e70d2f51daa97666142fdde98d0',1,'remmina_plugin_rdp_event']]],
  ['clipboard_5fhandler',['clipboard_handler',['../structrf__clipboard.html#a52ca364a1902d7f4095234923feed724',1,'rf_clipboard::clipboard_handler()'],['../struct___remmina_plugin_vnc_data.html#af834392929e0be100534bcc4a4d6df3c',1,'_RemminaPluginVncData::clipboard_handler()']]],
  ['clipboard_5ftimer',['clipboard_timer',['../struct___remmina_plugin_vnc_data.html#a9fc1c7e877a675b10282918e418c9bbc',1,'_RemminaPluginVncData']]],
  ['cliprdr',['cliprdr',['../structrf__context.html#a1bb1d1fcc3e6e813ee98f4fe4ce4f3b8',1,'rf_context']]],
  ['close_5fconnection',['close_connection',['../struct___remmina_protocol_plugin.html#ad4b21147a7592bf689c9f761d9e4fdc2',1,'_RemminaProtocolPlugin']]],
  ['closed',['closed',['../struct___remmina_protocol_widget_priv.html#a8700f21cfca6167fc9c4abe37c2d99b6',1,'_RemminaProtocolWidgetPriv::closed()'],['../struct___remmina_s_s_h_shell.html#a6b2e8c1d9705b4c5b64a0e07a1159e80',1,'_RemminaSSHShell::closed()']]],
  ['cnnobj',['cnnobj',['../structremmina__masterthread__exec__data.html#aebeb1a8332f2e1ce5264bf73348bdffd',1,'remmina_masterthread_exec_data::cnnobj()'],['../struct___remmina_protocol_widget.html#aae1dfb6ff52460c80a3adf80d4934f67',1,'_RemminaProtocolWidget::cnnobj()']]],
  ['cnnwin',['cnnwin',['../struct___remmina_connection_object.html#a871e61a40fb0acd54b10191a3942d328',1,'_RemminaConnectionObject']]],
  ['codeset',['codeset',['../structremmina__masterthread__exec__data.html#a8037aefe9dcee17afe1b14166800a668',1,'remmina_masterthread_exec_data']]],
  ['col_5ff',['COL_F',['../remmina__mpchange_8c.html#a7ff5f2dff38e7639981794c43dc9167baa19a4779198791797a93684c6db7733a',1,'remmina_mpchange.c']]],
  ['col_5ffilename',['COL_FILENAME',['../remmina__mpchange_8c.html#a7ff5f2dff38e7639981794c43dc9167badc0807f00f503edcc8e588a3894af0e3',1,'remmina_mpchange.c']]],
  ['col_5fgroup',['COL_GROUP',['../remmina__mpchange_8c.html#a7ff5f2dff38e7639981794c43dc9167ba0d7f93239dde2b010c5117543a7efcbd',1,'remmina_mpchange.c']]],
  ['col_5fname',['COL_NAME',['../remmina__mpchange_8c.html#a7ff5f2dff38e7639981794c43dc9167ba621a0fec1bcdb27626ca53cd7e8b8f24',1,'remmina_mpchange.c']]],
  ['col_5fusername',['COL_USERNAME',['../remmina__mpchange_8c.html#a7ff5f2dff38e7639981794c43dc9167ba4c1dc9a16a3183553d83687506a5652e',1,'remmina_mpchange.c']]],
  ['color0',['color0',['../struct___remmina_color_pref.html#a2a14fc38e73351f9009fe155dd859808',1,'_RemminaColorPref']]],
  ['color1',['color1',['../struct___remmina_color_pref.html#aa4a65b76fbdc0b3e97d3046f0b48e1ff',1,'_RemminaColorPref']]],
  ['color10',['color10',['../struct___remmina_color_pref.html#ae26f55ce50c1485cda60106c5f24256c',1,'_RemminaColorPref']]],
  ['color11',['color11',['../struct___remmina_color_pref.html#ac8e90ea1299ef641fbef1495f899cf97',1,'_RemminaColorPref']]],
  ['color12',['color12',['../struct___remmina_color_pref.html#ab2ca13ee672a2536665f66d544753928',1,'_RemminaColorPref']]],
  ['color13',['color13',['../struct___remmina_color_pref.html#a24a8c38ab11389b860d0658812ff7bbc',1,'_RemminaColorPref']]],
  ['color14',['color14',['../struct___remmina_color_pref.html#a4ad947d133d2aa2c94756aacb5031e79',1,'_RemminaColorPref']]],
  ['color15',['color15',['../struct___remmina_color_pref.html#a8c109e8479617a5e6da0fcaccf511c60',1,'_RemminaColorPref']]],
  ['color2',['color2',['../struct___remmina_color_pref.html#a3a897d3fdc112fd9f96d2363f4094fc4',1,'_RemminaColorPref']]],
  ['color3',['color3',['../struct___remmina_color_pref.html#ab2b41853bdaaefa3968017819c19d316',1,'_RemminaColorPref']]],
  ['color4',['color4',['../struct___remmina_color_pref.html#afa7c4748f22173796b35d1d50b6c1914',1,'_RemminaColorPref']]],
  ['color5',['color5',['../struct___remmina_color_pref.html#a2d0facd5a0480a450d525ce9ef4fed60',1,'_RemminaColorPref']]],
  ['color6',['color6',['../struct___remmina_color_pref.html#a8d7f02ac9b6890ad675b702708bc4af9',1,'_RemminaColorPref']]],
  ['color7',['color7',['../struct___remmina_color_pref.html#a1d9bd13f411189c9d8d8a08ceb77e8d7',1,'_RemminaColorPref']]],
  ['color8',['color8',['../struct___remmina_color_pref.html#a5d92875f0034e5fc3d360d97d5992bd4',1,'_RemminaColorPref']]],
  ['color9',['color9',['../struct___remmina_color_pref.html#ac8ae5e31836066ac69f7c907f30d2ffc',1,'_RemminaColorPref']]],
  ['color_5fpref',['color_pref',['../struct___remmina_pref.html#a85e3d3ef34fb8969211860df159a04d8',1,'_RemminaPref']]],
  ['color_5fschemes',['color_schemes',['../remmina__ssh__plugin_8c.html#a236a29beb2ea51daaeb01fe86d41807c',1,'remmina_ssh_plugin.c']]],
  ['colorbutton_5fbackground',['colorbutton_background',['../struct___remmina_pref_dialog.html#adda92d5335d281571b58a40f3ebe95d4',1,'_RemminaPrefDialog']]],
  ['colorbutton_5fcolor0',['colorbutton_color0',['../struct___remmina_pref_dialog.html#a877c22a6809bb846cc6555c4100ef877',1,'_RemminaPrefDialog']]],
  ['colorbutton_5fcolor1',['colorbutton_color1',['../struct___remmina_pref_dialog.html#a8d88c38e27116711851bfb573e90b811',1,'_RemminaPrefDialog']]],
  ['colorbutton_5fcolor10',['colorbutton_color10',['../struct___remmina_pref_dialog.html#ad9ea2466137b35ccef6896126954bc1a',1,'_RemminaPrefDialog']]],
  ['colorbutton_5fcolor11',['colorbutton_color11',['../struct___remmina_pref_dialog.html#a46e2af690d5d39c6d3ec2c07431d76a8',1,'_RemminaPrefDialog']]],
  ['colorbutton_5fcolor12',['colorbutton_color12',['../struct___remmina_pref_dialog.html#a995f847743fb828f3400559cea4fff35',1,'_RemminaPrefDialog']]],
  ['colorbutton_5fcolor13',['colorbutton_color13',['../struct___remmina_pref_dialog.html#a7a61d9daaf87c47c073e474498a1a15c',1,'_RemminaPrefDialog']]],
  ['colorbutton_5fcolor14',['colorbutton_color14',['../struct___remmina_pref_dialog.html#a4b8e7072d5c4b5c8b5e57714130f973d',1,'_RemminaPrefDialog']]],
  ['colorbutton_5fcolor15',['colorbutton_color15',['../struct___remmina_pref_dialog.html#aa63edd53778f0eba2e610690fb8002a3',1,'_RemminaPrefDialog']]],
  ['colorbutton_5fcolor2',['colorbutton_color2',['../struct___remmina_pref_dialog.html#a95129b25f045ca6f0bc90323b755aebb',1,'_RemminaPrefDialog']]],
  ['colorbutton_5fcolor3',['colorbutton_color3',['../struct___remmina_pref_dialog.html#a2ce8a15918b4f58702d1063afbbc5a4a',1,'_RemminaPrefDialog']]],
  ['colorbutton_5fcolor4',['colorbutton_color4',['../struct___remmina_pref_dialog.html#afa66945e979adc91f4956efb11115a2f',1,'_RemminaPrefDialog']]],
  ['colorbutton_5fcolor5',['colorbutton_color5',['../struct___remmina_pref_dialog.html#a4f80906dcdf81e03e383e38b8ca568a3',1,'_RemminaPrefDialog']]],
  ['colorbutton_5fcolor6',['colorbutton_color6',['../struct___remmina_pref_dialog.html#a9088c8f060e1fe7e12445fb2f9b99bc3',1,'_RemminaPrefDialog']]],
  ['colorbutton_5fcolor7',['colorbutton_color7',['../struct___remmina_pref_dialog.html#adf1125853e83c16e1ce9704c761d7b9c',1,'_RemminaPrefDialog']]],
  ['colorbutton_5fcolor8',['colorbutton_color8',['../struct___remmina_pref_dialog.html#a1b6e2744506a9e396d8cbae0529356bb',1,'_RemminaPrefDialog']]],
  ['colorbutton_5fcolor9',['colorbutton_color9',['../struct___remmina_pref_dialog.html#a280cb6d6e959c7e395a887defde7a485',1,'_RemminaPrefDialog']]],
  ['colorbutton_5fcursor',['colorbutton_cursor',['../struct___remmina_pref_dialog.html#a6bf0904ad661465e2a522b7dced372ab',1,'_RemminaPrefDialog']]],
  ['colorbutton_5fforeground',['colorbutton_foreground',['../struct___remmina_pref_dialog.html#a956db17e48d1c446df7677d3e9f917e2',1,'_RemminaPrefDialog']]],
  ['colordepth_5flist',['colordepth_list',['../rdp__plugin_8c.html#a7d6c92b318e43b96efddbcaa9400c1f1',1,'colordepth_list():&#160;rdp_plugin.c'],['../vnc__plugin_8c.html#a7d6c92b318e43b96efddbcaa9400c1f1',1,'colordepth_list():&#160;vnc_plugin.c'],['../xdmcp__plugin_8c.html#a7d6c92b318e43b96efddbcaa9400c1f1',1,'colordepth_list():&#160;xdmcp_plugin.c']]],
  ['colormap',['colormap',['../structrf__context.html#ad06c1d1a2295fbdf455df0250dfb2523',1,'rf_context']]],
  ['column_5ffiles_5flist_5fgroup',['column_files_list_group',['../struct___remmina_main.html#a56ac6ffe61c0d3663ebc6b26e65a8cbc',1,'_RemminaMain']]],
  ['combo_5fquick_5fconnect_5fprotocol',['combo_quick_connect_protocol',['../struct___remmina_main.html#a35f10af8065b3b64ba6f26db60061d39',1,'_RemminaMain']]],
  ['comboboxtext_5fappearance_5ffullscreen_5ftoolbar_5fvisibility',['comboboxtext_appearance_fullscreen_toolbar_visibility',['../struct___remmina_pref_dialog.html#a7af362b8421b6f066d4d9d737a01ebfb',1,'_RemminaPrefDialog']]],
  ['comboboxtext_5fappearance_5ftab_5finterface',['comboboxtext_appearance_tab_interface',['../struct___remmina_pref_dialog.html#a4a8bc03207480e0e616794ea5efc71f6',1,'_RemminaPrefDialog']]],
  ['comboboxtext_5fappearance_5fview_5fmode',['comboboxtext_appearance_view_mode',['../struct___remmina_pref_dialog.html#a917c214ab0662ebd06db3eb7f7c47bf2',1,'_RemminaPrefDialog']]],
  ['comboboxtext_5foptions_5fdouble_5fclick',['comboboxtext_options_double_click',['../struct___remmina_pref_dialog.html#a86e2ecb7e448ef661c2857de7b724061',1,'_RemminaPrefDialog']]],
  ['comboboxtext_5foptions_5fscale_5fquality',['comboboxtext_options_scale_quality',['../struct___remmina_pref_dialog.html#a5e3df5e92bbf0aec0f317216de88681d',1,'_RemminaPrefDialog']]],
  ['comboboxtext_5foptions_5fssh_5floglevel',['comboboxtext_options_ssh_loglevel',['../struct___remmina_pref_dialog.html#a517186be14ab61d72d6212592b958972',1,'_RemminaPrefDialog']]],
  ['common_5fidentities',['common_identities',['../remmina__ssh_8c.html#a8920b4663823da39661a108f04b33694',1,'remmina_ssh.c']]],
  ['compact',['compact',['../struct___remmina_protocol_setting.html#ab434990a6fe70b7e8a4a8b11129527a6',1,'_RemminaProtocolSetting::compact()'],['../struct___remmina_protocol_setting_opt.html#a4d4d2d1c3aff12f60831e0b03afcd4dc',1,'_RemminaProtocolSettingOpt::compact()']]],
  ['compare',['compare',['../remmina__ssh__plugin_8c.html#a2d76cf254046651d0d5bb36aa83637e4',1,'remmina_ssh_plugin.c']]],
  ['compare_5fsecret_5fplugin_5finit_5forder',['compare_secret_plugin_init_order',['../remmina__plugin__manager_8c.html#ac90d2ffe3909722ca3daedac28da76e5',1,'remmina_plugin_manager.c']]],
  ['compilation_2dguide_2dfor_2drhel_2emd',['Compilation-guide-for-RHEL.md',['../_compilation-guide-for-_r_h_e_l_8md.html',1,'']]],
  ['compilation_2emd',['Compilation.md',['../_compilation_8md.html',1,'']]],
  ['compile_2don_2darch_2dlinux_2emd',['Compile-on-Arch-Linux.md',['../_compile-on-_arch-_linux_8md.html',1,'']]],
  ['compile_2don_2ddebian_2d10_2dbuster_2emd',['Compile-on-Debian-10-Buster.md',['../_compile-on-_debian-10-_buster_8md.html',1,'']]],
  ['compile_2don_2ddebian_2d9_2dstretch_2emd',['Compile-on-Debian-9-Stretch.md',['../_compile-on-_debian-9-_stretch_8md.html',1,'']]],
  ['compile_2don_2dfreebsd_2emd',['Compile-on-FreeBSD.md',['../_compile-on-_free_b_s_d_8md.html',1,'']]],
  ['compile_2don_2dubuntu_2d14_2e04_2emd',['Compile-on-Ubuntu-14.04.md',['../_compile-on-_ubuntu-14_804_8md.html',1,'']]],
  ['compile_2don_2dubuntu_2d16_2e04_2emd',['Compile-on-Ubuntu-16.04.md',['../_compile-on-_ubuntu-16_804_8md.html',1,'']]],
  ['compile_2don_2dubuntu_2d18_2e04_2emd',['Compile-on-Ubuntu-18.04.md',['../_compile-on-_ubuntu-18_804_8md.html',1,'']]],
  ['compile_2don_2dubuntu_2d20_2e04_2emd',['Compile-on-Ubuntu-20.04.md',['../_compile-on-_ubuntu-20_804_8md.html',1,'']]],
  ['complete',['complete',['../structremmina__masterthread__exec__data.html#a2609267a5c4dcf8b9cbe411ee51656d9',1,'remmina_masterthread_exec_data::complete()'],['../structremmina__plugin__rdp__ui__object.html#a64657b5e71205d98805ca66b01bfae3c',1,'remmina_plugin_rdp_ui_object::complete()']]],
  ['complete_5fcleanup_5fon_5fmain_5fthread',['complete_cleanup_on_main_thread',['../rdp__plugin_8c.html#a6aa88468d17f574532ca959f554a70c5',1,'rdp_plugin.c']]],
  ['composition_5fcheck',['composition_check',['../struct___remmina_plugin_rdpset_grid.html#aaddc109b8404a5bbca0c8b0810ae922e',1,'_RemminaPluginRdpsetGrid']]],
  ['compression',['compression',['../struct___remmina_s_s_h.html#ab017331f59fceff63bb33ff00d088870',1,'_RemminaSSH']]],
  ['conn_5fclosed',['conn_closed',['../remmina__protocol__widget_8c.html#a69757e0c39fa9fcf31800f6375772016',1,'remmina_protocol_widget.c']]],
  ['conn_5fopened',['conn_opened',['../remmina__protocol__widget_8c.html#af4a20ef0ff8cc86feeba345498179e48',1,'remmina_protocol_widget.c']]],
  ['connect',['connect',['../struct___remmina_protocol_widget_class.html#a5cf457e2e48c9c2b831b0c8a4f88c760',1,'_RemminaProtocolWidgetClass']]],
  ['connect_5ffunc',['connect_func',['../struct___remmina_s_s_h_tunnel.html#aa904bb1d9f643f6fde2b0eea4a8b1660',1,'_RemminaSSHTunnel']]],
  ['connect_5fmessage_5fpanel',['connect_message_panel',['../struct___remmina_protocol_widget_priv.html#a708ea5b5a9d1f5dcc5ab5bfb3d1a8483',1,'_RemminaProtocolWidgetPriv']]],
  ['connected',['connected',['../struct___remmina_connection_object.html#aed1d667ed3085ee229c0dfdcf715084d',1,'_RemminaConnectionObject::connected()'],['../structrf__context.html#a3c2ffa55ba97bc70324d61b8cc838e73',1,'rf_context::connected()'],['../struct___remmina_plugin_vnc_data.html#ab105fe07330946ed3687ad2d06fabe95',1,'_RemminaPluginVncData::connected()']]],
  ['connection',['connection',['../struct___remmina_tp_channel_handler.html#a4f0768975a5f826a22e8e57c2dd76981',1,'_RemminaTpChannelHandler']]],
  ['connection_5fpath',['connection_path',['../struct___remmina_tp_channel_handler.html#a6799c3ddb6d8b4c3edeed94dfccc6213',1,'_RemminaTpChannelHandler']]],
  ['context',['context',['../structrf__clipboard.html#a773214c0bb4f351a609a085f786304aa',1,'rf_clipboard::context()'],['../structremmina__plugin__rdp__ui__object.html#ab83d6365bee86707a38dc69cdb2e34db',1,'remmina_plugin_rdp_ui_object::context()'],['../structrf__context.html#a23192b58404d1dc629d5e7db3fb103be',1,'rf_context::context()'],['../struct___remmina_tp_channel_handler.html#a36d9b29f36b9ea3fcc35ff2dfd8c0068',1,'_RemminaTpChannelHandler::context()'],['../struct___remmina_plugin_w_w_w_data.html#a22010474535695604f1e05498e726f3e',1,'_RemminaPluginWWWData::context()']]],
  ['contributing_2emd',['CONTRIBUTING.md',['../_c_o_n_t_r_i_b_u_t_i_n_g_8md.html',1,'']]],
  ['count',['count',['../structrs_sched_data.html#a1747344f64896feb08c863ec2cf2528e',1,'rsSchedData']]],
  ['credentials',['credentials',['../struct___remmina_plugin_w_w_w_data.html#aa8ef80ab8bb8cae8d56890720dd58959',1,'_RemminaPluginWWWData']]],
  ['crlf2lf',['crlf2lf',['../rdp__cliprdr_8c.html#a55dec77cdcdc16a6d85680943eb2a859',1,'rdp_cliprdr.c']]],
  ['cursor',['cursor',['../struct___remmina_color_pref.html#a5ae99dc78a80baa14e0407983077c500',1,'_RemminaColorPref::cursor()'],['../structrf__pointer.html#a6e960686357ad03c023712bdfcef1cf0',1,'rf_pointer::cursor()'],['../structremmina__plugin__rdp__ui__object.html#aba5a610f4114e7876e23d429b53f256a',1,'remmina_plugin_rdp_ui_object::cursor()']]],
  ['cursorblinking_5fcheck',['cursorblinking_check',['../struct___remmina_plugin_rdpset_grid.html#a515323687d10da1af50cee93ea2c9b93',1,'_RemminaPluginRdpsetGrid']]],
  ['cursorshadow_5fcheck',['cursorshadow_check',['../struct___remmina_plugin_rdpset_grid.html#ade8c5db180127c00e8413ec0c110da29',1,'_RemminaPluginRdpsetGrid']]],
  ['custom',['CUSTOM',['../remmina__ssh__plugin_8c.html#a236a29beb2ea51daaeb01fe86d41807ca945d6010d321d9fe75cbba7b6f37f3b5',1,'remmina_ssh_plugin.c']]],
  ['compilation',['Compilation',['../md__builds__remmina_remmina-ci__remmina_8wiki__compilation__compilation.html',1,'']]],
  ['compilation_2dguide_2dfor_2drhel',['Compilation-guide-for-RHEL',['../md__builds__remmina_remmina-ci__remmina_8wiki__compilation__compilation-guide-for-_r_h_e_l.html',1,'']]],
  ['compiling_20remmina_20on_20freebsd_2011',['Compiling Remmina on FreeBSD 11',['../md__builds__remmina_remmina-ci__remmina_8wiki__compilation__compile-on-_free_b_s_d.html',1,'']]],
  ['changelog',['Changelog',['../md__c_h_a_n_g_e_l_o_g.html',1,'']]],
  ['contributing',['CONTRIBUTING',['../md__c_o_n_t_r_i_b_u_t_i_n_g.html',1,'']]]
];
