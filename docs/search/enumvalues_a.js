var searchData=
[
  ['scdw_5faborting',['SCDW_ABORTING',['../structrf__clipboard.html#a772cb891c0d40e145cce5ffdd1b2f438af9766a14efb1de3fd44ed4fb71622e5b',1,'rf_clipboard']]],
  ['scdw_5fbusy_5fwait',['SCDW_BUSY_WAIT',['../structrf__clipboard.html#a772cb891c0d40e145cce5ffdd1b2f438aac2dccc58111570a94b8ec7475a8110e',1,'rf_clipboard']]],
  ['scdw_5fnone',['SCDW_NONE',['../structrf__clipboard.html#a772cb891c0d40e145cce5ffdd1b2f438a4b0fb5c70fe8ee6bbd80cb085f8571a1',1,'rf_clipboard']]],
  ['scrolled_5ffullscreen_5fmode',['SCROLLED_FULLSCREEN_MODE',['../remmina__pref_8h.html#aac34dfe6c6b73b43a4656c9dce041034a86e41f162b3b7d5bf1df78241ca1073e',1,'remmina_pref.h']]],
  ['scrolled_5fwindow_5fmode',['SCROLLED_WINDOW_MODE',['../remmina__pref_8h.html#aac34dfe6c6b73b43a4656c9dce041034af963e665cf74d483ccc74e28531a480a',1,'remmina_pref.h']]],
  ['server_5fcolumn',['SERVER_COLUMN',['../remmina__main_8c.html#ab04a0655cd1e3bcac5e8f48c18df1a57a8c129f382d9456db6f671e1e6e99d32f',1,'remmina_main.c']]],
  ['solarized_5fdark',['SOLARIZED_DARK',['../remmina__ssh__plugin_8c.html#a236a29beb2ea51daaeb01fe86d41807ca0fa0a9e0d9e9802d9efbf77e90ad98ab',1,'remmina_ssh_plugin.c']]],
  ['solarized_5flight',['SOLARIZED_LIGHT',['../remmina__ssh__plugin_8c.html#a236a29beb2ea51daaeb01fe86d41807cabfefd4b212ac88606d39713598487e13',1,'remmina_ssh_plugin.c']]],
  ['ssh_5fauth_5fagent',['SSH_AUTH_AGENT',['../remmina__file_8h.html#a99fb83031ce9923c84392b4e92f956b5a39a8dcea793c406f0fe90f3175c4318e',1,'remmina_file.h']]],
  ['ssh_5fauth_5fauto_5fpublickey',['SSH_AUTH_AUTO_PUBLICKEY',['../remmina__file_8h.html#a99fb83031ce9923c84392b4e92f956b5a9f6024449a620d679eab2c09ea77877e',1,'remmina_file.h']]],
  ['ssh_5fauth_5fgssapi',['SSH_AUTH_GSSAPI',['../remmina__file_8h.html#a99fb83031ce9923c84392b4e92f956b5a6264a71469928ad51fa802f65f1a934a',1,'remmina_file.h']]],
  ['ssh_5fauth_5fpassword',['SSH_AUTH_PASSWORD',['../remmina__file_8h.html#a99fb83031ce9923c84392b4e92f956b5a7d995a7868d2d2f81e9dd2f898b206fe',1,'remmina_file.h']]],
  ['ssh_5fauth_5fpublickey',['SSH_AUTH_PUBLICKEY',['../remmina__file_8h.html#a99fb83031ce9923c84392b4e92f956b5aeba5f4ff7657d7b15519556793ee8930',1,'remmina_file.h']]]
];
