var remmina__exec_8c =
[
    [ "cb_closewidget", "remmina__exec_8c.html#a0ffd51ed283da4640c920e75e89e694a", null ],
    [ "disable_rcw_delete_confirm_cb", "remmina__exec_8c.html#aacc2a2ffd19e56fcb3e4b025d180ab8f", null ],
    [ "newline_remove", "remmina__exec_8c.html#aca2d356a03e1aac7b716b408183aa305", null ],
    [ "remmina_application_condexit", "remmina__exec_8c.html#aaa67e07e2426828d7e7b9b30d2038d31", null ],
    [ "remmina_exec_autostart_cb", "remmina__exec_8c.html#ae3299f9cf136cb759dc052ee799b1f0d", null ],
    [ "remmina_exec_command", "remmina__exec_8c.html#a424cabdcff647797061e7482049d62a7", null ],
    [ "remmina_exec_exitremmina", "remmina__exec_8c.html#a206abe0e916081b29c5faa52330e2271", null ],
    [ "remmina_exec_get_build_config", "remmina__exec_8c.html#a93f5633a6348dc1df1a5723b3054a2a3", null ],
    [ "remmina_exec_set_setting", "remmina__exec_8c.html#a4e202866339a104ffdbfa519cfbcdca9", null ]
];