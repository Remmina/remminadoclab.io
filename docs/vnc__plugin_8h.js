var vnc__plugin_8h =
[
    [ "_RemminaPluginVncData", "struct___remmina_plugin_vnc_data.html", "struct___remmina_plugin_vnc_data" ],
    [ "_RemminaPluginVncEvent", "struct___remmina_plugin_vnc_event.html", "struct___remmina_plugin_vnc_event" ],
    [ "_RemminaPluginVncCoordinates", "struct___remmina_plugin_vnc_coordinates.html", "struct___remmina_plugin_vnc_coordinates" ],
    [ "RemminaPluginVncCoordinates", "vnc__plugin_8h.html#a674a2c4ec1a9f84a5104f171cd60cc18", null ],
    [ "RemminaPluginVncData", "vnc__plugin_8h.html#a323c38c39503620cba03053c071e4ae9", null ],
    [ "RemminaPluginVncEvent", "vnc__plugin_8h.html#a74ce33a323bd943b8c6749ddc18faa18", null ],
    [ "REMMINA_PLUGIN_VNC_EVENT_KEY", "vnc__plugin_8h.html#aa156d1cebb38c8a65846c4d9c006012aa98fec12a2047bf22ce4d44b9e0905540", null ],
    [ "REMMINA_PLUGIN_VNC_EVENT_POINTER", "vnc__plugin_8h.html#aa156d1cebb38c8a65846c4d9c006012aa1897cf83244e2e2e25c7f32e50a4964f", null ],
    [ "REMMINA_PLUGIN_VNC_EVENT_CUTTEXT", "vnc__plugin_8h.html#aa156d1cebb38c8a65846c4d9c006012aa0662a5bd411f37922da376de06e8fbfd", null ],
    [ "REMMINA_PLUGIN_VNC_EVENT_CHAT_OPEN", "vnc__plugin_8h.html#aa156d1cebb38c8a65846c4d9c006012aaa818c3e00b67a01cdee20db2136a7718", null ],
    [ "REMMINA_PLUGIN_VNC_EVENT_CHAT_SEND", "vnc__plugin_8h.html#aa156d1cebb38c8a65846c4d9c006012aa0723d566806911ca2ed79d03214fe4ee", null ],
    [ "REMMINA_PLUGIN_VNC_EVENT_CHAT_CLOSE", "vnc__plugin_8h.html#aa156d1cebb38c8a65846c4d9c006012aa4588937e7ecca9c67f043eabff7de4df", null ],
    [ "remmina_plugin_vnc_update_scale", "vnc__plugin_8h.html#a8a70c9cdaf6821611a3311c0d4797db9", null ]
];