var remmina__pref__dialog_8h =
[
    [ "_RemminaPrefDialogPriv", "struct___remmina_pref_dialog_priv.html", "struct___remmina_pref_dialog_priv" ],
    [ "_RemminaPrefDialog", "struct___remmina_pref_dialog.html", "struct___remmina_pref_dialog" ],
    [ "RemminaPrefDialog", "remmina__pref__dialog_8h.html#a5318669e14d555b8bd2adc81a54d97b7", null ],
    [ "RemminaPrefDialogPriv", "remmina__pref__dialog_8h.html#aeba55bfe8ab83707f2e68034061a3dd2", null ],
    [ "REMMINA_PREF_OPTIONS_TAB", "remmina__pref__dialog_8h.html#a96a58e29e8dbf2b5bdeb775cba46556ea5622b46aa48f22a0c4511f3697e56f4a", null ],
    [ "REMMINA_PREF_APPEARANCE", "remmina__pref__dialog_8h.html#a96a58e29e8dbf2b5bdeb775cba46556eab813650ab0842f29fc9dcf865a2285cb", null ],
    [ "REMMINA_PREF_APPLET_TAB", "remmina__pref__dialog_8h.html#a96a58e29e8dbf2b5bdeb775cba46556eab2070bbdfe1e9ee12f43a52556f3130c", null ],
    [ "remmina_pref_dialog_get_dialog", "remmina__pref__dialog_8h.html#aa8862957212096ddda372be1310c8bd4", null ],
    [ "remmina_pref_dialog_new", "remmina__pref__dialog_8h.html#a482168ebe847fb3d052f940e8b82a69a", null ],
    [ "remmina_prefdiag_unlock_repwd_on_changed", "remmina__pref__dialog_8h.html#a19ba9bbbff353b8e36810d423e6f7e8f", null ]
];