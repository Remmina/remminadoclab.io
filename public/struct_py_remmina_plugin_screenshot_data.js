var struct_py_remmina_plugin_screenshot_data =
[
    [ "bitsPerPixel", "struct_py_remmina_plugin_screenshot_data.html#a79271016958eb788bdbd7184b9af0bd7", null ],
    [ "buffer", "struct_py_remmina_plugin_screenshot_data.html#aac36eb74554bd33d71ae4b6531ffa720", null ],
    [ "bytesPerPixel", "struct_py_remmina_plugin_screenshot_data.html#a74e84cd06fe5493d2e309fa422e0349e", null ],
    [ "height", "struct_py_remmina_plugin_screenshot_data.html#a132778befe6b5251fccff28f12479b41", null ],
    [ "width", "struct_py_remmina_plugin_screenshot_data.html#a6e0de9833ceda28199a84c4c10437e46", null ]
];