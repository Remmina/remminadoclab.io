var struct_py_plugin =
[
    [ "entry_plugin", "struct_py_plugin.html#a1550876f14c98c48981015b8399a095b", null ],
    [ "file_plugin", "struct_py_plugin.html#a50c373b557faddaf959746d7086cbd4f", null ],
    [ "generic_plugin", "struct_py_plugin.html#a680f794a3909843ccb6d2e7ceeef8558", null ],
    [ "gp", "struct_py_plugin.html#a1c03e220a1192e29221b3a8e11ff91f4", null ],
    [ "instance", "struct_py_plugin.html#a3b78180c53db5f3547a8c276a815375a", null ],
    [ "pref_plugin", "struct_py_plugin.html#a0ad50bbd41dc27f511d6734b256a37ac", null ],
    [ "protocol_plugin", "struct_py_plugin.html#a00183896cad1d1b31b3c5cb724db001f", null ],
    [ "secret_plugin", "struct_py_plugin.html#afb78d1611099092a7412a2d1818cca40", null ],
    [ "tool_plugin", "struct_py_plugin.html#aeba82110f6ea8b55fcbebe80f5134c82", null ]
];