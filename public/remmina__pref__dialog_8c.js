var remmina__pref__dialog_8c =
[
    [ "remmina_pref_dialog_add_pref_plugin", "remmina__pref__dialog_8c.html#add9c48bccc2c61be2f2900a154648dd4", null ],
    [ "remmina_pref_dialog_clear_recent", "remmina__pref__dialog_8c.html#a470290d26c88259265912a3e0018b662", null ],
    [ "remmina_pref_dialog_disable_tray_icon_on_toggled", "remmina__pref__dialog_8c.html#a18bfbeb5d049a9f37f7624199b2b864a", null ],
    [ "remmina_pref_dialog_get_dialog", "remmina__pref__dialog_8c.html#a9f24dad52fd8f57961ac7c5df58f251e", null ],
    [ "remmina_pref_dialog_init", "remmina__pref__dialog_8c.html#a9a050b69e373c90ea7d400aa94f34344", null ],
    [ "remmina_pref_dialog_new", "remmina__pref__dialog_8c.html#ab09f64d25a5a016281a56edca7e2d70a", null ],
    [ "remmina_pref_dialog_on_action_close", "remmina__pref__dialog_8c.html#a722371d3ad01079279d5e86d7c8400e1", null ],
    [ "remmina_pref_dialog_on_close_clicked", "remmina__pref__dialog_8c.html#adc2763311ece7ab6b505d4ec56391f69", null ],
    [ "remmina_pref_dialog_on_key_chooser", "remmina__pref__dialog_8c.html#a7576b68fbb2a0497d7ac51eb0633e877", null ],
    [ "remmina_pref_dialog_set_button_label", "remmina__pref__dialog_8c.html#a384dde55873f855555c0194d45cbfd59", null ],
    [ "remmina_pref_dialog_vte_font_on_toggled", "remmina__pref__dialog_8c.html#a2d0556c47e16187ea61e70516a6346bc", null ],
    [ "remmina_pref_on_button_keystrokes_clicked", "remmina__pref__dialog_8c.html#a0c5f28eea2fe32297da5f7833df7b131", null ],
    [ "remmina_pref_on_button_resolutions_clicked", "remmina__pref__dialog_8c.html#ad4b3f23a900c8f07814fd0fb26f29e5f", null ],
    [ "remmina_pref_on_color_scheme_selected", "remmina__pref__dialog_8c.html#a50dcebddc0891688ae179ba940d21fd1", null ],
    [ "remmina_pref_on_dialog_destroy", "remmina__pref__dialog_8c.html#ab9776574663e2c3fcb793ec70887336d", null ],
    [ "remmina_prefdiag_on_grab_color_activated", "remmina__pref__dialog_8c.html#ad9db3e5d980f28a2764be2ee6db18fef", null ],
    [ "remmina_prefdiag_on_use_password_activated", "remmina__pref__dialog_8c.html#a9bf2ea132069b7fa70b4aa5f94b8fa21", null ],
    [ "pref_actions", "remmina__pref__dialog_8c.html#a2458dfa5d1188dceb52795faa20f8257", null ],
    [ "remmina_pref_dialog", "remmina__pref__dialog_8c.html#a2e3c32df32107ad3f4b9ea85fc001368", null ]
];