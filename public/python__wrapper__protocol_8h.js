var python__wrapper__protocol_8h =
[
    [ "PyRemminaProtocolFeature", "struct_py_remmina_protocol_feature.html", "struct_py_remmina_protocol_feature" ],
    [ "PyRemminaPluginScreenshotData", "struct_py_remmina_plugin_screenshot_data.html", "struct_py_remmina_plugin_screenshot_data" ],
    [ "python_wrapper_create_protocol_plugin", "python__wrapper__protocol_8h.html#aaa90f5541895f5c3dedaa16c15d80de7", null ],
    [ "python_wrapper_protocol_feature_new", "python__wrapper__protocol_8h.html#a26eea3cdd2d97d5cd8ad00d2bbe17c29", null ],
    [ "python_wrapper_protocol_init", "python__wrapper__protocol_8h.html#ab223d3563d16258e33e8dfae0ee41e5d", null ],
    [ "python_wrapper_screenshot_data_new", "python__wrapper__protocol_8h.html#ad9e2034841e5b0a10291ad015e4bfc46", null ]
];