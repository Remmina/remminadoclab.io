var struct___remmina_bug_report_dialog =
[
    [ "bug_report_debug_data_check_button", "struct___remmina_bug_report_dialog.html#a4f807361a7dbc51c7f78c972dfd99bf7", null ],
    [ "bug_report_description_textview", "struct___remmina_bug_report_dialog.html#a6a1266b9fbfca35027c49666f6a988c1", null ],
    [ "bug_report_email_entry", "struct___remmina_bug_report_dialog.html#a9a5dc56139bba06bb737de76c772b72c", null ],
    [ "bug_report_include_system_info_check_button", "struct___remmina_bug_report_dialog.html#a26a15b74f7fe05af79e3ad88bde3ced6", null ],
    [ "bug_report_name_entry", "struct___remmina_bug_report_dialog.html#a8c23731a45ab4afba45d8e1e54123c56", null ],
    [ "bug_report_submit_button", "struct___remmina_bug_report_dialog.html#a734ae2ce14a20857177746bdafda9aa2", null ],
    [ "bug_report_submit_status_label", "struct___remmina_bug_report_dialog.html#af1cf6b13484b785efffab143cb39ad04", null ],
    [ "bug_report_title_entry", "struct___remmina_bug_report_dialog.html#a46d0b5ae7c8d64da637f0572a8b245b6", null ],
    [ "builder", "struct___remmina_bug_report_dialog.html#a9097737e328d0c52f238664e7ea22aa4", null ],
    [ "dialog", "struct___remmina_bug_report_dialog.html#a16e0cd7851ff45bd50c5e40b07cac4d0", null ]
];