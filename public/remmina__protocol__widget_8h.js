var remmina__protocol__widget_8h =
[
    [ "_RemminaProtocolWidget", "struct___remmina_protocol_widget.html", "struct___remmina_protocol_widget" ],
    [ "_RemminaProtocolWidgetClass", "struct___remmina_protocol_widget_class.html", "struct___remmina_protocol_widget_class" ],
    [ "RemminaHostkeyFunc", "remmina__protocol__widget_8h.html#ab89fba118812616a3e47504cfe4f3390", null ],
    [ "RemminaProtocolPlugin", "remmina__protocol__widget_8h.html#a2dbd7c9006318dd027d5a5f0b4f59f26", null ],
    [ "RemminaProtocolWidgetPriv", "remmina__protocol__widget_8h.html#af0c9f34cf823d03994aacc083d832c19", null ],
    [ "remmina_protocol_widget_call_feature_by_ref", "remmina__protocol__widget_8h.html#a605b69a9aa4393024369cc5070488692", null ],
    [ "remmina_protocol_widget_call_feature_by_type", "remmina__protocol__widget_8h.html#a6a630e96e5b914d60d678ae8eaad4cd9", null ],
    [ "remmina_protocol_widget_chat_close", "remmina__protocol__widget_8h.html#a4a3a9ba7422fc302b0b412ac771c9953", null ],
    [ "remmina_protocol_widget_chat_open", "remmina__protocol__widget_8h.html#a66263e6e7da79357ceb80d32b191d9f2", null ],
    [ "remmina_protocol_widget_chat_receive", "remmina__protocol__widget_8h.html#ae2522242b3f39ee84ee8cf75170de0c0", null ],
    [ "remmina_protocol_widget_close_connection", "remmina__protocol__widget_8h.html#aa41324b7175c46bb42b978332db95d22", null ],
    [ "remmina_protocol_widget_desktop_resize", "remmina__protocol__widget_8h.html#a9acf045c5a3eec5b7f748678dee0d3ee", null ],
    [ "remmina_protocol_widget_emit_signal", "remmina__protocol__widget_8h.html#ac3e5a7f14aef4adb2e57d35e9c180b3b", null ],
    [ "remmina_protocol_widget_get_cacert", "remmina__protocol__widget_8h.html#a9473da27fd10eb46ef8360f9ad4be86d", null ],
    [ "remmina_protocol_widget_get_cacrl", "remmina__protocol__widget_8h.html#a70aecb9af2ea35527e8d058d1d87a0d3", null ],
    [ "remmina_protocol_widget_get_clientcert", "remmina__protocol__widget_8h.html#ab9632c73b32531b7bea976e10204670b", null ],
    [ "remmina_protocol_widget_get_clientkey", "remmina__protocol__widget_8h.html#a08d312825fc0de9f9286279d99f763a1", null ],
    [ "remmina_protocol_widget_get_current_scale_mode", "remmina__protocol__widget_8h.html#a5ec62969da54155e24edcb1d998f93f7", null ],
    [ "remmina_protocol_widget_get_domain", "remmina__protocol__widget_8h.html#a7c15fa81642c52ef382e1dbe34ffb6ac", null ],
    [ "remmina_protocol_widget_get_error_message", "remmina__protocol__widget_8h.html#a508044024b303c286a0e643e44d85a29", null ],
    [ "remmina_protocol_widget_get_expand", "remmina__protocol__widget_8h.html#a25bcb2ae99449624fed6f770218e3d25", null ],
    [ "remmina_protocol_widget_get_features", "remmina__protocol__widget_8h.html#af9b6a2c2665010ab09a6d9943f9850bd", null ],
    [ "remmina_protocol_widget_get_file", "remmina__protocol__widget_8h.html#a2506e80d482c34532d0e534856452069", null ],
    [ "remmina_protocol_widget_get_gtkwindow", "remmina__protocol__widget_8h.html#a8127b4cc7c278216dff87a7e4a336dfb", null ],
    [ "remmina_protocol_widget_get_height", "remmina__protocol__widget_8h.html#ae609b351fff17c235251b0ba7c480da5", null ],
    [ "remmina_protocol_widget_get_multimon", "remmina__protocol__widget_8h.html#abdf99138612d02bc3921b0b77bdc89cb", null ],
    [ "remmina_protocol_widget_get_name", "remmina__protocol__widget_8h.html#ab224a0f643d291f86ede54e373bd0a6f", null ],
    [ "remmina_protocol_widget_get_password", "remmina__protocol__widget_8h.html#a3fc52d61f77327534286f57e563e0923", null ],
    [ "remmina_protocol_widget_get_profile_remote_height", "remmina__protocol__widget_8h.html#ad8df6a87e19b7751b26820ef4a668de9", null ],
    [ "remmina_protocol_widget_get_profile_remote_width", "remmina__protocol__widget_8h.html#ab4c6435ef92e34a94270069ce1861907", null ],
    [ "remmina_protocol_widget_get_savepassword", "remmina__protocol__widget_8h.html#a9ed3110b7b030bccd0ff9ed70e86da2d", null ],
    [ "remmina_protocol_widget_get_type", "remmina__protocol__widget_8h.html#aca61c93aaf5f3cbc2046c8267bb84a2e", null ],
    [ "remmina_protocol_widget_get_username", "remmina__protocol__widget_8h.html#a96159bd8e9d70c74967e666e2f188df0", null ],
    [ "remmina_protocol_widget_get_width", "remmina__protocol__widget_8h.html#aaf355ea7103fc57a4cbc7b815cde2995", null ],
    [ "remmina_protocol_widget_grab_focus", "remmina__protocol__widget_8h.html#a290eb68f0e86de1b7b82e2d98a1494d1", null ],
    [ "remmina_protocol_widget_gtkviewport", "remmina__protocol__widget_8h.html#a2067c19b160eb43bddda6c6c6e8d6045", null ],
    [ "remmina_protocol_widget_has_error", "remmina__protocol__widget_8h.html#aab2f304822ccfd4979854ef1afd81771", null ],
    [ "remmina_protocol_widget_is_closed", "remmina__protocol__widget_8h.html#a2830f756dfc3d735b57b0c5753c429e3", null ],
    [ "remmina_protocol_widget_lock_dynres", "remmina__protocol__widget_8h.html#a36e01bda95825ee6b5f164f52af7c457", null ],
    [ "remmina_protocol_widget_map_event", "remmina__protocol__widget_8h.html#a1585ea6908dd5c0a9b0f47d409916435", null ],
    [ "remmina_protocol_widget_mpdestroy", "remmina__protocol__widget_8h.html#a97cba8208a4348446159bf465b3233f5", null ],
    [ "remmina_protocol_widget_mpprogress", "remmina__protocol__widget_8h.html#a78b123eca6709bceeacda7d791a94bf1", null ],
    [ "remmina_protocol_widget_new", "remmina__protocol__widget_8h.html#a5c2137462d3168e90c812f1ce75a6e62", null ],
    [ "remmina_protocol_widget_open_connection", "remmina__protocol__widget_8h.html#a6399c3828908e36ed6202b5599bd206b", null ],
    [ "remmina_protocol_widget_panel_auth", "remmina__protocol__widget_8h.html#aebc7c7d34c470081a52aa35d998ce868", null ],
    [ "remmina_protocol_widget_panel_authuserpwd_ssh_tunnel", "remmina__protocol__widget_8h.html#ad637ec849c497e4a0d4c723396aa6fba", null ],
    [ "remmina_protocol_widget_panel_authx509", "remmina__protocol__widget_8h.html#aa674c14f3a46dd5eb6b53d6f8ce6bd31", null ],
    [ "remmina_protocol_widget_panel_changed_certificate", "remmina__protocol__widget_8h.html#acf75fd4bac28a3d53da064b2905f23c1", null ],
    [ "remmina_protocol_widget_panel_destroy", "remmina__protocol__widget_8h.html#a78069d2e4c2773685126fc3127b26dae", null ],
    [ "remmina_protocol_widget_panel_hide", "remmina__protocol__widget_8h.html#a8ef0523afadadc9984d53fb02ecc01ad", null ],
    [ "remmina_protocol_widget_panel_new_certificate", "remmina__protocol__widget_8h.html#a262d75d6b535bd8c623e607805db4084", null ],
    [ "remmina_protocol_widget_panel_question_yesno", "remmina__protocol__widget_8h.html#a9092c29f585fdabaf0ce7e865670053b", null ],
    [ "remmina_protocol_widget_panel_show", "remmina__protocol__widget_8h.html#a843c2455a10c9382aee454515f9e7f8b", null ],
    [ "remmina_protocol_widget_panel_show_listen", "remmina__protocol__widget_8h.html#a278e13b53a4fdb65498e00e908edb98d", null ],
    [ "remmina_protocol_widget_panel_show_retry", "remmina__protocol__widget_8h.html#a7ed85f4ea19e0660dbf46f4f3dc4b895", null ],
    [ "remmina_protocol_widget_plugin_receives_keystrokes", "remmina__protocol__widget_8h.html#a480f842fac65440c367bf3c3d87d2601", null ],
    [ "remmina_protocol_widget_plugin_screenshot", "remmina__protocol__widget_8h.html#a57ac13fc4a2653c40e0601183628c0ca", null ],
    [ "remmina_protocol_widget_query_feature_by_ref", "remmina__protocol__widget_8h.html#a4017a800d7803a40d606ffce2578509a", null ],
    [ "remmina_protocol_widget_query_feature_by_type", "remmina__protocol__widget_8h.html#adbe15328275677cb38bd8c8a2d5e039d", null ],
    [ "remmina_protocol_widget_register_hostkey", "remmina__protocol__widget_8h.html#a13e14fa81f7ca52942415a7caf5b932c", null ],
    [ "remmina_protocol_widget_save_cred", "remmina__protocol__widget_8h.html#ab863cc1442a992adfa323bc77ab88504", null ],
    [ "remmina_protocol_widget_send_clipboard", "remmina__protocol__widget_8h.html#ae9b1b1b9fbce0de9b0bbb678c0bf9403", null ],
    [ "remmina_protocol_widget_send_keys_signals", "remmina__protocol__widget_8h.html#a9bcd0d356c7215dc0f791c9744360705", null ],
    [ "remmina_protocol_widget_send_keystrokes", "remmina__protocol__widget_8h.html#affb2413c00f0681fe08f90523f7e1471", null ],
    [ "remmina_protocol_widget_set_current_scale_mode", "remmina__protocol__widget_8h.html#ad6e2d00646e8268aa0e8bbe31b77db48", null ],
    [ "remmina_protocol_widget_set_display", "remmina__protocol__widget_8h.html#a15368afcbeb770f8cf25941bd49e7b54", null ],
    [ "remmina_protocol_widget_set_error", "remmina__protocol__widget_8h.html#a95088334da86faf9520383629fff48af", null ],
    [ "remmina_protocol_widget_set_expand", "remmina__protocol__widget_8h.html#a0b19dbe685e4465e7f06706270e120fa", null ],
    [ "remmina_protocol_widget_set_height", "remmina__protocol__widget_8h.html#aadd106cec729a80405c6097e4c662044", null ],
    [ "remmina_protocol_widget_set_hostkey_func", "remmina__protocol__widget_8h.html#a5aaadd752a3275a0a8e0018a3dd669f8", null ],
    [ "remmina_protocol_widget_set_width", "remmina__protocol__widget_8h.html#a41b62f211a8a8e960bc80aa1ba6b0380", null ],
    [ "remmina_protocol_widget_setup", "remmina__protocol__widget_8h.html#ac705ffa194e3c3457e9a0a1bd6c79be8", null ],
    [ "remmina_protocol_widget_signal_connection_closed", "remmina__protocol__widget_8h.html#a1cae52cad394f317c712eb4fd709d261", null ],
    [ "remmina_protocol_widget_signal_connection_opened", "remmina__protocol__widget_8h.html#a05b5edfc3d3d0dcff1745d1d35aaca5c", null ],
    [ "remmina_protocol_widget_ssh_exec", "remmina__protocol__widget_8h.html#a12eddf3428a53201ee0a2ec422d2fa4a", null ],
    [ "remmina_protocol_widget_start_direct_tunnel", "remmina__protocol__widget_8h.html#a9d55210413d6a313478f81cf76ecf1b7", null ],
    [ "remmina_protocol_widget_start_reverse_tunnel", "remmina__protocol__widget_8h.html#aa6d9f2f558fcd9e7fe58eefcde1c3c5c", null ],
    [ "remmina_protocol_widget_start_xport_tunnel", "remmina__protocol__widget_8h.html#a172fa9cf2ce196c9846a3e47ea9036b4", null ],
    [ "remmina_protocol_widget_unlock_dynres", "remmina__protocol__widget_8h.html#a2551dd6f53480609596d2695040fba5d", null ],
    [ "remmina_protocol_widget_unmap_event", "remmina__protocol__widget_8h.html#ac9da39853a32523057759a498bf6ac0a", null ],
    [ "remmina_protocol_widget_update_align", "remmina__protocol__widget_8h.html#aa88afa1e3f04974974a8be324b0a930e", null ],
    [ "remmina_protocol_widget_update_remote_resolution", "remmina__protocol__widget_8h.html#a14684e3222e6afb1b43d77634a174767", null ]
];