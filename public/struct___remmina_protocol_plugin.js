var struct___remmina_protocol_plugin =
[
    [ "advanced_settings", "struct___remmina_protocol_plugin.html#aceb00d9dff794500ccb04a1373eb32db", null ],
    [ "basic_settings", "struct___remmina_protocol_plugin.html#aae09a7e2c978cc36e85d0f295518978d", null ],
    [ "call_feature", "struct___remmina_protocol_plugin.html#a4908abc6c70c59c42e2757aecaa25cf1", null ],
    [ "close_connection", "struct___remmina_protocol_plugin.html#ad4b21147a7592bf689c9f761d9e4fdc2", null ],
    [ "description", "struct___remmina_protocol_plugin.html#a9bac82016e8b61d3c63b2e427be3ec70", null ],
    [ "domain", "struct___remmina_protocol_plugin.html#a61bee777a81714c80bed22d0cf8e13ae", null ],
    [ "features", "struct___remmina_protocol_plugin.html#afa1a3a21c504b386cecfb42d1c9e6bff", null ],
    [ "get_plugin_screenshot", "struct___remmina_protocol_plugin.html#ad4778e62aa78baace7d46fb1c4a475c4", null ],
    [ "icon_name", "struct___remmina_protocol_plugin.html#a9552c70a10eea2b8263dbc7d05e2fb10", null ],
    [ "icon_name_ssh", "struct___remmina_protocol_plugin.html#a8a4c7d195e85a1428abf82e952ff4df3", null ],
    [ "init", "struct___remmina_protocol_plugin.html#a3d9e2f2eb2594411e6fe63cde443dfb2", null ],
    [ "map_event", "struct___remmina_protocol_plugin.html#ae0dc6952f8015848cf97950fcc7d9997", null ],
    [ "name", "struct___remmina_protocol_plugin.html#a6e04143450eeb71e3d0b2c14983860a4", null ],
    [ "open_connection", "struct___remmina_protocol_plugin.html#ad2e965f3997d21a1e08d365c74e615bd", null ],
    [ "query_feature", "struct___remmina_protocol_plugin.html#aa49520ac95b505c111abfb7f3b7c55de", null ],
    [ "send_keystrokes", "struct___remmina_protocol_plugin.html#a92bb5524267ad4fe682fc07f778500d7", null ],
    [ "ssh_setting", "struct___remmina_protocol_plugin.html#aa5727804eb3f5cd4a4a40acb98df76bb", null ],
    [ "type", "struct___remmina_protocol_plugin.html#ac783acfaf93007d0100bd02f1faff7f0", null ],
    [ "unmap_event", "struct___remmina_protocol_plugin.html#abfb8cc172b53c16c8099eba7ab3bbc57", null ],
    [ "version", "struct___remmina_protocol_plugin.html#a3f3cd7ad27c78a1670de3f42414861ff", null ]
];