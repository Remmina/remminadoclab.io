var struct___remmina_secret_plugin =
[
    [ "delete_password", "struct___remmina_secret_plugin.html#abbe56963e94b1a131f3e6d9bcafce772", null ],
    [ "description", "struct___remmina_secret_plugin.html#a175d83898486e37aadd16279f559f659", null ],
    [ "domain", "struct___remmina_secret_plugin.html#a81ff30e7efe61fd0057184640baf545e", null ],
    [ "get_password", "struct___remmina_secret_plugin.html#a34defea2606bbc99a2c334f57e056b06", null ],
    [ "init", "struct___remmina_secret_plugin.html#a6c664274cd540fea91013d298d1ef031", null ],
    [ "init_order", "struct___remmina_secret_plugin.html#a0155c6684591e0dd843c7145792fef09", null ],
    [ "is_service_available", "struct___remmina_secret_plugin.html#a2123e14795b104f5fe8d77a4b7847261", null ],
    [ "name", "struct___remmina_secret_plugin.html#a197b28d8b71ccb0c91ce8bf27690968a", null ],
    [ "store_password", "struct___remmina_secret_plugin.html#a03fab6fcb013fa87be06d551f936c317", null ],
    [ "type", "struct___remmina_secret_plugin.html#a3e49bae6984c9bfd714ea8c664ce33a2", null ],
    [ "version", "struct___remmina_secret_plugin.html#ac6927c92267fdb786b01319aeade0daa", null ]
];