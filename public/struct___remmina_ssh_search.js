var struct___remmina_ssh_search =
[
    [ "builder", "struct___remmina_ssh_search.html#a8a571c5560ea5c57936b943a990465b5", null ],
    [ "close_button", "struct___remmina_ssh_search.html#ac8bc2106a642db92386720a9ef08fb25", null ],
    [ "entire_word_checkbutton", "struct___remmina_ssh_search.html#a94b038162acce3e176a66746041ca298", null ],
    [ "has_regex", "struct___remmina_ssh_search.html#a1d0ef8ac0f7a8aa86cd345898b079eff", null ],
    [ "match_case_checkbutton", "struct___remmina_ssh_search.html#ad2be1b094ba3d2e65bea1cbd5bc948e0", null ],
    [ "parent", "struct___remmina_ssh_search.html#ab9a8a6aebee01778f250adf601ed385c", null ],
    [ "regex_caseless", "struct___remmina_ssh_search.html#af33585cf803ea86f6e1dccc057e2cce0", null ],
    [ "regex_checkbutton", "struct___remmina_ssh_search.html#a244e121887afe8fa1624be3ada042a25", null ],
    [ "regex_pattern", "struct___remmina_ssh_search.html#a73f856810a9ee2d9c58673e83ba9a4eb", null ],
    [ "reveal_button", "struct___remmina_ssh_search.html#a8f9a64245acf9b02edac4fbbcfdedd02", null ],
    [ "revealer", "struct___remmina_ssh_search.html#a9b72b310751ac148d5c55b0f2a04a300", null ],
    [ "search_entry", "struct___remmina_ssh_search.html#a25f8e8061c69d543d5932bbe8e9b311d", null ],
    [ "search_next_button", "struct___remmina_ssh_search.html#a38b67c5450a7b7f154334d66995ed6d0", null ],
    [ "search_prev_button", "struct___remmina_ssh_search.html#af30971fb9e025ca075cd2a4a857ad57c", null ],
    [ "window", "struct___remmina_ssh_search.html#a15461197d1ae3ebdb827d8b3a95d7662", null ],
    [ "wrap_around_checkbutton", "struct___remmina_ssh_search.html#a3a0be990d7ef8629f8c4caf188b2d723", null ]
];