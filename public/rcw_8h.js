var rcw_8h =
[
    [ "_RemminaConnectionWindow", "struct___remmina_connection_window.html", "struct___remmina_connection_window" ],
    [ "_RemminaConnectionWindowClass", "struct___remmina_connection_window_class.html", "struct___remmina_connection_window_class" ],
    [ "RemminaConnectionObject", "rcw_8h.html#a4c54d77416105f6b094796316f5360a2", null ],
    [ "RemminaConnectionWindow", "rcw_8h.html#af8f33bc09c007b0300868933759dfcb9", null ],
    [ "RemminaConnectionWindowClass", "rcw_8h.html#a5d87c09eb05e6f22618f485e4864368b", null ],
    [ "RemminaConnectionWindowPriv", "rcw_8h.html#a4bfcfdfd9d43712dd05d90ed3fd47dec", null ],
    [ "RemminaConnectionWindowOnDeleteConfirmMode", "rcw_8h.html#a2450dfe27d3ded59370ea879419f307f", [
      [ "RCW_ONDELETE_CONFIRM_IF_2_OR_MORE", "rcw_8h.html#a2450dfe27d3ded59370ea879419f307fae2b5eff3ff03f3633e217a3c31b3b43b", null ],
      [ "RCW_ONDELETE_NOCONFIRM", "rcw_8h.html#a2450dfe27d3ded59370ea879419f307fadd57127d30bc4755bf3a8323045922bc", null ]
    ] ],
    [ "rco_destroy_message_panel", "rcw_8h.html#a96e821dc68caa7f3380a513eb47aead7", null ],
    [ "rco_get_monitor_geometry", "rcw_8h.html#aee06df544a91184cba1fe77c6991065a", null ],
    [ "rco_show_message_panel", "rcw_8h.html#ac5eda3a29c88ac88d80b5775e39ffcb4", null ],
    [ "rcw_delete", "rcw_8h.html#adae469553aa0647d0df5d88b180e7cd7", null ],
    [ "rcw_get_gtkviewport", "rcw_8h.html#a96f1d274dafe5e4953da52beb75f7d14", null ],
    [ "rcw_get_gtkwindow", "rcw_8h.html#affd4a8e1c05f88299990bb0f3d5d1b9d", null ],
    [ "rcw_get_type", "rcw_8h.html#a2e3ebdd3e3450d651445ab54a7113f8b", null ],
    [ "rcw_open_from_file", "rcw_8h.html#a1b7a771392e15e527005305e71cc2546", null ],
    [ "rcw_open_from_file_full", "rcw_8h.html#ad3c2fe67b137c757bd21b79ad5dcc5ff", null ],
    [ "rcw_open_from_filename", "rcw_8h.html#aa37f2a9c56df0ce1a0dbc32f8989e4b5", null ],
    [ "rcw_set_delete_confirm_mode", "rcw_8h.html#a4204fcd726a60493b290dd5590ec693d", null ]
];