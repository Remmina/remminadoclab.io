var remmina__log_8h =
[
    [ "_remmina_audit", "remmina__log_8h.html#a19da9a0d0eac2a2b7fcc926dee2eb368", null ],
    [ "_remmina_critical", "remmina__log_8h.html#a35109f5950931f79c13dac26143a69d3", null ],
    [ "_remmina_debug", "remmina__log_8h.html#a9234814488626cc2513e9fb255a90f53", null ],
    [ "_remmina_error", "remmina__log_8h.html#a984e0494481144aa121893f5d14025f7", null ],
    [ "_remmina_info", "remmina__log_8h.html#a49817b70a398a2f21514fb0066c6e04c", null ],
    [ "_remmina_message", "remmina__log_8h.html#ae439b5fb61045d22c9474b32d0158185", null ],
    [ "_remmina_warning", "remmina__log_8h.html#a8a47ecac307ae1787cdad1ccc4b43d13", null ],
    [ "remmina_log_file_append", "remmina__log_8h.html#a73e0841803c1472516c49fc6ecc47357", null ],
    [ "remmina_log_print", "remmina__log_8h.html#aca8f826a273194e22f4467421af9b3aa", null ],
    [ "remmina_log_printf", "remmina__log_8h.html#ac7848510c1fe1990c992e28cd31af9d8", null ],
    [ "remmina_log_running", "remmina__log_8h.html#a96f5bb72acbdcbf8793b4a5f4a6132b5", null ],
    [ "remmina_log_start", "remmina__log_8h.html#a72cf53183f3ef89c4208fdccb629003d", null ]
];