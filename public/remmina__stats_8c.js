var remmina__stats_8c =
[
    [ "ProfilesData", "struct_profiles_data.html", "struct_profiles_data" ],
    [ "remmina_profiles_get_data", "remmina__stats_8c.html#a1dff2b06d5acefc9bd2f25508ab45328", null ],
    [ "remmina_stats_get_all", "remmina__stats_8c.html#a7398b36347e3621772776c466b565641", null ],
    [ "remmina_stats_get_gtk_backend", "remmina__stats_8c.html#a73fbf1064af209f1f7272d737168971c", null ],
    [ "remmina_stats_get_gtk_version", "remmina__stats_8c.html#aed3a8dd3e01a05d96d6cf3625bb28731", null ],
    [ "remmina_stats_get_indicator", "remmina__stats_8c.html#aee2ea5acceb2fbc3aa4ff21d136c5734", null ],
    [ "remmina_stats_get_kiosk_mode", "remmina__stats_8c.html#a56956b7db0c631121151b19b920176da", null ],
    [ "remmina_stats_get_os_info", "remmina__stats_8c.html#a43133d5e00f481d315ef0d58cb52e70a", null ],
    [ "remmina_stats_get_primary_password_status", "remmina__stats_8c.html#af5a6dc18fe855a6c80fe1dea454ef4f6", null ],
    [ "remmina_stats_get_profiles", "remmina__stats_8c.html#a65500d6b5d93cc7e3fc3d72329ff3847", null ],
    [ "remmina_stats_get_secret_plugin", "remmina__stats_8c.html#afe9ad8b63938e3486cf09ebbbe2e549c", null ],
    [ "remmina_stats_get_user_env", "remmina__stats_8c.html#a42f0554bf8eacb253a023d601cd59648", null ],
    [ "remmina_stats_get_version", "remmina__stats_8c.html#ab248458a3face2634bab909ffeb8291e", null ],
    [ "remmina_stats_get_wm_name", "remmina__stats_8c.html#ae596da7901fb3b45b7290a1d70b3ea3a", null ]
];