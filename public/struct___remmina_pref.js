var struct___remmina_pref =
[
    [ "always_show_notes", "struct___remmina_pref.html#a320861799b86dc076d5f87e3a216eb95", null ],
    [ "always_show_tab", "struct___remmina_pref.html#a6d77a2663675f01670ed7cc2f14e39c9", null ],
    [ "applet_enable_avahi", "struct___remmina_pref.html#abbd08ed0c74b8f216e633aae6b141792", null ],
    [ "applet_hide_count", "struct___remmina_pref.html#acee9d7384a5e3949df53cdf54390bbcd", null ],
    [ "applet_new_ontop", "struct___remmina_pref.html#a999b74e3aab2e7fbc4431e803511b568", null ],
    [ "audit", "struct___remmina_pref.html#a332ae31bb6d7376144aeda9b9e11b0f8", null ],
    [ "auto_scroll_step", "struct___remmina_pref.html#a65a1f2b90d033425fd0c041fccae320c", null ],
    [ "color_pref", "struct___remmina_pref.html#a85e3d3ef34fb8969211860df159a04d8", null ],
    [ "confirm_close", "struct___remmina_pref.html#aa9a057b8d4095678d3c0806ab37af365", null ],
    [ "dark_theme", "struct___remmina_pref.html#a7c139ee405d50330b1d9bfb646c0258c", null ],
    [ "datadir_path", "struct___remmina_pref.html#a920221c2f304bfa278d0944bbbb6e011", null ],
    [ "default_action", "struct___remmina_pref.html#a054a55a2c39181609150cbb4d22413ef", null ],
    [ "default_mode", "struct___remmina_pref.html#ab0928bd64388cb1ced2f542d3b35019d", null ],
    [ "deny_screenshot_clipboard", "struct___remmina_pref.html#ae63c0c0ec8dbfe8bf665221868661b35", null ],
    [ "disable_news", "struct___remmina_pref.html#a19d00f32a342a827abd3548674e357cb", null ],
    [ "disable_stats", "struct___remmina_pref.html#ae01fecc0003fa209b0b0aa24a882360a", null ],
    [ "disable_tip", "struct___remmina_pref.html#aa53a2217327ab79ecf3bd9e817cd0242", null ],
    [ "disable_tray_icon", "struct___remmina_pref.html#afcd157a6f3bddb4b23b4e328cd18358c", null ],
    [ "enc_mode", "struct___remmina_pref.html#a788378c190a4ec0ebcf60c570f3b50d5", null ],
    [ "expanded_group", "struct___remmina_pref.html#aa8d4c923cb9ca1be451436d1790e8ee5", null ],
    [ "floating_toolbar_placement", "struct___remmina_pref.html#ac95fed56e8995e3361928ebf87505f21", null ],
    [ "fullscreen_on_auto", "struct___remmina_pref.html#a4a74d72f34e960155c489e0f91574d2b", null ],
    [ "fullscreen_toolbar_visibility", "struct___remmina_pref.html#a97ad6670df5c399df233f0a74f5c148f", null ],
    [ "grab_color", "struct___remmina_pref.html#a5f5bc6d5bc53adaf815444145242a7df", null ],
    [ "grab_color_switch", "struct___remmina_pref.html#a3d7ca8d4ce2e28daebd72e336adef356", null ],
    [ "hide_connection_toolbar", "struct___remmina_pref.html#a9258f6c3c18f14b4abb148aa43e6bc0e", null ],
    [ "hide_searchbar", "struct___remmina_pref.html#ae293a1fa591d3db78bd587c5af9c9101", null ],
    [ "hide_toolbar", "struct___remmina_pref.html#a8cbf300dc913d51365c93f928e0d02fd", null ],
    [ "hostkey", "struct___remmina_pref.html#a6ae707cecd1f6660d8e2168d7422b797", null ],
    [ "info_uid_prefix", "struct___remmina_pref.html#a8fb0c4f38e7f8222bfdade66332a051c", null ],
    [ "keystrokes", "struct___remmina_pref.html#a086c9b0fe4bcfe0e5349422d17886d65", null ],
    [ "last_quickconnect_protocol", "struct___remmina_pref.html#ac31f888a39eec7ef6de922c7b53d044e", null ],
    [ "list_refresh_workaround", "struct___remmina_pref.html#a994425a04b78282ff40a05580cc9355c", null ],
    [ "lock_connect", "struct___remmina_pref.html#a766ea73df1b7d25866582ec209941de8", null ],
    [ "lock_edit", "struct___remmina_pref.html#a599f643c5a42ffd9c6a382199f147efd", null ],
    [ "lock_view_passwords", "struct___remmina_pref.html#a9da302463dbefeecf192fd3460de1658", null ],
    [ "main_height", "struct___remmina_pref.html#aeb86dc8005c98ce2eab57bd900467875", null ],
    [ "main_maximize", "struct___remmina_pref.html#a8edfd2a9a9a06b1fd876fbe8742b9ff6", null ],
    [ "main_sort_column_id", "struct___remmina_pref.html#ac13f1310a2625b29defdf5b0ba8fd2d0", null ],
    [ "main_sort_order", "struct___remmina_pref.html#a4504684f0e67bb50157fd6862182e2c4", null ],
    [ "main_width", "struct___remmina_pref.html#a6318ff8e2f73d36fb8b35cbed503c222", null ],
    [ "periodic_news_last_checksum", "struct___remmina_pref.html#aa4523648b80418ad5cb7dca12474875f", null ],
    [ "prevent_snap_welcome_message", "struct___remmina_pref.html#a18872e092d22f5d3accb66d6a22845d7", null ],
    [ "recent_maximum", "struct___remmina_pref.html#ab78e34f426da9a37b467efbdf2183644", null ],
    [ "remmina_file_name", "struct___remmina_pref.html#aa1544a98f03088dc260ed98f2deac94f", null ],
    [ "resolutions", "struct___remmina_pref.html#a59eebf1d365ef192de8e14da2c381f37", null ],
    [ "save_view_mode", "struct___remmina_pref.html#aad6660781f636788d5f05b3bac85d676", null ],
    [ "scale_quality", "struct___remmina_pref.html#a095f19f07c5ebeb47a89b1bfe23cc21e", null ],
    [ "screenshot_name", "struct___remmina_pref.html#ae2267a1de362addcee0c5801612dc5d6", null ],
    [ "screenshot_path", "struct___remmina_pref.html#a3df98a686c4e2703455c1a786d3f1f5c", null ],
    [ "secret", "struct___remmina_pref.html#a99a34af002da68036758fab3ceb08259", null ],
    [ "shortcutkey_autofit", "struct___remmina_pref.html#ae5c3a7fd35cc51f658dda67052440b47", null ],
    [ "shortcutkey_clipboard", "struct___remmina_pref.html#a1d61b87017fd39af9803ab744a7329d0", null ],
    [ "shortcutkey_disconnect", "struct___remmina_pref.html#a17bfba7a11836808719d33d5f6edbdc7", null ],
    [ "shortcutkey_dynres", "struct___remmina_pref.html#a2cc56c093d45511af91c6b05c522c45a", null ],
    [ "shortcutkey_fullscreen", "struct___remmina_pref.html#a10b1f3d9b49687c09ec5dd98a0577d7f", null ],
    [ "shortcutkey_grab", "struct___remmina_pref.html#a363642437f5920c93fe35c07ef3601cc", null ],
    [ "shortcutkey_minimize", "struct___remmina_pref.html#a887cbbd379d159cd0ac1acea7173d07b", null ],
    [ "shortcutkey_multimon", "struct___remmina_pref.html#a4f2cf3433d4f689fbe8b250dcfb8148d", null ],
    [ "shortcutkey_nexttab", "struct___remmina_pref.html#a712773f926eb2ec030d6af80cdc33095", null ],
    [ "shortcutkey_prevtab", "struct___remmina_pref.html#a45d70bbebf5163081d67df3dbb73cc1b", null ],
    [ "shortcutkey_scale", "struct___remmina_pref.html#a2859598ef2f87afb5a970a197a503765", null ],
    [ "shortcutkey_screenshot", "struct___remmina_pref.html#a1b5f4704fb50b67ff1e3daaf7d7d3209", null ],
    [ "shortcutkey_toolbar", "struct___remmina_pref.html#aa3dc5f8676c4747f2fcaf9a68d16d11b", null ],
    [ "shortcutkey_viewonly", "struct___remmina_pref.html#a4eb866c8e08104362aa51f14f5894e51", null ],
    [ "small_toolbutton", "struct___remmina_pref.html#a293541cc884cabe4f9b2b3181b3ea9c0", null ],
    [ "ssh_loglevel", "struct___remmina_pref.html#aac4f34f297a653052752b571777bc001", null ],
    [ "ssh_parseconfig", "struct___remmina_pref.html#a1d7deb091e0d51a8d7885ad9e22dc9a7", null ],
    [ "ssh_tcp_keepcnt", "struct___remmina_pref.html#a082f698b8aa7cec3c1cb0fd987d4358f", null ],
    [ "ssh_tcp_keepidle", "struct___remmina_pref.html#a2bf96a40b9f63300d02e158b26546203", null ],
    [ "ssh_tcp_keepintvl", "struct___remmina_pref.html#a793ae9c3e2f88eed85551c85d6832ebd", null ],
    [ "ssh_tcp_usrtimeout", "struct___remmina_pref.html#ad92d70d47d7ad80298efe591955fa8b8", null ],
    [ "sshtunnel_port", "struct___remmina_pref.html#af348bb968fd98ba269c4e253750e2400", null ],
    [ "tab_mode", "struct___remmina_pref.html#aecbe4c6a13ec6eda818c9f74c6f63468", null ],
    [ "toolbar_pin_down", "struct___remmina_pref.html#a62b161b504422d53a766abb3444f8449", null ],
    [ "toolbar_placement", "struct___remmina_pref.html#a0242e9fcc63d6d24257f63fc23077133", null ],
    [ "trust_all", "struct___remmina_pref.html#ae4c194f4711959dd2c96492b9a0e7f61", null ],
    [ "uid", "struct___remmina_pref.html#a49c21f0374af5abd5c3034cbc665a17f", null ],
    [ "unlock_password", "struct___remmina_pref.html#a65b01db52636d3fb896cd8a5c68eb2fc", null ],
    [ "unlock_repassword", "struct___remmina_pref.html#ac001e6ac3d70ae6adca7d918898a9651", null ],
    [ "unlock_timeout", "struct___remmina_pref.html#aef2fe0f203dd68ed30d12e3b22a4f972", null ],
    [ "use_primary_password", "struct___remmina_pref.html#a70f73ed240c2ce813dd9fa66fd1a6677", null ],
    [ "view_file_mode", "struct___remmina_pref.html#ab43c3da7d8551c9df726996c540726f1", null ],
    [ "vte_allow_bold_text", "struct___remmina_pref.html#a52f5fa8b2b4500c3ab9ae35f0d410f17", null ],
    [ "vte_font", "struct___remmina_pref.html#ac342b87e3992a20833d39e864e8ab85b", null ],
    [ "vte_lines", "struct___remmina_pref.html#af9c8b60418a9dc795230e4ebb084f850", null ],
    [ "vte_shortcutkey_copy", "struct___remmina_pref.html#a579fae9094500d743c26d03f4c34d457", null ],
    [ "vte_shortcutkey_decrease_font", "struct___remmina_pref.html#a2ae608a877b349b1af50c2456afda972", null ],
    [ "vte_shortcutkey_increase_font", "struct___remmina_pref.html#a90e0cfac442b9e50e0f4857b3ff74255", null ],
    [ "vte_shortcutkey_paste", "struct___remmina_pref.html#a901034949132f369eb34c4f1ded3a0a6", null ],
    [ "vte_shortcutkey_search_text", "struct___remmina_pref.html#a3e3d20bf883608e3ff133901d2f4e8d5", null ],
    [ "vte_shortcutkey_select_all", "struct___remmina_pref.html#a9c5171c5d8a61cc48171065644613cad", null ],
    [ "vte_system_colors", "struct___remmina_pref.html#a57e104c8ff60943f4a03e2e15c6b2a22", null ]
];