var structremmina__plugin__rdp__event =
[
    [ "clipboard_formatdatarequest", "structremmina__plugin__rdp__event.html#a29c0bb85b92dc8cc94ea8b59c291c392", null ],
    [ "clipboard_formatdataresponse", "structremmina__plugin__rdp__event.html#acf991d0d42d24dc66abf2dcf56641dd9", null ],
    [ "clipboard_formatlist", "structremmina__plugin__rdp__event.html#a8eb42faccc9713d98f845331a474ebfa", null ],
    [ "data", "structremmina__plugin__rdp__event.html#af9cd4e060d42600c8a9089b04612a624", null ],
    [ "desktopOrientation", "structremmina__plugin__rdp__event.html#acc5ea94958152ae483d269a28f34b0b0", null ],
    [ "desktopScaleFactor", "structremmina__plugin__rdp__event.html#ad8c508c65e9f84c92f2f817eb7be2985", null ],
    [ "deviceScaleFactor", "structremmina__plugin__rdp__event.html#a30115ccc01062368d9b1189a0f378f57", null ],
    [ "extended", "structremmina__plugin__rdp__event.html#ae1093ce4f4e4e4b632ca4704ecfa698e", null ],
    [ "extended1", "structremmina__plugin__rdp__event.html#ac8c805908d492afeaeb6ef6ce6fad218", null ],
    [ "flags", "structremmina__plugin__rdp__event.html#a7cee906e36975da3aadf6874da5da49c", null ],
    [ "Flags", "structremmina__plugin__rdp__event.html#ab1ee38a965bda260c81a55eedbd97114", null ],
    [ "height", "structremmina__plugin__rdp__event.html#a0ff77bb8ed355ac55a7dc4ab28a02649", null ],
    [ "key_code", "structremmina__plugin__rdp__event.html#a6c91276b122d70b8e723e046f6dbd151", null ],
    [ "key_event", "structremmina__plugin__rdp__event.html#a75a455645ae176d1cec7cb36284369bc", null ],
    [ "Left", "structremmina__plugin__rdp__event.html#a2822d7acc35ca8ecc5afb4c44b1ac117", null ],
    [ "monitor_layout", "structremmina__plugin__rdp__event.html#a58d252211b2e96735b4162cc600954e2", null ],
    [ "mouse_event", "structremmina__plugin__rdp__event.html#aa4b2f7703b0355e5d570442b9878ece9", null ],
    [ "pFormatDataRequest", "structremmina__plugin__rdp__event.html#ad3b01f14f235b582b54aa9a0057d5c1c", null ],
    [ "pFormatList", "structremmina__plugin__rdp__event.html#a02c2d40efeac883dcc6294be9782e082", null ],
    [ "physicalHeight", "structremmina__plugin__rdp__event.html#aab8e2356f6619d11ecc80b8b767ca815", null ],
    [ "physicalWidth", "structremmina__plugin__rdp__event.html#af62c17a8001e045d3f9e781a0a5089ef", null ],
    [ "size", "structremmina__plugin__rdp__event.html#a81b19d4696e8a38139182113d5d741ba", null ],
    [ "Top", "structremmina__plugin__rdp__event.html#a880c2311bdcc07c03842427490da7764", null ],
    [ "type", "structremmina__plugin__rdp__event.html#a84797e0cdf4484432740a8946325f126", null ],
    [ "unicode_code", "structremmina__plugin__rdp__event.html#a7a1c9ce96d50f8902f7504bbfd0a057b", null ],
    [ "up", "structremmina__plugin__rdp__event.html#ab834752f9089cbc4a8841fdc037aa094", null ],
    [ "width", "structremmina__plugin__rdp__event.html#ac8972b1def0a956b7c36534da0e56b3b", null ],
    [ "x", "structremmina__plugin__rdp__event.html#a2f6b6cb00f2511b7849654b026cfd105", null ],
    [ "y", "structremmina__plugin__rdp__event.html#af02b51a258600a3c4f6d57e868dc39cf", null ]
];