var remmina__pref__dialog_8h =
[
    [ "_RemminaPrefDialogPriv", "struct___remmina_pref_dialog_priv.html", "struct___remmina_pref_dialog_priv" ],
    [ "_RemminaPrefDialog", "struct___remmina_pref_dialog.html", "struct___remmina_pref_dialog" ],
    [ "RemminaPrefDialog", "remmina__pref__dialog_8h.html#ae6ce150f9449c96617eafffb4485017a", null ],
    [ "RemminaPrefDialogPriv", "remmina__pref__dialog_8h.html#abbda37c3b2fbcaff9aac6a2d396993bf", null ],
    [ "remmina_pref_dialog_get_dialog", "remmina__pref__dialog_8h.html#a73b901ed8ee7e318b512f716065adb29", null ],
    [ "remmina_pref_dialog_new", "remmina__pref__dialog_8h.html#ad54ed89678b180a685af40b6226d1458", null ],
    [ "remmina_pref_dialog_on_action_close", "remmina__pref__dialog_8h.html#a722371d3ad01079279d5e86d7c8400e1", null ],
    [ "remmina_prefdiag_unlock_repwd_on_changed", "remmina__pref__dialog_8h.html#a19ba9bbbff353b8e36810d423e6f7e8f", null ]
];