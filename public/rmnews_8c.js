var rmnews_8c =
[
    [ "rmnews_close_clicked", "rmnews_8c.html#aaa3ec81813c3b38be1f75885e6005a00", null ],
    [ "rmnews_defaultcl_on_click", "rmnews_8c.html#a7825b2c9facea97439f5780d83e86798", null ],
    [ "rmnews_dialog_deleted", "rmnews_8c.html#adf38ed099d3ac477aea7e3bbd6d95bd4", null ],
    [ "rmnews_get_file_contents", "rmnews_8c.html#a11d7277ba911522ac40656774cbe41b9", null ],
    [ "rmnews_get_news", "rmnews_8c.html#a26cb497eeb30a956dcdc5790b393a301", null ],
    [ "rmnews_get_uid", "rmnews_8c.html#a59416d6e8999802e8b145b0338331081", null ],
    [ "rmnews_get_url", "rmnews_8c.html#a341ebc424e040f8f8a2e5e46b1920c7e", null ],
    [ "rmnews_get_url_cb", "rmnews_8c.html#a9a0c1177e67a08c991e5344196ebac01", null ],
    [ "rmnews_get_url_cb", "rmnews_8c.html#aa360ccd2808c76aab98c1fc4bc336c38", null ],
    [ "rmnews_news_switch_state_set_cb", "rmnews_8c.html#a0b8a063b164dbf0798982daa9399a793", null ],
    [ "rmnews_on_stream_splice", "rmnews_8c.html#a78c63694cb6197f185242acac0ea1575", null ],
    [ "rmnews_periodic_check", "rmnews_8c.html#a88b2d9ce9e7bc64373e2eb7f904a0814", null ],
    [ "rmnews_schedule", "rmnews_8c.html#a8433da15acb3138804b4816189e8fc3a", null ],
    [ "rmnews_show_news", "rmnews_8c.html#af58ee02676e8c920a6e9b4676bb86abe", null ],
    [ "eweekdays", "rmnews_8c.html#af4777302c760225b833206545e9f437d", null ],
    [ "output_file_path", "rmnews_8c.html#a565782499953048b5e51934123195e29", null ],
    [ "rmnews_news_dialog", "rmnews_8c.html#ae1bb737d0381441eebbf3652b3ef416d", null ],
    [ "session", "rmnews_8c.html#a175c6dbec04bf34e3f2a5bd791d2d336", null ],
    [ "supported_mime_types", "rmnews_8c.html#a8ebbcba2c07af0938b3c29e316747d12", null ]
];