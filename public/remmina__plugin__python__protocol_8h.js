var remmina__plugin__python__protocol_8h =
[
    [ "PyRemminaProtocolFeature", "struct_py_remmina_protocol_feature.html", "struct_py_remmina_protocol_feature" ],
    [ "PyRemminaPluginScreenshotData", "struct_py_remmina_plugin_screenshot_data.html", "struct_py_remmina_plugin_screenshot_data" ],
    [ "remmina_plugin_python_create_protocol_plugin", "remmina__plugin__python__protocol_8h.html#a5a0da2bf9171882bf83fba5cb0e0e51e", null ],
    [ "remmina_plugin_python_protocol_feature_new", "remmina__plugin__python__protocol_8h.html#abdd6fa4e5a04f103b3ace3b8bdd3ac89", null ],
    [ "remmina_plugin_python_protocol_init", "remmina__plugin__python__protocol_8h.html#a51c80290e7edac79d637e33f985bab02", null ],
    [ "remmina_plugin_python_screenshot_data_new", "remmina__plugin__python__protocol_8h.html#ac1c6a37ae2dd655cc5cf710412fb590e", null ]
];