var structrf__clipboard =
[
    [ "clientformatdatarequest_tv", "structrf__clipboard.html#af85bab349549714dc18a8938ac60926d", null ],
    [ "clipboard_handler", "structrf__clipboard.html#a52ca364a1902d7f4095234923feed724", null ],
    [ "context", "structrf__clipboard.html#a773214c0bb4f351a609a085f786304aa", null ],
    [ "format", "structrf__clipboard.html#ad22c0f3cd325db2746e9ca42cca0d65b", null ],
    [ "requestedFormatId", "structrf__clipboard.html#a66caa2e83dfdc4655df71d81502f93e3", null ],
    [ "rfi", "structrf__clipboard.html#a268b5336ae92d8d2f54c5ee441325d68", null ],
    [ "server_html_format_id", "structrf__clipboard.html#a19d94ab0c58925408c8c7663dfd180ee", null ],
    [ "srv_clip_data_wait", "structrf__clipboard.html#ab7fe54d6efef77df8bb0cdd65a2fcdd7", null ],
    [ "srv_data", "structrf__clipboard.html#ae14721fd73459cc2be441c96f5a96be7", null ],
    [ "srv_data_mutex", "structrf__clipboard.html#a267035cf20278f9df7cddb7e8fcce589", null ],
    [ "system", "structrf__clipboard.html#ad7541d40ba2e4463c36ffab12cfe3adf", null ],
    [ "transfer_clip_cond", "structrf__clipboard.html#acf5bf9da1a18e21409c4b10368f8dfd7", null ],
    [ "transfer_clip_mutex", "structrf__clipboard.html#a5c6be1637ed9e2ce72ffde2ed5400205", null ]
];