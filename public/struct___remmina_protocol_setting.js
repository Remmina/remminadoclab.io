var struct___remmina_protocol_setting =
[
    [ "compact", "struct___remmina_protocol_setting.html#ab434990a6fe70b7e8a4a8b11129527a6", null ],
    [ "label", "struct___remmina_protocol_setting.html#a7a5b063deb126422c461203c074a3dc4", null ],
    [ "name", "struct___remmina_protocol_setting.html#ac4cd339607204c5f478727e2a0c04857", null ],
    [ "opt1", "struct___remmina_protocol_setting.html#ae1cc5b9d910f68aa5909e3cf483c1d1d", null ],
    [ "opt2", "struct___remmina_protocol_setting.html#a55a12c6ed9ab21954208e0e83165f770", null ],
    [ "type", "struct___remmina_protocol_setting.html#a6274d44b592a54162ff583135e55ffc9", null ],
    [ "validator", "struct___remmina_protocol_setting.html#a94df87db138d4e44e81b3e6c99148ed3", null ],
    [ "validator_data", "struct___remmina_protocol_setting.html#a6086b43049072a3a3245c4a45f43dbc5", null ]
];