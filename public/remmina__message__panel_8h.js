var remmina__message__panel_8h =
[
    [ "RemminaMessagePanelCallback", "remmina__message__panel_8h.html#ae2923c94c1caf1ac4a7eb48361d2670a", null ],
    [ "G_DECLARE_DERIVABLE_TYPE", "remmina__message__panel_8h.html#a558c1459208a2a314734a1cf92e8e156", null ],
    [ "remmina_message_panel_field_get_filename", "remmina__message__panel_8h.html#ab89dba73bc6832c6c5061a0d8bd0fbb4", null ],
    [ "remmina_message_panel_field_get_string", "remmina__message__panel_8h.html#a44054d029d4615092ae8a2ee1bf81db4", null ],
    [ "remmina_message_panel_field_get_switch_state", "remmina__message__panel_8h.html#a80bc2fcb39c236da739593c2134c73fa", null ],
    [ "remmina_message_panel_field_set_filename", "remmina__message__panel_8h.html#aec9dcd96d0148fec9cdc9e94cc8dc1ff", null ],
    [ "remmina_message_panel_field_set_string", "remmina__message__panel_8h.html#a0e20944d484c95036eeb6b5d7775c901", null ],
    [ "remmina_message_panel_field_set_switch", "remmina__message__panel_8h.html#a67a48217d44350e028ed95b1b55f0b0f", null ],
    [ "remmina_message_panel_focus_auth_entry", "remmina__message__panel_8h.html#aacb1f47e5ad88086c1bda1b0a156c34d", null ],
    [ "remmina_message_panel_new", "remmina__message__panel_8h.html#adf4a7707360c836ba312a2c7286db294", null ],
    [ "remmina_message_panel_response", "remmina__message__panel_8h.html#af7db7b9f49fe6b83b17471116d363d40", null ],
    [ "remmina_message_panel_setup_auth", "remmina__message__panel_8h.html#ae2cec8bc8216154388727b7e35200667", null ],
    [ "remmina_message_panel_setup_auth_x509", "remmina__message__panel_8h.html#ac10f450f856ca7d9c0b6896261e9c407", null ],
    [ "remmina_message_panel_setup_message", "remmina__message__panel_8h.html#ae01d27ae9f678dc5a4fa32f0b401f434", null ],
    [ "remmina_message_panel_setup_progress", "remmina__message__panel_8h.html#afae4072b7d8b54392ec08da7d6ead620", null ],
    [ "remmina_message_panel_setup_question", "remmina__message__panel_8h.html#ae5b1825b5d4e56ecc2f25c28bb042c32", null ]
];