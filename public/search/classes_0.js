var searchData=
[
  ['_5fconnectiondata_3574',['_ConnectionData',['../struct___connection_data.html',1,'']]],
  ['_5fdialogdata_3575',['_DialogData',['../struct___dialog_data.html',1,'']]],
  ['_5fgvncplugindata_3576',['_GVncPluginData',['../struct___g_vnc_plugin_data.html',1,'']]],
  ['_5fpygclosure_3577',['_PyGClosure',['../struct___py_g_closure.html',1,'']]],
  ['_5fpygobject_5ffunctions_3578',['_PyGObject_Functions',['../struct___py_g_object___functions.html',1,'']]],
  ['_5fremminaappletmenu_3579',['_RemminaAppletMenu',['../struct___remmina_applet_menu.html',1,'']]],
  ['_5fremminaappletmenuclass_3580',['_RemminaAppletMenuClass',['../struct___remmina_applet_menu_class.html',1,'']]],
  ['_5fremminaappletmenuitem_3581',['_RemminaAppletMenuItem',['../struct___remmina_applet_menu_item.html',1,'']]],
  ['_5fremminaappletmenuitemclass_3582',['_RemminaAppletMenuItemClass',['../struct___remmina_applet_menu_item_class.html',1,'']]],
  ['_5fremminaavahi_3583',['_RemminaAvahi',['../struct___remmina_avahi.html',1,'']]],
  ['_5fremminaavahipriv_3584',['_RemminaAvahiPriv',['../struct___remmina_avahi_priv.html',1,'']]],
  ['_5fremminabugreportdialog_3585',['_RemminaBugReportDialog',['../struct___remmina_bug_report_dialog.html',1,'']]],
  ['_5fremminacellrendererpixbuf_3586',['_RemminaCellRendererPixbuf',['../struct___remmina_cell_renderer_pixbuf.html',1,'']]],
  ['_5fremminacellrendererpixbufclass_3587',['_RemminaCellRendererPixbufClass',['../struct___remmina_cell_renderer_pixbuf_class.html',1,'']]],
  ['_5fremminachatwindow_3588',['_RemminaChatWindow',['../struct___remmina_chat_window.html',1,'']]],
  ['_5fremminachatwindowclass_3589',['_RemminaChatWindowClass',['../struct___remmina_chat_window_class.html',1,'']]],
  ['_5fremminacolorpref_3590',['_RemminaColorPref',['../struct___remmina_color_pref.html',1,'']]],
  ['_5fremminaconnectionobject_3591',['_RemminaConnectionObject',['../struct___remmina_connection_object.html',1,'']]],
  ['_5fremminaconnectionwindow_3592',['_RemminaConnectionWindow',['../struct___remmina_connection_window.html',1,'']]],
  ['_5fremminaconnectionwindowclass_3593',['_RemminaConnectionWindowClass',['../struct___remmina_connection_window_class.html',1,'']]],
  ['_5fremminaentryplugin_3594',['_RemminaEntryPlugin',['../struct___remmina_entry_plugin.html',1,'']]],
  ['_5fremminafile_3595',['_RemminaFile',['../struct___remmina_file.html',1,'']]],
  ['_5fremminafileeditor_3596',['_RemminaFileEditor',['../struct___remmina_file_editor.html',1,'']]],
  ['_5fremminafileeditorclass_3597',['_RemminaFileEditorClass',['../struct___remmina_file_editor_class.html',1,'']]],
  ['_5fremminafileplugin_3598',['_RemminaFilePlugin',['../struct___remmina_file_plugin.html',1,'']]],
  ['_5fremminaftpclient_3599',['_RemminaFTPClient',['../struct___remmina_f_t_p_client.html',1,'']]],
  ['_5fremminaftpclientclass_3600',['_RemminaFTPClientClass',['../struct___remmina_f_t_p_client_class.html',1,'']]],
  ['_5fremminaftptask_3601',['_RemminaFTPTask',['../struct___remmina_f_t_p_task.html',1,'']]],
  ['_5fremminagroupdata_3602',['_RemminaGroupData',['../struct___remmina_group_data.html',1,'']]],
  ['_5fremminaicon_3603',['_RemminaIcon',['../struct___remmina_icon.html',1,'']]],
  ['_5fremminainfodialog_3604',['_RemminaInfoDialog',['../struct___remmina_info_dialog.html',1,'']]],
  ['_5fremminainfomessage_3605',['_RemminaInfoMessage',['../struct___remmina_info_message.html',1,'']]],
  ['_5fremminakeychooserarguments_3606',['_RemminaKeyChooserArguments',['../struct___remmina_key_chooser_arguments.html',1,'']]],
  ['_5fremminakeyval_3607',['_RemminaKeyVal',['../struct___remmina_key_val.html',1,'']]],
  ['_5fremminalanguagewrapperplugin_3608',['_RemminaLanguageWrapperPlugin',['../struct___remmina_language_wrapper_plugin.html',1,'']]],
  ['_5fremminalogwindow_3609',['_RemminaLogWindow',['../struct___remmina_log_window.html',1,'']]],
  ['_5fremminalogwindowclass_3610',['_RemminaLogWindowClass',['../struct___remmina_log_window_class.html',1,'']]],
  ['_5fremminamain_3611',['_RemminaMain',['../struct___remmina_main.html',1,'']]],
  ['_5fremminamainpriv_3612',['_RemminaMainPriv',['../struct___remmina_main_priv.html',1,'']]],
  ['_5fremminamonitor_3613',['_RemminaMonitor',['../struct___remmina_monitor.html',1,'']]],
  ['_5fremminapasswddialog_3614',['_RemminaPasswdDialog',['../struct___remmina_passwd_dialog.html',1,'']]],
  ['_5fremminaplugin_3615',['_RemminaPlugin',['../struct___remmina_plugin.html',1,'']]],
  ['_5fremminapluginexecdata_3616',['_RemminaPluginExecData',['../struct___remmina_plugin_exec_data.html',1,'']]],
  ['_5fremminapluginrdpsetgrid_3617',['_RemminaPluginRdpsetGrid',['../struct___remmina_plugin_rdpset_grid.html',1,'']]],
  ['_5fremminapluginrdpsetgridclass_3618',['_RemminaPluginRdpsetGridClass',['../struct___remmina_plugin_rdpset_grid_class.html',1,'']]],
  ['_5fremminapluginscreenshotdata_3619',['_RemminaPluginScreenshotData',['../struct___remmina_plugin_screenshot_data.html',1,'']]],
  ['_5fremminapluginservice_3620',['_RemminaPluginService',['../struct___remmina_plugin_service.html',1,'']]],
  ['_5fremminapluginsftpdata_3621',['_RemminaPluginSftpData',['../struct___remmina_plugin_sftp_data.html',1,'']]],
  ['_5fremminapluginspicedata_3622',['_RemminaPluginSpiceData',['../struct___remmina_plugin_spice_data.html',1,'']]],
  ['_5fremminapluginspicexferwidgets_3623',['_RemminaPluginSpiceXferWidgets',['../struct___remmina_plugin_spice_xfer_widgets.html',1,'']]],
  ['_5fremminapluginsshdata_3624',['_RemminaPluginSshData',['../struct___remmina_plugin_ssh_data.html',1,'']]],
  ['_5fremminapluginvnccoordinates_3625',['_RemminaPluginVncCoordinates',['../struct___remmina_plugin_vnc_coordinates.html',1,'']]],
  ['_5fremminapluginvnccuttextparam_3626',['_RemminaPluginVncCuttextParam',['../struct___remmina_plugin_vnc_cuttext_param.html',1,'']]],
  ['_5fremminapluginvncdata_3627',['_RemminaPluginVncData',['../struct___remmina_plugin_vnc_data.html',1,'']]],
  ['_5fremminapluginvncevent_3628',['_RemminaPluginVncEvent',['../struct___remmina_plugin_vnc_event.html',1,'']]],
  ['_5fremminapluginwwwdata_3629',['_RemminaPluginWWWData',['../struct___remmina_plugin_w_w_w_data.html',1,'']]],
  ['_5fremminapluginx2godata_3630',['_RemminaPluginX2GoData',['../struct___remmina_plugin_x2_go_data.html',1,'']]],
  ['_5fremminapref_3631',['_RemminaPref',['../struct___remmina_pref.html',1,'']]],
  ['_5fremminaprefdialog_3632',['_RemminaPrefDialog',['../struct___remmina_pref_dialog.html',1,'']]],
  ['_5fremminaprefdialogpriv_3633',['_RemminaPrefDialogPriv',['../struct___remmina_pref_dialog_priv.html',1,'']]],
  ['_5fremminaprefplugin_3634',['_RemminaPrefPlugin',['../struct___remmina_pref_plugin.html',1,'']]],
  ['_5fremminaprotocolfeature_3635',['_RemminaProtocolFeature',['../struct___remmina_protocol_feature.html',1,'']]],
  ['_5fremminaprotocolplugin_3636',['_RemminaProtocolPlugin',['../struct___remmina_protocol_plugin.html',1,'']]],
  ['_5fremminaprotocolsetting_3637',['_RemminaProtocolSetting',['../struct___remmina_protocol_setting.html',1,'']]],
  ['_5fremminaprotocolsettingopt_3638',['_RemminaProtocolSettingOpt',['../struct___remmina_protocol_setting_opt.html',1,'']]],
  ['_5fremminaprotocolwidget_3639',['_RemminaProtocolWidget',['../struct___remmina_protocol_widget.html',1,'']]],
  ['_5fremminaprotocolwidgetclass_3640',['_RemminaProtocolWidgetClass',['../struct___remmina_protocol_widget_class.html',1,'']]],
  ['_5fremminaprotocolwidgetpriv_3641',['_RemminaProtocolWidgetPriv',['../struct___remmina_protocol_widget_priv.html',1,'']]],
  ['_5fremminaprotocolwidgetsignaldata_3642',['_RemminaProtocolWidgetSignalData',['../struct___remmina_protocol_widget_signal_data.html',1,'']]],
  ['_5fremminascrolledviewport_3643',['_RemminaScrolledViewport',['../struct___remmina_scrolled_viewport.html',1,'']]],
  ['_5fremminascrolledviewportclass_3644',['_RemminaScrolledViewportClass',['../struct___remmina_scrolled_viewport_class.html',1,'']]],
  ['_5fremminasecretplugin_3645',['_RemminaSecretPlugin',['../struct___remmina_secret_plugin.html',1,'']]],
  ['_5fremminaserverpluginresponse_3646',['_RemminaServerPluginResponse',['../struct___remmina_server_plugin_response.html',1,'']]],
  ['_5fremminasftp_3647',['_RemminaSFTP',['../struct___remmina_s_f_t_p.html',1,'']]],
  ['_5fremminasftpclient_3648',['_RemminaSFTPClient',['../struct___remmina_s_f_t_p_client.html',1,'']]],
  ['_5fremminasftpclientclass_3649',['_RemminaSFTPClientClass',['../struct___remmina_s_f_t_p_client_class.html',1,'']]],
  ['_5fremminassh_3650',['_RemminaSSH',['../struct___remmina_s_s_h.html',1,'']]],
  ['_5fremminasshsearch_3651',['_RemminaSshSearch',['../struct___remmina_ssh_search.html',1,'']]],
  ['_5fremminasshshell_3652',['_RemminaSSHShell',['../struct___remmina_s_s_h_shell.html',1,'']]],
  ['_5fremminasshtunnel_3653',['_RemminaSSHTunnel',['../struct___remmina_s_s_h_tunnel.html',1,'']]],
  ['_5fremminastringlist_3654',['_RemminaStringList',['../struct___remmina_string_list.html',1,'']]],
  ['_5fremminastringlistpriv_3655',['_RemminaStringListPriv',['../struct___remmina_string_list_priv.html',1,'']]],
  ['_5fremminatoolplugin_3656',['_RemminaToolPlugin',['../struct___remmina_tool_plugin.html',1,'']]],
  ['_5fremminatpchannelhandler_3657',['_RemminaTpChannelHandler',['../struct___remmina_tp_channel_handler.html',1,'']]],
  ['_5fremminatphandler_3658',['_RemminaTpHandler',['../struct___remmina_tp_handler.html',1,'']]],
  ['_5fremminatphandlerclass_3659',['_RemminaTpHandlerClass',['../struct___remmina_tp_handler_class.html',1,'']]],
  ['_5fremminaunlockdialog_3660',['_RemminaUnlockDialog',['../struct___remmina_unlock_dialog.html',1,'']]],
  ['_5fx2gocustomuserdata_3661',['_X2GoCustomUserData',['../struct___x2_go_custom_user_data.html',1,'']]]
];
