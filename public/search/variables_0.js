var searchData=
[
  ['_5f_5flast_5fresult_5445',['__last_result',['../python__wrapper__common_8c.html#af9dda67526060ddd963ed23ce80ee6ca',1,'python_wrapper_common.c']]],
  ['_5fpygobject_5fapi_5446',['_PyGObject_API',['../src_2pygobject_8h.html#a2fa9a05e0ca89774ce56238aa81a6cac',1,'_PyGObject_API():&#160;pygobject.h'],['../plugins_2python__wrapper_2pygobject_8h.html#adac0c88b46c6dfc5074c0306128b1488',1,'_PyGObject_API():&#160;pygobject.h']]],
  ['_5fremmina_5faudit_5447',['_remmina_audit',['../struct___remmina_plugin_service.html#aecf866eced70d5e15969149920337314',1,'_RemminaPluginService']]],
  ['_5fremmina_5fcritical_5448',['_remmina_critical',['../struct___remmina_plugin_service.html#a6421cc4cd8c2175d769ba5c24a879517',1,'_RemminaPluginService']]],
  ['_5fremmina_5fdebug_5449',['_remmina_debug',['../struct___remmina_plugin_service.html#a588f2c328c535384a0adaac027c4df72',1,'_RemminaPluginService']]],
  ['_5fremmina_5ferror_5450',['_remmina_error',['../struct___remmina_plugin_service.html#a48651c1147371e7e9069eb1fd06a9e4f',1,'_RemminaPluginService']]],
  ['_5fremmina_5finfo_5451',['_remmina_info',['../struct___remmina_plugin_service.html#a5cb9cccadb5fea7d9925e10ad4c14316',1,'_RemminaPluginService']]],
  ['_5fremmina_5fmessage_5452',['_remmina_message',['../struct___remmina_plugin_service.html#a810268861a25b7794619acc0a8a15209',1,'_RemminaPluginService']]],
  ['_5fremmina_5fwarning_5453',['_remmina_warning',['../struct___remmina_plugin_service.html#a99ea58f6bc0e2a67cc756e66f954c9b8',1,'_RemminaPluginService']]]
];
