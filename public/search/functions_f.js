var searchData=
[
  ['on_5fexport_5fsave_5fresponse_4032',['on_export_save_response',['../remmina__main_8c.html#a98eaf5fff6c69f0caee9b00557e02710',1,'remmina_main.c']]],
  ['onmainthread_5fcb_4033',['onMainThread_cb',['../vnc__plugin_8c.html#a9ba8e968998b64b3832e484a2ca7dfc1',1,'onMainThread_cb(struct onMainThread_cb_data *d):&#160;vnc_plugin.c'],['../x2go__plugin_8c.html#a9ba8e968998b64b3832e484a2ca7dfc1',1,'onMainThread_cb(struct onMainThread_cb_data *d):&#160;x2go_plugin.c']]],
  ['onmainthread_5fcleanup_5fhandler_4034',['onMainThread_cleanup_handler',['../vnc__plugin_8c.html#a5dea604d34a804e23a5cd8a6683d9e49',1,'onMainThread_cleanup_handler(gpointer data):&#160;vnc_plugin.c'],['../x2go__plugin_8c.html#a5dea604d34a804e23a5cd8a6683d9e49',1,'onMainThread_cleanup_handler(gpointer data):&#160;x2go_plugin.c']]],
  ['onmainthread_5fgtk_5fsocket_5fadd_5fid_4035',['onMainThread_gtk_socket_add_id',['../x2go__plugin_8c.html#a31b9185763acddffe0bbdef41380bf43',1,'x2go_plugin.c']]],
  ['onmainthread_5fschedule_5fcallback_5fand_5fwait_4036',['onMainThread_schedule_callback_and_wait',['../vnc__plugin_8c.html#a390e3b371156197feb518b737a31cc53',1,'onMainThread_schedule_callback_and_wait(struct onMainThread_cb_data *d):&#160;vnc_plugin.c'],['../x2go__plugin_8c.html#a390e3b371156197feb518b737a31cc53',1,'onMainThread_schedule_callback_and_wait(struct onMainThread_cb_data *d):&#160;x2go_plugin.c']]],
  ['open_5fconnection_5flast_5fstage_4037',['open_connection_last_stage',['../rcw_8c.html#a80f465cb910660ded402fb0e7bb3a7fd',1,'rcw.c']]]
];
