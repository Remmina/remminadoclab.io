var searchData=
[
  ['raw_6273',['raw',['../struct_py_generic.html#a1c5857c0a7ee53cbcdcd009b4d9900e7',1,'PyGeneric']]],
  ['rcbutton_6274',['rcbutton',['../structremmina__protocol__widget__dialog__mt__data__t.html#a29b8f44a7f4514422d3a5e04e0b8f0b5',1,'remmina_protocol_widget_dialog_mt_data_t']]],
  ['rcw_5fopen_5ffrom_5ffile_5ffull_6275',['rcw_open_from_file_full',['../struct___remmina_plugin_service.html#abd51e82b9296d57abd324aecbf243137',1,'_RemminaPluginService']]],
  ['rcw_5fsignals_6276',['rcw_signals',['../rcw_8c.html#a2ea4e41deacf11e9b9b71e7eac06869e',1,'rcw.c']]],
  ['rdp_5fkeyboard_5flayout_6277',['rdp_keyboard_layout',['../rdp__settings_8c.html#a526c9f77caff8ae5ba1f00e7c94c7444',1,'rdp_settings.c']]],
  ['rdp_5fkeyboard_5fremapping_5flist_6278',['rdp_keyboard_remapping_list',['../rdp__settings_8c.html#a66cee83de41d0ca01cf8bd899da04870',1,'rdp_settings.c']]],
  ['rdpdr_5fdata_6279',['rdpdr_data',['../structrf__context.html#a93dd55d1f081496959f1051d04cc3ba4',1,'rf_context']]],
  ['rdpgfxchan_6280',['rdpgfxchan',['../structrf__context.html#ab5ffc4d8ba9d3d8a298414dc8e4efd2a',1,'rf_context']]],
  ['rdpsnd_5foptions_6281',['rdpsnd_options',['../structrf__context.html#a0ae7b9fee3a645d77cf90c8a22f0d90f',1,'rf_context']]],
  ['reachable_6282',['reachable',['../struct___remmina_monitor.html#aba786669dc689e081c5bae5b5f202b47',1,'_RemminaMonitor']]],
  ['reasonable_5flimit_5ffor_5fmalloc_6283',['REASONABLE_LIMIT_FOR_MALLOC',['../python__wrapper__common_8c.html#ac83725232721f3acb210665af52c74b3',1,'python_wrapper_common.c']]],
  ['recent_5fmaximum_6284',['recent_maximum',['../struct___remmina_pref.html#ab78e34f426da9a37b467efbdf2183644',1,'_RemminaPref']]],
  ['reconnect_5fattempts_6285',['reconnect_attempts',['../struct___remmina_plugin_rdpset_grid.html#adc95f8c8732eb300f3f12e40564a27a7',1,'_RemminaPluginRdpsetGrid']]],
  ['reconnect_5fmaxattempts_6286',['reconnect_maxattempts',['../structrf__context.html#aa0b949ab1e2f06b7d5efeccc924efaee',1,'rf_context']]],
  ['reconnect_5fnattempt_6287',['reconnect_nattempt',['../structrf__context.html#a4c35800dba8da687f7f56e3bb7a23413',1,'rf_context']]],
  ['reg_6288',['reg',['../structremmina__plugin__rdp__ui__object.html#a46b74b0f00b79d0fb4b0b0b147260b80',1,'remmina_plugin_rdp_ui_object']]],
  ['regex_5fcaseless_6289',['regex_caseless',['../struct___remmina_ssh_search.html#af33585cf803ea86f6e1dccc057e2cce0',1,'_RemminaSshSearch']]],
  ['regex_5fcheckbutton_6290',['regex_checkbutton',['../struct___remmina_ssh_search.html#a244e121887afe8fa1624be3ada042a25',1,'_RemminaSshSearch']]],
  ['regex_5fpattern_6291',['regex_pattern',['../struct___remmina_ssh_search.html#a73f856810a9ee2d9c58673e83ba9a4eb',1,'_RemminaSshSearch']]],
  ['register_5fboxed_6292',['register_boxed',['../struct___py_g_object___functions.html#a7f4ca95af3425d2c3f9ee00e8c1fb11a',1,'_PyGObject_Functions']]],
  ['register_5fclass_6293',['register_class',['../struct___py_g_object___functions.html#ad435a76d97cb1f3470701aa423a89dd6',1,'_PyGObject_Functions']]],
  ['register_5fclass_5finit_6294',['register_class_init',['../struct___py_g_object___functions.html#aa0a2fc985d201ab7412bd2396c428566',1,'_PyGObject_Functions']]],
  ['register_5fgtype_5fcustom_6295',['register_gtype_custom',['../struct___py_g_object___functions.html#a532c4f72d01bc7e1c08797d10122159f',1,'_PyGObject_Functions']]],
  ['register_5finterface_6296',['register_interface',['../struct___py_g_object___functions.html#aaccc9e9bf93974ee732ef352461e33e4',1,'_PyGObject_Functions']]],
  ['register_5finterface_5finfo_6297',['register_interface_info',['../struct___py_g_object___functions.html#a1e3d0ce2120a032c435be91bf927b4b2',1,'_PyGObject_Functions']]],
  ['register_5fplugin_6298',['register_plugin',['../struct___remmina_plugin_service.html#a529cc64f67b16f13f0445add690a8df2',1,'_RemminaPluginService']]],
  ['register_5fpointer_6299',['register_pointer',['../struct___py_g_object___functions.html#a4d53c9a288e7771481e6d6b3d85eb0bf',1,'_PyGObject_Functions']]],
  ['register_5fwrapper_6300',['register_wrapper',['../struct___py_g_object___functions.html#ab8838fce87634bf6179115ab3780e38f',1,'_PyGObject_Functions']]],
  ['remmina_5fapplet_5fmenu_5fsignals_6301',['remmina_applet_menu_signals',['../remmina__applet__menu_8c.html#a20b651866ac24047839ee81c714bc596',1,'remmina_applet_menu.c']]],
  ['remmina_5favailable_5fplugin_5ftable_6302',['remmina_available_plugin_table',['../remmina__plugin__manager_8c.html#a132bfd7f61975c8edf732d4d0670a5ab',1,'remmina_plugin_manager.c']]],
  ['remmina_5fbug_5freport_5fdialog_6303',['remmina_bug_report_dialog',['../remmina__bug__report_8c.html#ae2b61ed64d7c375dcf5e5aa5cc2f7da2',1,'remmina_bug_report.c']]],
  ['remmina_5fchat_5fwindow_5fsignals_6304',['remmina_chat_window_signals',['../remmina__chat__window_8c.html#a49cae8631d5db230a97f9f5f3c66ff87',1,'remmina_chat_window.c']]],
  ['remmina_5fcolors_5ffile_6305',['remmina_colors_file',['../remmina__pref_8h.html#a94d3cf980275b6e9b0c701972f8b1cce',1,'remmina_pref.h']]],
  ['remmina_5fdrop_5ftypes_6306',['remmina_drop_types',['../remmina__main_8c.html#a8ccd8303ada6e26880504fc58812fbda',1,'remmina_main.c']]],
  ['remmina_5fec_5fpubkey_6307',['remmina_EC_PubKey',['../remmina__utils_8h.html#a68cd3e062c79543c1187d113a57046ef',1,'remmina_utils.h']]],
  ['remmina_5ffile_6308',['remmina_file',['../struct___remmina_protocol_widget_priv.html#a0f1ee4a3ddb7afe518588dce3046f5ec',1,'_RemminaProtocolWidgetPriv::remmina_file()'],['../struct___remmina_connection_object.html#a431b19fa993d0810e88cc973a307303c',1,'_RemminaConnectionObject::remmina_file()']]],
  ['remmina_5ffile_5fname_6309',['remmina_file_name',['../struct___remmina_pref.html#aa1544a98f03088dc260ed98f2deac94f',1,'_RemminaPref']]],
  ['remmina_5ffile_5fsecret_5fschema_6310',['remmina_file_secret_schema',['../glibsecret__plugin_8c.html#a4adccd38b5ef09344624c0ba2ac7046f',1,'glibsecret_plugin.c']]],
  ['remmina_5fftp_5fclient_5fsignals_6311',['remmina_ftp_client_signals',['../remmina__ftp__client_8c.html#a3cb68d802d3c35dff5cee970d3c33bd5',1,'remmina_ftp_client.c']]],
  ['remmina_5fftp_5fclient_5ftaskid_6312',['remmina_ftp_client_taskid',['../remmina__ftp__client_8c.html#a5a0c3d9452b7b75c3ae49ac7a9851e0a',1,'remmina_ftp_client.c']]],
  ['remmina_5ficon_6313',['remmina_icon',['../remmina__icon_8c.html#a912ed1addd1481ff94b775aad48731f0',1,'remmina_icon.c']]],
  ['remmina_5finfo_5fbutton_5fclose_6314',['remmina_info_button_close',['../struct___remmina_info_dialog.html#a1833054cee54ca460a03eb378942b4b5',1,'_RemminaInfoDialog']]],
  ['remmina_5finfo_5fdialog_6315',['remmina_info_dialog',['../remmina__info_8c.html#a15d4cbe903c84d0e39ce961fe6302bc7',1,'remmina_info.c']]],
  ['remmina_5finfo_5flabel_6316',['remmina_info_label',['../struct___remmina_info_dialog.html#a084f57e2e36c388175f1c908ad2f9ddb',1,'_RemminaInfoDialog']]],
  ['remmina_5finfo_5ftext_5fview_6317',['remmina_info_text_view',['../struct___remmina_info_dialog.html#a06b891212cb0b3a24f923734829d13b6',1,'_RemminaInfoDialog']]],
  ['remmina_5fkeymap_5ffile_6318',['remmina_keymap_file',['../remmina__pref_8c.html#a10b667eb7a100d6ac8d9544697f22743',1,'remmina_pref.c']]],
  ['remmina_5fkeymap_5ftable_6319',['remmina_keymap_table',['../remmina__pref_8c.html#ac194a2cb922d4a047b825dc69713cc46',1,'remmina_pref.c']]],
  ['remmina_5foptions_6320',['remmina_options',['../remmina_8c.html#a3837fb7dd2ca88b6f7acaecc0d062ac5',1,'remmina.c']]],
  ['remmina_5fpasswd_5fdialog_6321',['remmina_passwd_dialog',['../remmina__passwd_8c.html#a815d840bde1b72e8241722a3dd9792fc',1,'remmina_passwd.c']]],
  ['remmina_5fplugin_6322',['remmina_plugin',['../gvnc__plugin_8c.html#a3b53d8bbfcf5bdf5564c8804d211cf99',1,'remmina_plugin():&#160;gvnc_plugin.c'],['../plugin_8c.html#a3b53d8bbfcf5bdf5564c8804d211cf99',1,'remmina_plugin():&#160;plugin.c'],['../www__plugin_8c.html#a3b53d8bbfcf5bdf5564c8804d211cf99',1,'remmina_plugin():&#160;www_plugin.c'],['../exec__plugin_8c.html#a3b53d8bbfcf5bdf5564c8804d211cf99',1,'remmina_plugin():&#160;exec_plugin.c']]],
  ['remmina_5fplugin_5fexec_5fbasic_5fsettings_6323',['remmina_plugin_exec_basic_settings',['../exec__plugin_8c.html#a89c6ce747f68e963b8c6d21427a352f3',1,'exec_plugin.c']]],
  ['remmina_5fplugin_5fglibsecret_6324',['remmina_plugin_glibsecret',['../glibsecret__plugin_8c.html#a1b5617378d3f4d61a5150cff19d551dd',1,'glibsecret_plugin.c']]],
  ['remmina_5fplugin_5fkwallet_6325',['remmina_plugin_kwallet',['../kwallet__plugin__main_8c.html#af31524d2a9ee0747edc915947283b469',1,'kwallet_plugin_main.c']]],
  ['remmina_5fplugin_5fmanager_5fservice_6326',['remmina_plugin_manager_service',['../remmina__plugin__manager_8c.html#a3996819fdb2c833f3c64c2ad399e5f10',1,'remmina_plugin_manager_service():&#160;remmina_plugin_manager.c'],['../remmina__plugin__manager_8h.html#a3996819fdb2c833f3c64c2ad399e5f10',1,'remmina_plugin_manager_service():&#160;remmina_plugin_manager.c']]],
  ['remmina_5fplugin_5fservice_6327',['remmina_plugin_service',['../spice__plugin_8c.html#a9493664f6bdafe3f5b593c3e5e1eacc7',1,'remmina_plugin_service():&#160;spice_plugin.c'],['../remmina__sftp__plugin_8c.html#a9493664f6bdafe3f5b593c3e5e1eacc7',1,'remmina_plugin_service():&#160;remmina_sftp_plugin.c'],['../remmina__ssh__plugin_8c.html#a9493664f6bdafe3f5b593c3e5e1eacc7',1,'remmina_plugin_service():&#160;remmina_ssh_plugin.c'],['../exec__plugin_8c.html#a9493664f6bdafe3f5b593c3e5e1eacc7',1,'remmina_plugin_service():&#160;exec_plugin.c'],['../gvnc__plugin_8c.html#a9493664f6bdafe3f5b593c3e5e1eacc7',1,'remmina_plugin_service():&#160;gvnc_plugin.c'],['../kwallet__plugin__main_8c.html#a9493664f6bdafe3f5b593c3e5e1eacc7',1,'remmina_plugin_service():&#160;kwallet_plugin_main.c'],['../python__wrapper__common_8c.html#a9493664f6bdafe3f5b593c3e5e1eacc7',1,'remmina_plugin_service():&#160;python_wrapper_common.c'],['../rdp__plugin_8c.html#a9493664f6bdafe3f5b593c3e5e1eacc7',1,'remmina_plugin_service():&#160;rdp_plugin.c'],['../rdp__plugin_8h.html#a9493664f6bdafe3f5b593c3e5e1eacc7',1,'remmina_plugin_service():&#160;rdp_plugin.c'],['../glibsecret__plugin_8c.html#a9493664f6bdafe3f5b593c3e5e1eacc7',1,'remmina_plugin_service():&#160;glibsecret_plugin.c'],['../spice__plugin_8h.html#a9493664f6bdafe3f5b593c3e5e1eacc7',1,'remmina_plugin_service():&#160;rdp_plugin.c'],['../plugin_8c.html#a9493664f6bdafe3f5b593c3e5e1eacc7',1,'remmina_plugin_service():&#160;plugin.c'],['../vnc__plugin_8c.html#a9493664f6bdafe3f5b593c3e5e1eacc7',1,'remmina_plugin_service():&#160;vnc_plugin.c'],['../www__plugin_8c.html#a9493664f6bdafe3f5b593c3e5e1eacc7',1,'remmina_plugin_service():&#160;www_plugin.c'],['../www__plugin_8h.html#a9493664f6bdafe3f5b593c3e5e1eacc7',1,'remmina_plugin_service():&#160;rdp_plugin.c']]],
  ['remmina_5fplugin_5fsftp_6328',['remmina_plugin_sftp',['../remmina__sftp__plugin_8c.html#a464296b351cd89c358ec77f521c8f7ab',1,'remmina_sftp_plugin.c']]],
  ['remmina_5fplugin_5fsftp_5ffeatures_6329',['remmina_plugin_sftp_features',['../remmina__sftp__plugin_8c.html#a924786df34d61d90471f38dfd37aa855',1,'remmina_sftp_plugin.c']]],
  ['remmina_5fplugin_5fsignal_5fdata_6330',['remmina_plugin_signal_data',['../remmina__plugin__manager_8c.html#a07ced11866414a4af710fccfcc64a68b',1,'remmina_plugin_manager.c']]],
  ['remmina_5fplugin_5fspice_6331',['remmina_plugin_spice',['../spice__plugin_8c.html#a394f5bc2fd2f06247702ffbf27a94998',1,'spice_plugin.c']]],
  ['remmina_5fplugin_5fspice_5fadvanced_5fsettings_6332',['remmina_plugin_spice_advanced_settings',['../spice__plugin_8c.html#af3dc7072fd3204a1231025c1ea664409',1,'spice_plugin.c']]],
  ['remmina_5fplugin_5fspice_5fbasic_5fsettings_6333',['remmina_plugin_spice_basic_settings',['../spice__plugin_8c.html#aa6772e43af47ffc62ab556770e3cb56c',1,'spice_plugin.c']]],
  ['remmina_5fplugin_5fspice_5ffeatures_6334',['remmina_plugin_spice_features',['../spice__plugin_8c.html#a656cda0ea6cda6991e94119284d3ae81',1,'spice_plugin.c']]],
  ['remmina_5fplugin_5fssh_6335',['remmina_plugin_ssh',['../remmina__ssh__plugin_8c.html#a3cab806d9a2a069aa4446c30cbf41599',1,'remmina_ssh_plugin.c']]],
  ['remmina_5fplugin_5fssh_5ffeatures_6336',['remmina_plugin_ssh_features',['../remmina__ssh__plugin_8c.html#aaabfe48db4bcae99b7c2e30d37ff73a0',1,'remmina_ssh_plugin.c']]],
  ['remmina_5fplugin_5ftable_6337',['remmina_plugin_table',['../remmina__plugin__manager_8c.html#a65313d34bb7e940f388e45818fd5889e',1,'remmina_plugin_manager.c']]],
  ['remmina_5fplugin_5ftelepathy_6338',['remmina_plugin_telepathy',['../telepathy__plugin_8c.html#a509c855e1783653e907f8dfe290a5b58',1,'telepathy_plugin.c']]],
  ['remmina_5fplugin_5ftelepathy_5fservice_6339',['remmina_plugin_telepathy_service',['../telepathy__channel__handler_8c.html#ad6a9e111bcd9487a6215c417ea70e052',1,'remmina_plugin_telepathy_service():&#160;telepathy_plugin.c'],['../telepathy__handler_8c.html#ad6a9e111bcd9487a6215c417ea70e052',1,'remmina_plugin_telepathy_service():&#160;telepathy_plugin.c'],['../telepathy__plugin_8c.html#ad6a9e111bcd9487a6215c417ea70e052',1,'remmina_plugin_telepathy_service():&#160;telepathy_plugin.c']]],
  ['remmina_5fplugin_5fthread_6340',['remmina_plugin_thread',['../structrf__context.html#aa79909e0ac140a3404151a40bed6a6ec',1,'rf_context']]],
  ['remmina_5fplugin_5ftool_5fbasic_5fsettings_6341',['remmina_plugin_tool_basic_settings',['../plugin_8c.html#a833f9235bae6f4784189b5b825189df6',1,'plugin.c']]],
  ['remmina_5fplugin_5ftype_5fname_6342',['remmina_plugin_type_name',['../remmina__plugin__manager_8c.html#a719ce18a83ed7ecb3388f179a8590c10',1,'remmina_plugin_manager.c']]],
  ['remmina_5fplugin_5fvnc_6343',['remmina_plugin_vnc',['../vnc__plugin_8c.html#a954db5d93dbd612c1f64c3a3b05b97ea',1,'vnc_plugin.c']]],
  ['remmina_5fplugin_5fvnc_5fadvanced_5fsettings_6344',['remmina_plugin_vnc_advanced_settings',['../vnc__plugin_8c.html#a8525912fbb407209875f84448abec8c7',1,'vnc_plugin.c']]],
  ['remmina_5fplugin_5fvnc_5fbasic_5fsettings_6345',['remmina_plugin_vnc_basic_settings',['../vnc__plugin_8c.html#a8b2de0c8917cafa414c0b9ed4c29aee3',1,'vnc_plugin.c']]],
  ['remmina_5fplugin_5fvnc_5ffeatures_6346',['remmina_plugin_vnc_features',['../vnc__plugin_8c.html#ab9cd44cc6ac7467e6c31c3d5fe77ac21',1,'vnc_plugin.c']]],
  ['remmina_5fplugin_5fvnc_5fno_5fencrypt_5fauth_5ftypes_6347',['remmina_plugin_vnc_no_encrypt_auth_types',['../vnc__plugin_8c.html#aa6380a77d4119e8ce70c2d46fd60aece',1,'vnc_plugin.c']]],
  ['remmina_5fplugin_5fvnci_6348',['remmina_plugin_vnci',['../vnc__plugin_8c.html#a289d56662190215fffd46338a57133d9',1,'vnc_plugin.c']]],
  ['remmina_5fplugin_5fvnci_5fbasic_5fsettings_6349',['remmina_plugin_vnci_basic_settings',['../vnc__plugin_8c.html#ac25782cb49710754854863dc9500bd57',1,'vnc_plugin.c']]],
  ['remmina_5fplugin_5fwindow_6350',['remmina_plugin_window',['../remmina__plugin__manager_8c.html#a33a9c222c20a8e7e9d5c6ea2d61f907e',1,'remmina_plugin_manager.c']]],
  ['remmina_5fplugin_5fwww_5fadvanced_5fsettings_6351',['remmina_plugin_www_advanced_settings',['../www__plugin_8c.html#ac6b72af8913086b2ec590a675473f2a1',1,'www_plugin.c']]],
  ['remmina_5fplugin_5fwww_5fbasic_5fsettings_6352',['remmina_plugin_www_basic_settings',['../www__plugin_8c.html#a3874a40164919bc5389d166df691bfdb',1,'www_plugin.c']]],
  ['remmina_5fpref_6353',['remmina_pref',['../rcw_8c.html#a29701ae152ba15f6d8921f395174d2df',1,'remmina_pref():&#160;rcw.c'],['../remmina__pref_8h.html#a29701ae152ba15f6d8921f395174d2df',1,'remmina_pref():&#160;rcw.c']]],
  ['remmina_5fpref_5fdialog_6354',['remmina_pref_dialog',['../remmina__pref__dialog_8c.html#a2e3c32df32107ad3f4b9ea85fc001368',1,'remmina_pref_dialog.c']]],
  ['remmina_5fpref_5ffile_6355',['remmina_pref_file',['../rcw_8c.html#af657fd4825d16d8d003a8b42fbd0c715',1,'remmina_pref_file():&#160;rcw.c'],['../remmina__pref_8h.html#af657fd4825d16d8d003a8b42fbd0c715',1,'remmina_pref_file():&#160;rcw.c']]],
  ['remmina_5fprotocol_5fwidget_5fget_5fcurrent_5fscale_5fmode_6356',['remmina_protocol_widget_get_current_scale_mode',['../struct___remmina_plugin_service.html#ae97f4052c4bbda628682ae071ada27ed',1,'_RemminaPluginService']]],
  ['remmina_5fprotocol_5fwidget_5fsignals_6357',['remmina_protocol_widget_signals',['../remmina__protocol__widget_8c.html#a2c2a41aeee8f3ef6481378a95c02c130',1,'remmina_protocol_widget.c']]],
  ['remmina_5fpython_5fmodule_5ftype_6358',['remmina_python_module_type',['../python__wrapper__remmina_8c.html#a822ee64f62b7bf7af738dfec34e69737',1,'python_wrapper_remmina.c']]],
  ['remmina_5fpython_5fmodule_5ftype_5fmethods_6359',['remmina_python_module_type_methods',['../python__wrapper__remmina_8c.html#adf58bae87972fe68fa239199e7ca6e1b',1,'python_wrapper_remmina.c']]],
  ['remmina_5fpython_5fwrapper_6360',['remmina_python_wrapper',['../python__wrapper__plugin_8c.html#ae2abb74183563726e9085156c50e60e4',1,'python_wrapper_plugin.c']]],
  ['remmina_5frsa_5fpubkey_5fv1_6361',['remmina_RSA_PubKey_v1',['../remmina__utils_8c.html#ac6043621572739a633ed39be0ffcb387',1,'remmina_RSA_PubKey_v1():&#160;remmina_utils.c'],['../remmina__utils_8h.html#ac6043621572739a633ed39be0ffcb387',1,'remmina_RSA_PubKey_v1():&#160;remmina_utils.c']]],
  ['remmina_5frsa_5fpubkey_5fv2_6362',['remmina_RSA_PubKey_v2',['../remmina__utils_8c.html#ab0331fe621ebd14b531898d17814547f',1,'remmina_RSA_PubKey_v2():&#160;remmina_utils.c'],['../remmina__utils_8h.html#ab0331fe621ebd14b531898d17814547f',1,'remmina_RSA_PubKey_v2():&#160;remmina_utils.c']]],
  ['remmina_5fsecret_5fplugin_6363',['remmina_secret_plugin',['../remmina__plugin__manager_8c.html#a5e79d6abd0b12c1ed2bb33988e41857e',1,'remmina_plugin_manager.c']]],
  ['remmina_5fsftp_5fbasic_5fsettings_6364',['remmina_sftp_basic_settings',['../remmina__sftp__plugin_8c.html#a999de1dbe291169ccc33dffa123a1315',1,'remmina_sftp_plugin.c']]],
  ['remmina_5fssh_5fadvanced_5fsettings_6365',['remmina_ssh_advanced_settings',['../remmina__ssh__plugin_8c.html#a51b5159cc5a6833759bc87688221b807',1,'remmina_ssh_plugin.c']]],
  ['remmina_5fssh_5fbasic_5fsettings_6366',['remmina_ssh_basic_settings',['../remmina__ssh__plugin_8c.html#a30ddbf13515b3bdad4e220d75b7912bf',1,'remmina_ssh_plugin.c']]],
  ['remmina_5ftp_5fhandler_6367',['remmina_tp_handler',['../telepathy__plugin_8c.html#a1e255408ada3d8096a2bc959a55ae5c0',1,'telepathy_plugin.c']]],
  ['remmina_5funlock_5fdialog_6368',['remmina_unlock_dialog',['../remmina__unlock_8c.html#a18d0da78f53c4002601a59e85373f6d7',1,'remmina_unlock.c']]],
  ['remmina_5fwidget_5fpool_6369',['remmina_widget_pool',['../remmina__widget__pool_8c.html#a904ecc5c259a92edee29d749b3406ac2',1,'remmina_widget_pool.c']]],
  ['remmina_5fwww_5ffeatures_6370',['remmina_www_features',['../www__plugin_8c.html#ae8ced85169ca68aae1ed7f14b488e5b8',1,'www_plugin.c']]],
  ['remmina_5fx2go_5finit_5fmutex_6371',['remmina_x2go_init_mutex',['../x2go__plugin_8c.html#a5f054e33e1f9086ade1868e09396c2c2',1,'x2go_plugin.c']]],
  ['remmina_5fx2go_5fwindow_5fid_5farray_6372',['remmina_x2go_window_id_array',['../x2go__plugin_8c.html#acb28505c6a55be236da68c16efda30f9',1,'x2go_plugin.c']]],
  ['remminadir_6373',['remminadir',['../remmina__file__manager_8c.html#af83917dac9f2646670733215ba7a3482',1,'remmina_file_manager.c']]],
  ['remminafile_6374',['remminafile',['../structremmina__masterthread__exec__data.html#ad02dd3e2c0839ed5dda61b49a4377eb9',1,'remmina_masterthread_exec_data']]],
  ['remminamain_6375',['remminamain',['../remmina__main_8c.html#ab7c680e1f6ffeef21cb928a083d36d89',1,'remmina_main.c']]],
  ['remminapluginfunc_6376',['RemminaPluginFunc',['../remmina__plugin__manager_8h.html#a68d4ed61b9d039dd06ba4252a8971c18',1,'remmina_plugin_manager.h']]],
  ['remminapluginmain_6377',['RemminaPluginMain',['../remmina__plugin__native_8h.html#adbfe7a14da3e23caf70b2419ceac0eb1',1,'remmina_plugin_native.h']]],
  ['remminastringarray_6378',['RemminaStringArray',['../remmina__string__array_8h.html#a62228cb0bc959307e3268cff98e1f7bb',1,'remmina_string_array.h']]],
  ['remminawidgetpoolforeachfunc_6379',['RemminaWidgetPoolForEachFunc',['../remmina__widget__pool_8h.html#a6ced6afec83f47d8ac36b01bff2eb61e',1,'remmina_widget_pool.h']]],
  ['remotedir_6380',['remotedir',['../struct___remmina_f_t_p_task.html#aff3ca859a6687076313869aeb569dd87',1,'_RemminaFTPTask']]],
  ['remotedisplay_6381',['remotedisplay',['../struct___remmina_s_s_h_tunnel.html#af670e0b0960c3e71e9f0b8b797ef9c28',1,'_RemminaSSHTunnel']]],
  ['renderer_6382',['renderer',['../struct___remmina_cell_renderer_pixbuf.html#adc32a5ea949ab492a1d57e64a1adf045',1,'_RemminaCellRendererPixbuf']]],
  ['repeater_5ftooltip_6383',['repeater_tooltip',['../vnc__plugin_8c.html#a616ad35664d4a399f88a8a84ee89140d',1,'vnc_plugin.c']]],
  ['request_6384',['request',['../struct___remmina_plugin_w_w_w_data.html#ac7390cf88e14b015713ea1f0980909f0',1,'_RemminaPluginWWWData']]],
  ['requestedformatid_6385',['requestedFormatId',['../structrf__clipboard.html#a66caa2e83dfdc4655df71d81502f93e3',1,'rf_clipboard']]],
  ['resolutions_6386',['resolutions',['../struct___remmina_pref.html#a59eebf1d365ef192de8e14da2c381f37',1,'_RemminaPref']]],
  ['resolutions_5flist_6387',['resolutions_list',['../struct___remmina_pref_dialog_priv.html#ae835f35a6211b2858701e4a3be835f05',1,'_RemminaPrefDialogPriv']]],
  ['response_6388',['response',['../struct_mp_run_info.html#acb8e8aaf721611a761541a325e2a5904',1,'MpRunInfo::response()'],['../struct___remmina_key_chooser_arguments.html#a2fd9e4d001913994dd7ffee24563bde7',1,'_RemminaKeyChooserArguments::response()'],['../structcurl__msg.html#ad71ca721937a53cf9506e7e2a714ee77',1,'curl_msg::response()']]],
  ['response_5fcallback_6389',['response_callback',['../struct_remmina_message_panel_private.html#a9766a5c72bd815501e2ba0a5ac8cd283',1,'RemminaMessagePanelPrivate::response_callback()'],['../structremmina__masterthread__exec__data.html#ad918fe1f14834f71a7a55bdee0c9a543',1,'remmina_masterthread_exec_data::response_callback()']]],
  ['response_5fcallback_5fdata_6390',['response_callback_data',['../struct_remmina_message_panel_private.html#a6595dbe739d97d82e36c6e0878bcf1d4',1,'RemminaMessagePanelPrivate::response_callback_data()'],['../structremmina__masterthread__exec__data.html#aed4afd0ce7ecf117534076f500aa0658',1,'remmina_masterthread_exec_data::response_callback_data()']]],
  ['ret_5fmp_6391',['ret_mp',['../structremmina__masterthread__exec__data.html#ab4a1d527225cf5a768814b02acb0e7a1',1,'remmina_masterthread_exec_data']]],
  ['retptr_6392',['retptr',['../structremmina__plugin__rdp__ui__object.html#a6cdd472797d227d7ce79a312c0fa2a24',1,'remmina_plugin_rdp_ui_object']]],
  ['retry_5fmessage_5fpanel_6393',['retry_message_panel',['../struct___remmina_protocol_widget_priv.html#acad26c8fddd63360e26ecc99b0d57be4',1,'_RemminaProtocolWidgetPriv']]],
  ['retval_6394',['retval',['../structremmina__plugin__rdp__ui__object.html#a9f6aba25030b6859fbd57bee840feeee',1,'remmina_plugin_rdp_ui_object::retval()'],['../structremmina__masterthread__exec__data.html#a9d393b6e73a6971ac91cad231f796e15',1,'remmina_masterthread_exec_data::retval()'],['../structremmina__masterthread__exec__data.html#ae203540a298d2f92459e740789e8799c',1,'remmina_masterthread_exec_data::retval()'],['../structremmina__masterthread__exec__data.html#aea0366c99f68b738f396678ea6b77e07',1,'remmina_masterthread_exec_data::retval()'],['../struct___remmina_unlock_dialog.html#a100f356b1eb9f6574b349e7573408235',1,'_RemminaUnlockDialog::retval()'],['../struct___remmina_info_dialog.html#a8f72aa00bf10de2f425f3757d2f8fcc2',1,'_RemminaInfoDialog::retval()']]],
  ['reveal_5fbutton_6395',['reveal_button',['../struct___remmina_ssh_search.html#a8f9a64245acf9b02edac4fbbcfdedd02',1,'_RemminaSshSearch']]],
  ['revealer_6396',['revealer',['../struct___remmina_ssh_search.html#a9b72b310751ac148d5c55b0f2a04a300',1,'_RemminaSshSearch']]],
  ['rfi_6397',['rfi',['../structrf__clipboard.html#a268b5336ae92d8d2f54c5ee441325d68',1,'rf_clipboard']]],
  ['rfx_6398',['rfx',['../structremmina__plugin__rdp__ui__object.html#ae707c3ee78fed655028ec16f3dbfa058',1,'remmina_plugin_rdp_ui_object']]],
  ['rgb_5fbuffer_6399',['rgb_buffer',['../struct___remmina_plugin_vnc_data.html#a5412ada4c656d0053f0656bed890c030',1,'_RemminaPluginVncData']]],
  ['rm_5fmonitor_6400',['rm_monitor',['../remmina__monitor_8c.html#a6eae21e170328d883ef13f8f71401ffd',1,'remmina_monitor.c']]],
  ['rm_5fplugin_5fservice_6401',['rm_plugin_service',['../x2go__plugin_8c.html#ab54b79eb5b8f9560d3e785cf16c45a9b',1,'x2go_plugin.c']]],
  ['rmplugin_5fx2go_6402',['rmplugin_x2go',['../x2go__plugin_8c.html#a59baf7f963dc3cdbd87224706cd73c68',1,'x2go_plugin.c']]],
  ['rmplugin_5fx2go_5fbasic_5fsettings_6403',['rmplugin_x2go_basic_settings',['../x2go__plugin_8c.html#a71dcd8bd6ea5810a7299fa01b675baf9',1,'x2go_plugin.c']]],
  ['rmplugin_5fx2go_5ffeatures_6404',['rmplugin_x2go_features',['../x2go__plugin_8c.html#a68c599f47bcfdc7702e0e11410f2939f',1,'x2go_plugin.c']]],
  ['rowref_6405',['rowref',['../struct___remmina_f_t_p_task.html#a0e2d8b5a795eff39aaa5678ee371cea8',1,'_RemminaFTPTask']]],
  ['run_5fline_6406',['run_line',['../struct___remmina_s_s_h_shell.html#af5c36c45f7e0beedee2e365da48a1331',1,'_RemminaSSHShell']]],
  ['running_6407',['running',['../struct___remmina_s_s_h_tunnel.html#a956cb7289e147875ae5f745b33514ebd',1,'_RemminaSSHTunnel::running()'],['../struct___remmina_plugin_vnc_data.html#ac4e1f4b6fc95c31896cc50f315312f93',1,'_RemminaPluginVncData::running()']]]
];
