var indexSectionsWithContent =
{
  0: "<_abcdefghijklmnopqrstuvwxy",
  1: "_cdilmoprs",
  2: "_abcdefghiklmnprstuvwx",
  3: "_abcdefghijklmnoprstuvwx",
  4: "_abcdefghiklmnopqrstuvwxy",
  5: "_dglnprsx",
  6: "_cgprsw",
  7: "cdefglnoprstuvwx",
  8: "<abcdefghimnpqrstuvw"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Pages"
};

