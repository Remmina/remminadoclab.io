var searchData=
[
  ['x_3566',['x',['../structremmina__plugin__rdp__event.html#a2f6b6cb00f2511b7849654b026cfd105',1,'remmina_plugin_rdp_event::x()'],['../structregion.html#ad258e7b02e819b46656294de5e4bd8ad',1,'region::x()'],['../structremmina__plugin__rdp__ui__object.html#ab0223592e68f270b22632c83c300cac9',1,'remmina_plugin_rdp_ui_object::x()'],['../structon_main_thread__cb__data.html#a92fd0bfc7715dcb8237a002850701222',1,'onMainThread_cb_data::x()'],['../struct___remmina_plugin_vnc_event.html#a66390bbb63e42d25f8fa8e0be7f7dee8',1,'_RemminaPluginVncEvent::x()'],['../struct___remmina_plugin_vnc_coordinates.html#a33c99179134cf900b66103767766ed3a',1,'_RemminaPluginVncCoordinates::x()']]],
  ['x2go_5fplugin_2ec_3567',['x2go_plugin.c',['../x2go__plugin_8c.html',1,'']]],
  ['x2go_5fplugin_2eh_3568',['x2go_plugin.h',['../x2go__plugin_8h.html',1,'']]],
  ['x2gocustomuserdata_3569',['X2GoCustomUserData',['../x2go__plugin_8c.html#abab5a8b3a4c90bace42bfe9d0a5ab019',1,'x2go_plugin.c']]],
  ['xport_5ftunnel_5finit_3570',['xport_tunnel_init',['../python__wrapper__protocol__widget_8c.html#a4cf41bc908920ef34d850cdf4c659ae3',1,'python_wrapper_protocol_widget.c']]],
  ['xterm_3571',['XTERM',['../remmina__ssh__plugin_8c.html#a236a29beb2ea51daaeb01fe86d41807ca0a9ecf4938fe6dbd1cb74a7b23865a56',1,'remmina_ssh_plugin.c']]],
  ['xterm_5fpalette_3572',['xterm_palette',['../remmina__ssh__plugin_8c.html#a9cc4363a5dd981de398aca34155b38c0',1,'remmina_ssh_plugin.c']]]
];
