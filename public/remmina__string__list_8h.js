var remmina__string__list_8h =
[
    [ "_RemminaStringListPriv", "struct___remmina_string_list_priv.html", "struct___remmina_string_list_priv" ],
    [ "_RemminaStringList", "struct___remmina_string_list.html", "struct___remmina_string_list" ],
    [ "RemminaStringList", "remmina__string__list_8h.html#a4aee29347c52d1ac15c790f0ed679a93", null ],
    [ "RemminaStringListPriv", "remmina__string__list_8h.html#aa7d04d7e20ff9420a28d6fe8b6d43569", null ],
    [ "RemminaStringListValidationFunc", "remmina__string__list_8h.html#a3112419c42db7e04347a3ba63778c7bb", null ],
    [ "remmina_string_list_get_text", "remmina__string__list_8h.html#a74058eac0af28c05a55d041103457aec", null ],
    [ "remmina_string_list_new", "remmina__string__list_8h.html#a26bc63cdb43ed3fda2d86ac1d95a5029", null ],
    [ "remmina_string_list_set_text", "remmina__string__list_8h.html#ab1bacc097f59c20b477d954bb863a5e0", null ],
    [ "remmina_string_list_set_titles", "remmina__string__list_8h.html#aa9ca6ccf40b954051a0daa5c1efeef87", null ],
    [ "remmina_string_list_set_validation_func", "remmina__string__list_8h.html#a500b85eafd23280f998d2980d1eba80e", null ]
];