var struct___connection_data =
[
    [ "host", "struct___connection_data.html#a5366b4138e2ff42c85f81a83a23cb2c1", null ],
    [ "password", "struct___connection_data.html#a0b7548b539c507772e709e8867f6b1a8", null ],
    [ "ssh_passphrase", "struct___connection_data.html#a9bf0eafee9efa826d4dd89ba1f358236", null ],
    [ "ssh_privatekey", "struct___connection_data.html#a6c5715c8a4da2ccdf272f6e23de4a629", null ],
    [ "username", "struct___connection_data.html#af15752c3cf9e16fff53be579bf747541", null ]
];