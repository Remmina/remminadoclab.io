var vnc__plugin_8h =
[
    [ "_RemminaPluginVncData", "struct___remmina_plugin_vnc_data.html", "struct___remmina_plugin_vnc_data" ],
    [ "_RemminaPluginVncEvent", "struct___remmina_plugin_vnc_event.html", "struct___remmina_plugin_vnc_event" ],
    [ "_RemminaPluginVncCoordinates", "struct___remmina_plugin_vnc_coordinates.html", "struct___remmina_plugin_vnc_coordinates" ],
    [ "RemminaPluginVncCoordinates", "vnc__plugin_8h.html#ad54e42da6a8a5ca07c66dfb1c83577ee", null ],
    [ "RemminaPluginVncData", "vnc__plugin_8h.html#a02047520bdf8b76bbae0c0d24d44f6f6", null ],
    [ "RemminaPluginVncEvent", "vnc__plugin_8h.html#a99c7b7ecfa58051baccd4179a854374e", null ],
    [ "remmina_plugin_vnc_update_scale", "vnc__plugin_8h.html#a8a70c9cdaf6821611a3311c0d4797db9", null ]
];