var remmina__chat__window_8h =
[
    [ "_RemminaChatWindow", "struct___remmina_chat_window.html", "struct___remmina_chat_window" ],
    [ "_RemminaChatWindowClass", "struct___remmina_chat_window_class.html", "struct___remmina_chat_window_class" ],
    [ "RemminaChatWindow", "remmina__chat__window_8h.html#ab81bcca5490e9e7cc9a74417b47b8b3c", null ],
    [ "RemminaChatWindowClass", "remmina__chat__window_8h.html#a4b46068fd9ef1db0f64d71ee5655dfcb", null ],
    [ "remmina_chat_window_get_type", "remmina__chat__window_8h.html#a5388d221b74aacf623628fe5a2f83782", null ],
    [ "remmina_chat_window_new", "remmina__chat__window_8h.html#a5e4fa655fddf361fed13bbd759bea24f", null ],
    [ "remmina_chat_window_receive", "remmina__chat__window_8h.html#a45724aed5474dd741563ffbf89237022", null ]
];