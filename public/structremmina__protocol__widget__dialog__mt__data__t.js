var structremmina__protocol__widget__dialog__mt__data__t =
[
    [ "called_from_subthread", "structremmina__protocol__widget__dialog__mt__data__t.html#a81eb96409c1543bcf2d9474d4d89907f", null ],
    [ "default_domain", "structremmina__protocol__widget__dialog__mt__data__t.html#a14655c577c6f070ee28f4211baf05361", null ],
    [ "default_password", "structremmina__protocol__widget__dialog__mt__data__t.html#a650c926c67ed4ec841cf8553d6454850", null ],
    [ "default_username", "structremmina__protocol__widget__dialog__mt__data__t.html#ad9460e6c8e837ce941568d74499e0b28", null ],
    [ "dtype", "structremmina__protocol__widget__dialog__mt__data__t.html#a13fd74ca385ce7f7a4d6094a65bd17f7", null ],
    [ "gp", "structremmina__protocol__widget__dialog__mt__data__t.html#aac83645c5474683d37a9a61fcd640b7a", null ],
    [ "pflags", "structremmina__protocol__widget__dialog__mt__data__t.html#a92d8a08aba2743f081e22f39fcaf0df8", null ],
    [ "pt_cond", "structremmina__protocol__widget__dialog__mt__data__t.html#a9ee011a8e1d09de188018e634aa2bbda", null ],
    [ "pt_mutex", "structremmina__protocol__widget__dialog__mt__data__t.html#a3ecf120415414844ebcf07a1c86e9cdf", null ],
    [ "rcbutton", "structremmina__protocol__widget__dialog__mt__data__t.html#a29b8f44a7f4514422d3a5e04e0b8f0b5", null ],
    [ "strpasswordlabel", "structremmina__protocol__widget__dialog__mt__data__t.html#a72561ca7477a62e8c7234409205e6544", null ],
    [ "title", "structremmina__protocol__widget__dialog__mt__data__t.html#a3f67e6ca406ec87db6ae6a3582770c74", null ]
];