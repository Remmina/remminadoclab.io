var struct___remmina_protocol_feature =
[
    [ "id", "struct___remmina_protocol_feature.html#a36b55faeba78528ac9708dfa66667e20", null ],
    [ "opt1", "struct___remmina_protocol_feature.html#aa7ac5ec9d95867734619583a2049e952", null ],
    [ "opt1_type_hint", "struct___remmina_protocol_feature.html#a4b8ffd6e32126a9c4d2074521279b12b", null ],
    [ "opt2", "struct___remmina_protocol_feature.html#aab44850c13a7e5e20a02603ae173d53d", null ],
    [ "opt2_type_hint", "struct___remmina_protocol_feature.html#a006b0d6d883c6dd0da6c444991973ac7", null ],
    [ "opt3", "struct___remmina_protocol_feature.html#ab3c15094a16418c5951c5382c8a44190", null ],
    [ "opt3_type_hint", "struct___remmina_protocol_feature.html#a9abdee80580cddd54405d0577e459c2c", null ],
    [ "type", "struct___remmina_protocol_feature.html#a1826baafd2d7b5dc8a009ef4fcad77e9", null ]
];