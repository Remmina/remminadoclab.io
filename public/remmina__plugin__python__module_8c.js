var remmina__plugin__python__module_8c =
[
    [ "GetEnumOrDefault", "remmina__plugin__python__module_8c.html#a597e686436c18ba3eff1b6f5c0f4011e", null ],
    [ "remmina_plugin_python_create_entry_plugin", "remmina__plugin__python__module_8c.html#a5187294d5f089c147551aa2a24e0e326", null ],
    [ "remmina_plugin_python_create_file_plugin", "remmina__plugin__python__module_8c.html#ab1c5961945180dfcf251e1a14903fe62", null ],
    [ "remmina_plugin_python_create_pref_plugin", "remmina__plugin__python__module_8c.html#a177856f76dc71bf7f4c2599271e4eea7", null ],
    [ "remmina_plugin_python_create_protocol_plugin", "remmina__plugin__python__module_8c.html#a8bf2a1ea1f25c1b28d0b32bd2d3f3d43", null ],
    [ "remmina_plugin_python_create_secret_plugin", "remmina__plugin__python__module_8c.html#a1a75ac7dfa97841ded14e70bc9e687b9", null ],
    [ "remmina_plugin_python_create_tool_plugin", "remmina__plugin__python__module_8c.html#a7f880a1cd707ae12177e50074da4c7ce", null ],
    [ "remmina_protocol_call_feature_wrapper", "remmina__plugin__python__module_8c.html#aaecec6bbf5bac24c099f56bd65f59d2d", null ],
    [ "remmina_protocol_close_connection_wrapper", "remmina__plugin__python__module_8c.html#a8337d133c8ac8b68ad602213d8c2eb43", null ],
    [ "remmina_protocol_get_plugin_screenshot_wrapper", "remmina__plugin__python__module_8c.html#a70bc0f230e18c4292e7f4afaf70cdfae", null ],
    [ "remmina_protocol_init_wrapper", "remmina__plugin__python__module_8c.html#ae6f588f0ed182aca0f3075de5cda93b8", null ],
    [ "remmina_protocol_open_connection_wrapper", "remmina__plugin__python__module_8c.html#a21ce742fbe1c51a7364a0746b5bfaf25", null ],
    [ "remmina_protocol_query_feature_wrapper", "remmina__plugin__python__module_8c.html#a7727aaf072cc0489849d7d57cb08edd3", null ],
    [ "remmina_protocol_send_keytrokes_wrapper", "remmina__plugin__python__module_8c.html#a47e3667417032983fd63d680476afc9d", null ]
];