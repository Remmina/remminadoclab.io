var struct___remmina_plugin_x2_go_data =
[
    [ "available_features", "struct___remmina_plugin_x2_go_data.html#a642dc28885abcd14f90fd8710b220e2e", null ],
    [ "disconnected", "struct___remmina_plugin_x2_go_data.html#a3fe6e076459db4ee337d70ef2c1d838a", null ],
    [ "display", "struct___remmina_plugin_x2_go_data.html#abca43c87558a7cd7cba5f380e321cba5", null ],
    [ "orig_handler", "struct___remmina_plugin_x2_go_data.html#a81c09c0a216e8f6fab0eb03dc5140361", null ],
    [ "pidx2go", "struct___remmina_plugin_x2_go_data.html#a096eb53491e1297c8119badfb75df393", null ],
    [ "socket", "struct___remmina_plugin_x2_go_data.html#a10588addf4b97f37b01fa84f06ed9cb2", null ],
    [ "socket_id", "struct___remmina_plugin_x2_go_data.html#ab0d88dfa878a99bfd6bc2b79f20bdcfe", null ],
    [ "thread", "struct___remmina_plugin_x2_go_data.html#aca25367be14cc65a5313151814f44916", null ],
    [ "window_id", "struct___remmina_plugin_x2_go_data.html#a556145e479d666ec055ed1f907bd6022", null ]
];