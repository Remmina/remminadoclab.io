var struct___remmina_plugin_vnc_event =
[
    [ "button_mask", "struct___remmina_plugin_vnc_event.html#a03809ddab0aea8b6d2796641d4ccda32", null ],
    [ "event_data", "struct___remmina_plugin_vnc_event.html#a65073955046001c6f9da47b74e511f95", null ],
    [ "event_type", "struct___remmina_plugin_vnc_event.html#a0ffbaa26908fd25e653dfaf5ad8ccff0", null ],
    [ "key", "struct___remmina_plugin_vnc_event.html#a8692b45f4698212435199558d318c7d3", null ],
    [ "keyval", "struct___remmina_plugin_vnc_event.html#ae2a04bad3d386a453554a431f9e9ad87", null ],
    [ "pointer", "struct___remmina_plugin_vnc_event.html#a36f50946fb858d27308fc46c2653503e", null ],
    [ "pressed", "struct___remmina_plugin_vnc_event.html#a264ea1e41624de1e4a0e9cad031f0ac8", null ],
    [ "text", "struct___remmina_plugin_vnc_event.html#a79891d24c0e6257efceeefb3c10d8c65", null ],
    [ "text", "struct___remmina_plugin_vnc_event.html#a83ee3a216af13753747e628db3278993", null ],
    [ "x", "struct___remmina_plugin_vnc_event.html#a66390bbb63e42d25f8fa8e0be7f7dee8", null ],
    [ "y", "struct___remmina_plugin_vnc_event.html#a35d3190915553df1cfc65fb951403c11", null ]
];