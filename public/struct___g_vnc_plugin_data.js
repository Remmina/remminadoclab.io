var struct___g_vnc_plugin_data =
[
    [ "addr", "struct___g_vnc_plugin_data.html#abad2697134d2d754a0df70d0cbb398b0", null ],
    [ "box", "struct___g_vnc_plugin_data.html#a69d1945197597dcc203ad06cd230ef2f", null ],
    [ "clipstr", "struct___g_vnc_plugin_data.html#ae61d3e9e60fe171c7021e3db47a8500f", null ],
    [ "conn", "struct___g_vnc_plugin_data.html#a45a2c89f810b30ea993ba3245a3312e9", null ],
    [ "depth_profile", "struct___g_vnc_plugin_data.html#affe57aeb733152c22661f2242d2c5db8", null ],
    [ "error_msg", "struct___g_vnc_plugin_data.html#a551979283fe3d2a5a292da2d41358572", null ],
    [ "fd", "struct___g_vnc_plugin_data.html#a7f66a24811aa00909de794c47e89d071", null ],
    [ "height", "struct___g_vnc_plugin_data.html#adcfa22529d69eec8414ed3e64fd0bdd6", null ],
    [ "lossy_encoding", "struct___g_vnc_plugin_data.html#ad476177bc9b886254223688b79e07898", null ],
    [ "pa", "struct___g_vnc_plugin_data.html#a9801c8e44873771eef6e649638989207", null ],
    [ "shared", "struct___g_vnc_plugin_data.html#a87deacc499a3b37393c555f5cac9995e", null ],
    [ "signal_clipboard", "struct___g_vnc_plugin_data.html#a2ea581f52e0942bf1aad06004a11f70c", null ],
    [ "viewonly", "struct___g_vnc_plugin_data.html#a661dd99cf5ba2e60b9b53b551d2b355d", null ],
    [ "vnc", "struct___g_vnc_plugin_data.html#ae5eb52954916f69fba44f0783cfdd506", null ],
    [ "width", "struct___g_vnc_plugin_data.html#a46c68b45ac89a62e02b94aa7540b48ec", null ]
];