var remmina__info_8h =
[
    [ "_RemminaInfoDialog", "struct___remmina_info_dialog.html", "struct___remmina_info_dialog" ],
    [ "_RemminaInfoMessage", "struct___remmina_info_message.html", "struct___remmina_info_message" ],
    [ "RemminaInfoDialog", "remmina__info_8h.html#a4af09aa4fa1ffd9cea002efd0ca41bd6", null ],
    [ "RemminaInfoMessage", "remmina__info_8h.html#abeca43aa6a19a1040aa6afd7f06c5fc4", null ],
    [ "remmina_info_periodic_check", "remmina__info_8h.html#a442b4168665a7f03323b98177105f3a3", null ],
    [ "remmina_info_schedule", "remmina__info_8h.html#a943c2b4631211107632e302816b4bd01", null ],
    [ "remmina_info_show_response", "remmina__info_8h.html#aecabe29b7a2e6372e26f748e17e02ae6", null ],
    [ "remmina_info_stats_collector", "remmina__info_8h.html#a274d46fd3159438a385a9ed02d83a392", null ],
    [ "remmina_info_stats_get_all", "remmina__info_8h.html#af9a78640d19d40bf46e9cd05ba01575d", null ],
    [ "remmina_info_stats_get_os_info", "remmina__info_8h.html#ad75a6967192e98357ad37b9f90b8c169", null ],
    [ "remmina_info_stats_get_python", "remmina__info_8h.html#ace635be32c8df451691288e7f9ddf3ab", null ],
    [ "remmina_info_stats_get_uid", "remmina__info_8h.html#aaed923507db671022f1b9f90efdbb8b4", null ]
];