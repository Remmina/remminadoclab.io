/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Remmina - The GTK+ Remote Desktop Client", "index.html", [
    [ "<em>Welcome to the Remmina wiki</em>", "index.html", null ],
    [ "v1.4.34", "md__c_h_a_n_g_e_l_o_g.html", null ],
    [ "CONTRIBUTING", "md__c_o_n_t_r_i_b_u_t_i_n_g.html", null ],
    [ "README", "md__r_e_a_d_m_e.html", null ],
    [ "Table of contents", "md__builds__remmina_remmina_ci__remmina_wiki__sidebar.html", null ],
    [ "Compilation-guide-for-RHEL", "md__builds__remmina_remmina_ci__remmina_wiki__compilation__compilation_guide_for__r_h_e_l.html", null ],
    [ "<!--", "md__builds__remmina_remmina_ci__remmina_wiki__compilation__compilation_guide.html", null ],
    [ "Introduction on how to compile Remmina", "md__builds__remmina_remmina_ci__remmina_wiki__compilation__compilation.html", null ],
    [ "Quick and dirty guide for compiling Remmina on Arch", "md__builds__remmina_remmina_ci__remmina_wiki__compilation__compile_on__arch__linux.html", null ],
    [ "Quick and dirty guide for compiling remmina on Debian 10", "md__builds__remmina_remmina_ci__remmina_wiki__compilation__compile_on__debian_10__buster.html", null ],
    [ "Quick and dirty guide for compiling remmina on Debian 9", "md__builds__remmina_remmina_ci__remmina_wiki__compilation__compile_on__debian_9__stretch.html", null ],
    [ "Compiling Remmina on FreeBSD 11", "md__builds__remmina_remmina_ci__remmina_wiki__compilation__compile_on__free_b_s_d.html", null ],
    [ "Quick and dirty guide for compiling remmina on ubuntu 14.04", "md__builds__remmina_remmina_ci__remmina_wiki__compilation__compile_on__ubuntu_14_04.html", null ],
    [ "Quick and dirty guide for compiling remmina on ubuntu 16.04", "md__builds__remmina_remmina_ci__remmina_wiki__compilation__compile_on__ubuntu_16_04.html", null ],
    [ "Quick and dirty guide for compiling remmina on ubuntu 18.04", "md__builds__remmina_remmina_ci__remmina_wiki__compilation__compile_on__ubuntu_18_04.html", null ],
    [ "Quick and dirty guide for compiling remmina on ubuntu 20.04", "md__builds__remmina_remmina_ci__remmina_wiki__compilation__compile_on__ubuntu_20_04.html", null ],
    [ "Quick and dirty guide for compiling remmina on ubuntu 22.04", "md__builds__remmina_remmina_ci__remmina_wiki__compilation__compile_on__ubuntu_22_04.html", null ],
    [ "Quick and dirty guide for compiling Remmina on Debian 11 Bullseye", "md__builds__remmina_remmina_ci__remmina_wiki__compile_on__debian_11__bullseye.html", null ],
    [ "Contribute-to-the-Remmina-documentation", "md__builds__remmina_remmina_ci__remmina_wiki__contribution__contribute_to_the__remmina_documentation.html", null ],
    [ "Requirements", "md__builds__remmina_remmina_ci__remmina_wiki__contribution__h_o_w_t_o_generate_the_changelog.html", null ],
    [ "Contribution", "md__builds__remmina_remmina_ci__remmina_wiki__contribution.html", null ],
    [ "Development-Notes", "md__builds__remmina_remmina_ci__remmina_wiki__development__development__notes.html", null ],
    [ "Multi monitor support", "md__builds__remmina_remmina_ci__remmina_wiki__development_multi_monitor_support.html", null ],
    [ "Writing Remmina plugins in Python", "md__builds__remmina_remmina_ci__remmina_wiki__development__plugin__development__python__a_p_i.html", null ],
    [ "Entry-Example", "md__builds__remmina_remmina_ci__remmina_wiki__development__plugin__development__python__entry__example.html", null ],
    [ "File-Example", "md__builds__remmina_remmina_ci__remmina_wiki__development__plugin__development__python__file__example.html", null ],
    [ "Preference-Example", "md__builds__remmina_remmina_ci__remmina_wiki__development__plugin__development__python__preference__example.html", null ],
    [ "Example plugin to provide a new protocol", "md__builds__remmina_remmina_ci__remmina_wiki__development__plugin__development__python__protocol__example.html", null ],
    [ "Secret-Example", "md__builds__remmina_remmina_ci__remmina_wiki__development__plugin__development__python__secret__example.html", null ],
    [ "Tool-Example", "md__builds__remmina_remmina_ci__remmina_wiki__development__plugin__development__python__tool__example.html", null ],
    [ "Python", "md__builds__remmina_remmina_ci__remmina_wiki__development__plugin__development__python.html", null ],
    [ "Plugin-Development", "md__builds__remmina_remmina_ci__remmina_wiki__development__plugin__development.html", null ],
    [ "Releasing a new Remmina version", "md__builds__remmina_remmina_ci__remmina_wiki__development__releasing_a_new__remmina_version.html", null ],
    [ "Development", "md__builds__remmina_remmina_ci__remmina_wiki__development.html", null ],
    [ "General info", "md__builds__remmina_remmina_ci__remmina_wiki__gtk_socket_feature_is_not_available_in_a__wayland_session.html", null ],
    [ "How-to-translate-Remmina", "md__builds__remmina_remmina_ci__remmina_wiki__localisation__how_to_translate__remmina.html", null ],
    [ "Update-gettext-messages", "md__builds__remmina_remmina_ci__remmina_wiki__localisation__update_gettext_messages.html", null ],
    [ "Authentication", "md__builds__remmina_remmina_ci__remmina_wiki__problems_and_tweaks__apple__v_n_c_protocol_notes.html", null ],
    [ "Black-screen", "md__builds__remmina_remmina_ci__remmina_wiki__problems_and_tweaks__black_screen.html", null ],
    [ "GTK-versions-on-various-distributions", "md__builds__remmina_remmina_ci__remmina_wiki__problems_and_tweaks__g_t_k_versions_on_various_distributions.html", null ],
    [ "Problems-with-Wayland", "md__builds__remmina_remmina_ci__remmina_wiki__problems_and_tweaks__problems_with__wayland.html", null ],
    [ "Remmina-RDP-and-HiDPI-scaling", "md__builds__remmina_remmina_ci__remmina_wiki__problems_and_tweaks__remmina__r_d_p_and__hi_d_p_i_scaling.html", null ],
    [ "Remmina-VNC-to-Raspbian-Stretch", "md__builds__remmina_remmina_ci__remmina_wiki__problems_and_tweaks__remmina__v_n_c_to__raspbian__stretch.html", null ],
    [ "Unrecognized image format", "md__builds__remmina_remmina_ci__remmina_wiki__problems_and_tweaks__snap__crashing.html", null ],
    [ "Squares-instead-of-text", "md__builds__remmina_remmina_ci__remmina_wiki__problems_and_tweaks__squares_instead_of_text.html", null ],
    [ "Systray menu", "md__builds__remmina_remmina_ci__remmina_wiki__problems_and_tweaks__systray_menu.html", null ],
    [ "Viewing passwords", "md__builds__remmina_remmina_ci__remmina_wiki__problems_and_tweaks__view_and_export_passwords.html", null ],
    [ "How to configure key mapping with the VNC plugin.", "md__builds__remmina_remmina_ci__remmina_wiki__problems_and_tweaks_vnc_key_mapping_configuration.html", null ],
    [ "Problems-and-tweaks", "md__builds__remmina_remmina_ci__remmina_wiki__problems_and_tweaks.html", null ],
    [ "<em>Remmina Config File Options</em>", "md__builds__remmina_remmina_ci__remmina_wiki__remmina__config__file__options.html", null ],
    [ "Preferences", "md__builds__remmina_remmina_ci__remmina_wiki__remmina__preferences.html", null ],
    [ "New-element", "md__builds__remmina_remmina_ci__remmina_wiki__testing__new_element.html", null ],
    [ "Testing under Ubuntu with the remmina-next-daily PPA", "md__builds__remmina_remmina_ci__remmina_wiki__testing__testing_under__ubuntu_with_the_remmina_next_daily__p_p_a.html", null ],
    [ "Connect to multiple profiles from a terminal", "md__builds__remmina_remmina_ci__remmina_wiki__usage__remmina_command_line_examples.html", null ],
    [ "General information", "md__builds__remmina_remmina_ci__remmina_wiki__usage__remmina_debugging.html", null ],
    [ "Using colour schemes in Remmina SSH protocol plugin", "md__builds__remmina_remmina_ci__remmina_wiki__usage__remmina__s_s_h__terminal_colour_schemes.html", null ],
    [ "Remmina-SSH-wizardry", "md__builds__remmina_remmina_ci__remmina_wiki__usage__remmina__s_s_h_wizardry.html", null ],
    [ "Remmina Usage FAQ", "md__builds__remmina_remmina_ci__remmina_wiki__usage__remmina__usage__f_a_q.html", null ],
    [ "Introduction", "md__builds__remmina_remmina_ci__remmina_wiki__usage__remmina__user_s_guide.html", null ],
    [ "Usage", "md__builds__remmina_remmina_ci__remmina_wiki__usage.html", null ],
    [ "Removed plugins", "md_plugins__r_e_a_d_m_e.html", null ],
    [ "Todo List", "todo.html", null ],
    [ "Data Structures", "annotated.html", [
      [ "Data Structures", "annotated.html", "annotated_dup" ],
      [ "Data Structure Index", "classes.html", null ],
      [ "Data Fields", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Variables", "functions_vars.html", "functions_vars" ],
        [ "Enumerator", "functions_eval.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ],
      [ "Globals", "globals.html", [
        [ "All", "globals.html", "globals_dup" ],
        [ "Functions", "globals_func.html", "globals_func" ],
        [ "Variables", "globals_vars.html", "globals_vars" ],
        [ "Typedefs", "globals_type.html", null ],
        [ "Enumerations", "globals_enum.html", null ],
        [ "Enumerator", "globals_eval.html", "globals_eval" ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html",
"md__builds__remmina_remmina_ci__remmina_wiki__compilation__compile_on__debian_10__buster.html",
"python__wrapper__protocol__widget_8c.html#a2ef0af826bce30c53f33e28dfa3d9a2f",
"rcw_8c.html#aaad3184fedef1eccd1f92d8ef43cca44",
"rdp__plugin_8h.html#a051d1d2ab58b0119d656f5036277b7c6a6ed2696cd28620a9d41f501fe22162bc",
"remmina__exec_8h.html#a23616cb0334a45c2b477be6efd45ee0cab9f0b4fd42775c1d2b2311e0b9692885",
"remmina__ftp__client_8c.html#adb5e2ad5fd93f065bdc5c307df813c36",
"remmina__main_8h.html#a10e574723222f5df9db456977e62a093",
"remmina__pref_8c.html#a745bbef1d48a7462f8db997821742395",
"remmina__protocol__widget_8h.html#a9d55210413d6a313478f81cf76ecf1b7",
"remmina__ssh_8h.html#ac2d3f3b6550e4d1fe4caabe1df6af508",
"remmina__widget__pool_8h.html#ab776b24a3b829a1d5dd7974cb8582c05",
"struct___remmina_connection_window.html#a05faadfc5f0372cca2f4552b1882813c",
"struct___remmina_plugin_service.html#a6421cc4cd8c2175d769ba5c24a879517",
"struct___remmina_pref.html#aa8d4c923cb9ca1be451436d1790e8ee5",
"struct___remmina_s_f_t_p_client_class.html#ac7786b1cfca2fcc92c85f10e4939f80d",
"structcurl__msg.html#a54f2eda10acf556f77194f10c716f6f8",
"structrf__context.html#adeee1ea1db2e19b99e4deb80b8a6a960",
"www__plugin_8c.html#af1e5c3cccf8fcfb51c4518462b9b61ea"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';