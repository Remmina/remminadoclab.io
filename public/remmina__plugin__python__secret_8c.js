var remmina__plugin__python__secret_8c =
[
    [ "remmina_plugin_python_create_secret_plugin", "remmina__plugin__python__secret_8c.html#a9f5996d4df4748ee8287c09b4fac6c99", null ],
    [ "remmina_plugin_python_secret_delete_password_wrapper", "remmina__plugin__python__secret_8c.html#aa35f5644620101bcc51bc977fdef6375", null ],
    [ "remmina_plugin_python_secret_get_password_wrapper", "remmina__plugin__python__secret_8c.html#a3a16012b0c59159db5ebf5c5a2ea76a4", null ],
    [ "remmina_plugin_python_secret_init", "remmina__plugin__python__secret_8c.html#a67f1bb637144079f1a497eb69907a0e0", null ],
    [ "remmina_plugin_python_secret_init_wrapper", "remmina__plugin__python__secret_8c.html#acf9cbf62cee9311e7bb1600872085760", null ],
    [ "remmina_plugin_python_secret_is_service_available_wrapper", "remmina__plugin__python__secret_8c.html#a4b62cbc77856c4872f27b21ba4c7a814", null ],
    [ "remmina_plugin_python_secret_store_password_wrapper", "remmina__plugin__python__secret_8c.html#a080bac0730272232e8577d653c423938", null ]
];