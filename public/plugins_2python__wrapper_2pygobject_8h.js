var plugins_2python__wrapper_2pygobject_8h =
[
    [ "_PyGClosure", "struct___py_g_closure.html", "struct___py_g_closure" ],
    [ "PyGObject", "struct_py_g_object.html", "struct_py_g_object" ],
    [ "PyGBoxed", "struct_py_g_boxed.html", "struct_py_g_boxed" ],
    [ "PyGPointer", "struct_py_g_pointer.html", "struct_py_g_pointer" ],
    [ "PyGParamSpec", "struct_py_g_param_spec.html", "struct_py_g_param_spec" ],
    [ "_PyGObject_Functions", "struct___py_g_object___functions.html", "struct___py_g_object___functions" ],
    [ "PyGClassInitFunc", "plugins_2python__wrapper_2pygobject_8h.html#ae7eb0d738416d8c9e517e5315010d98b", null ],
    [ "PyGClosure", "plugins_2python__wrapper_2pygobject_8h.html#a5fd56bd0ab96fa108f13eedd4a32813d", null ],
    [ "PyGFatalExceptionFunc", "plugins_2python__wrapper_2pygobject_8h.html#aa4fb7010e1fe712bd991f6636358af72", null ],
    [ "PyGObjectData", "plugins_2python__wrapper_2pygobject_8h.html#a8373d23668bd78bcb75857e129e49b17", null ],
    [ "PyGThreadBlockFunc", "plugins_2python__wrapper_2pygobject_8h.html#afe8c456f1f21fb47870b39d1cfc6cf7f", null ],
    [ "PyGTypeRegistrationFunction", "plugins_2python__wrapper_2pygobject_8h.html#aa6bee291c198fcfe9815d319f8a021a1", null ],
    [ "PyGObjectFlags", "plugins_2python__wrapper_2pygobject_8h.html#a18eca93aad557c40e8a75b655961b159", [
      [ "PYGOBJECT_USING_TOGGLE_REF", "src_2pygobject_8h.html#a18eca93aad557c40e8a75b655961b159a6ca21e5d81f80ca35b556f92f5ca35cc", null ],
      [ "PYGOBJECT_IS_FLOATING_REF", "src_2pygobject_8h.html#a18eca93aad557c40e8a75b655961b159a9ed2edb74e687da7e752fd9968eece34", null ],
      [ "PYGOBJECT_USING_TOGGLE_REF", "plugins_2python__wrapper_2pygobject_8h.html#a18eca93aad557c40e8a75b655961b159a6ca21e5d81f80ca35b556f92f5ca35cc", null ],
      [ "PYGOBJECT_IS_FLOATING_REF", "plugins_2python__wrapper_2pygobject_8h.html#a18eca93aad557c40e8a75b655961b159a9ed2edb74e687da7e752fd9968eece34", null ]
    ] ],
    [ "pygobject_init", "plugins_2python__wrapper_2pygobject_8h.html#a4710f3c87d09751239a9ea9cd8fd4e15", null ],
    [ "_PyGObject_API", "plugins_2python__wrapper_2pygobject_8h.html#adac0c88b46c6dfc5074c0306128b1488", null ],
    [ "PyClosureExceptionHandler", "plugins_2python__wrapper_2pygobject_8h.html#a92f94cbde200b7b5de58c89f41441a54", null ]
];