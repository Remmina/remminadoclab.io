var struct___dialog_data =
[
    [ "buttons", "struct___dialog_data.html#a3cee753dca9f060cde0638c54e1466c4", null ],
    [ "callbackfunc", "struct___dialog_data.html#a20aaba191919fe9c83f5170ba6a394bd", null ],
    [ "dialog_factory_data", "struct___dialog_data.html#a3354e4dfc0e1702842362187a1c856a7", null ],
    [ "dialog_factory_func", "struct___dialog_data.html#a27aede73b66f5c9d7da7eccbc585f85f", null ],
    [ "flags", "struct___dialog_data.html#a4110c1f36565f5926035c9ed1f060893", null ],
    [ "message", "struct___dialog_data.html#a093903efc52764f5593b546ce8ba5105", null ],
    [ "parent", "struct___dialog_data.html#ae5593c8e0d23accabe87f06449f5aa5a", null ],
    [ "title", "struct___dialog_data.html#af9c5056ae12d845e8c8c30b5e57997d8", null ],
    [ "type", "struct___dialog_data.html#a1c48cea48c8dadf952e6ad684b8f89bb", null ]
];