var remmina__scrolled__viewport_8h =
[
    [ "_RemminaScrolledViewport", "struct___remmina_scrolled_viewport.html", "struct___remmina_scrolled_viewport" ],
    [ "_RemminaScrolledViewportClass", "struct___remmina_scrolled_viewport_class.html", "struct___remmina_scrolled_viewport_class" ],
    [ "RemminaScrolledViewport", "remmina__scrolled__viewport_8h.html#a80e0756cf9b34977ec8fe063edd39fb8", null ],
    [ "RemminaScrolledViewportClass", "remmina__scrolled__viewport_8h.html#a37a31ca7d0f6b71c899ba1741b62e719", null ],
    [ "remmina_scrolled_viewport_get_type", "remmina__scrolled__viewport_8h.html#a233033bbbbeb18a654ca3610ab319312", null ],
    [ "remmina_scrolled_viewport_new", "remmina__scrolled__viewport_8h.html#aadfe00c3960eab12d17bdb47495dd1ee", null ],
    [ "remmina_scrolled_viewport_remove_motion", "remmina__scrolled__viewport_8h.html#a0ba56f429a9c1ec05b093f5f78be692c", null ]
];