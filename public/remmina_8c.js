var remmina_8c =
[
    [ "_gpg_error_to_errno", "remmina_8c.html#a1ecec6e37469e2e927a612747269587c", null ],
    [ "main", "remmina_8c.html#a0ddf1224851353fc92bfbff6f499fa97", null ],
    [ "remmina_on_command_line", "remmina_8c.html#aeb286fc5bd52ab5382ab8cb495dc2914", null ],
    [ "remmina_on_local_cmdline", "remmina_8c.html#a9e8319f901232eb5fada98335217979c", null ],
    [ "remmina_on_startup", "remmina_8c.html#a26d2c20be18ff44f239b756f1d5be01a", null ],
    [ "disablenews", "remmina_8c.html#a0e2a887037f1ac58aa26fae55187601a", null ],
    [ "disablestats", "remmina_8c.html#abc6571db9cf13bad1df41a4bb8802ca9", null ],
    [ "disabletoolbar", "remmina_8c.html#a86d25bcf3c47f758f813a175a18b6ff1", null ],
    [ "disabletrayicon", "remmina_8c.html#af2649a017ee95bd78adf58fda09c3e58", null ],
    [ "extrahardening", "remmina_8c.html#ae729d4262682c8674b8dd0135014810a", null ],
    [ "fullscreen", "remmina_8c.html#ae0e3cdfa06abe7e29f987421869713e1", null ],
    [ "GCRY_THREAD_OPTION_PTHREAD_IMPL", "remmina_8c.html#a3d760300165528383af409d2964d79d2", null ],
    [ "gcrypt_thread_initialized", "remmina_8c.html#a87cf5ad579b1e914ce7ea788cead4104", null ],
    [ "imode", "remmina_8c.html#acae7c019a113637ecd9aec737a067217", null ],
    [ "kioskmode", "remmina_8c.html#ab4a9a67c5372ff07b71d0558679ab7ae", null ],
    [ "remmina_options", "remmina_8c.html#a3837fb7dd2ca88b6f7acaecc0d062ac5", null ]
];