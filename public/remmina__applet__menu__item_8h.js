var remmina__applet__menu__item_8h =
[
    [ "_RemminaAppletMenuItem", "struct___remmina_applet_menu_item.html", "struct___remmina_applet_menu_item" ],
    [ "_RemminaAppletMenuItemClass", "struct___remmina_applet_menu_item_class.html", "struct___remmina_applet_menu_item_class" ],
    [ "RemminaAppletMenuItem", "remmina__applet__menu__item_8h.html#ae7a7c047c3adb3519e27d1b62586af45", null ],
    [ "RemminaAppletMenuItemClass", "remmina__applet__menu__item_8h.html#aa75bce8065204e2dfe0d036b99467e10", null ],
    [ "RemminaAppletMenuItemType", "remmina__applet__menu__item_8h.html#a75e42156d38612a96ae48899316b4727", [
      [ "REMMINA_APPLET_MENU_ITEM_FILE", "remmina__applet__menu__item_8h.html#a75e42156d38612a96ae48899316b4727a9edd77222ca6a7576822feebf0588572", null ],
      [ "REMMINA_APPLET_MENU_ITEM_NEW", "remmina__applet__menu__item_8h.html#a75e42156d38612a96ae48899316b4727a9056a1ad9722374064fe166aa77e69a3", null ],
      [ "REMMINA_APPLET_MENU_ITEM_DISCOVERED", "remmina__applet__menu__item_8h.html#a75e42156d38612a96ae48899316b4727a3a6eab177da4cc0bf8267148c3837bf1", null ]
    ] ],
    [ "remmina_applet_menu_item_compare", "remmina__applet__menu__item_8h.html#a09acb894371400d720c9e4ce20024f08", null ],
    [ "remmina_applet_menu_item_get_type", "remmina__applet__menu__item_8h.html#ad77db14bb39fdbf9422f5f4d6be821ef", null ],
    [ "remmina_applet_menu_item_new", "remmina__applet__menu__item_8h.html#a8518d5abbcb0cf2ff0923e41d55e1a96", null ]
];