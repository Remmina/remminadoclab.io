var remmina__plugin__python__protocol_8c =
[
    [ "remmina_plugin_python_create_protocol_plugin", "remmina__plugin__python__protocol_8c.html#a5a0da2bf9171882bf83fba5cb0e0e51e", null ],
    [ "remmina_plugin_python_protocol_init", "remmina__plugin__python__protocol_8c.html#a51c80290e7edac79d637e33f985bab02", null ],
    [ "remmina_protocol_call_feature_wrapper", "remmina__plugin__python__protocol_8c.html#a2c51e3afd8d05632330497c567e527a1", null ],
    [ "remmina_protocol_close_connection_wrapper", "remmina__plugin__python__protocol_8c.html#a714b3762602a93651f3a1f6c35cc97a8", null ],
    [ "remmina_protocol_get_plugin_screenshot_wrapper", "remmina__plugin__python__protocol_8c.html#a541be6857deef27ac09c2caf7ae57099", null ],
    [ "remmina_protocol_init_wrapper", "remmina__plugin__python__protocol_8c.html#aad6b25cbb7627180e6f3904a43ae7b89", null ],
    [ "remmina_protocol_map_event_wrapper", "remmina__plugin__python__protocol_8c.html#a076f2ca9db40272512ad682a45eda571", null ],
    [ "remmina_protocol_open_connection_wrapper", "remmina__plugin__python__protocol_8c.html#a9ebbb58ee514013f3bbb2ffa60efb630", null ],
    [ "remmina_protocol_query_feature_wrapper", "remmina__plugin__python__protocol_8c.html#a5931951ae96e22e972c4a2a54c28af23", null ],
    [ "remmina_protocol_send_keytrokes_wrapper", "remmina__plugin__python__protocol_8c.html#a162c9587302a7a4788f3c45329f56f0c", null ],
    [ "remmina_protocol_unmap_event_wrapper", "remmina__plugin__python__protocol_8c.html#a4ad3d2c8c8a65d787389b134ab716807", null ]
];