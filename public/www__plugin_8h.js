var www__plugin_8h =
[
    [ "WWWWebViewDocumentType", "www__plugin_8h.html#accff0b8fc090986bc39b32ed358f03c9", [
      [ "WWW_WEB_VIEW_DOCUMENT_HTML", "www__plugin_8h.html#accff0b8fc090986bc39b32ed358f03c9ad8a3ee80054a920b92a193c6e84d7b9c", null ],
      [ "WWW_WEB_VIEW_DOCUMENT_XML", "www__plugin_8h.html#accff0b8fc090986bc39b32ed358f03c9a3312508c6e879d0bdda13baf651bc569", null ],
      [ "WWW_WEB_VIEW_DOCUMENT_IMAGE", "www__plugin_8h.html#accff0b8fc090986bc39b32ed358f03c9a665fa26d2235022f11a2b36b06a885dd", null ],
      [ "WWW_WEB_VIEW_DOCUMENT_OCTET_STREAM", "www__plugin_8h.html#accff0b8fc090986bc39b32ed358f03c9a20be7f0ccef7d70651347889eba498a4", null ],
      [ "WWW_WEB_VIEW_DOCUMENT_OTHER", "www__plugin_8h.html#accff0b8fc090986bc39b32ed358f03c9a94ddb3245efc2e8ee4e5944d3d50055b", null ]
    ] ],
    [ "remmina_plugin_www_decide_nav", "www__plugin_8h.html#ad7744b6ffed0a7250dad8d73a1bbdb32", null ],
    [ "remmina_plugin_www_decide_newwin", "www__plugin_8h.html#a05218b821edab542795439a87ad9d909", null ],
    [ "remmina_plugin_www_decide_resource", "www__plugin_8h.html#aab3ceb1ae095823fcbee98ce2811b374", null ],
    [ "remmina_plugin_www_notify_download", "www__plugin_8h.html#ae91c9382d5bb29416eab7de18c6d6b8d", null ],
    [ "remmina_plugin_www_response_received", "www__plugin_8h.html#a98eddc35144b89bce2e247d5ea49177b", null ],
    [ "remmina_plugin_service", "www__plugin_8h.html#a9493664f6bdafe3f5b593c3e5e1eacc7", null ]
];