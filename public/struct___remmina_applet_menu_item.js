var struct___remmina_applet_menu_item =
[
    [ "filename", "struct___remmina_applet_menu_item.html#aac9b89701606ff0f130f36ec0df55674", null ],
    [ "group", "struct___remmina_applet_menu_item.html#a561b4a6b148ac58ce0fafff8e299fc7e", null ],
    [ "item_type", "struct___remmina_applet_menu_item.html#a770b749c60737542802925cd6f3524fa", null ],
    [ "menu_item", "struct___remmina_applet_menu_item.html#a6d1f49b26c899b6ec61d5d28efe7b46c", null ],
    [ "name", "struct___remmina_applet_menu_item.html#abfc6c501c400171718ddf4a520154e8c", null ],
    [ "protocol", "struct___remmina_applet_menu_item.html#a0c122a469388d5000ce5af4ec29a561e", null ],
    [ "server", "struct___remmina_applet_menu_item.html#ac0c498b70034dfb69bbc4ae3ecb2d9a3", null ],
    [ "ssh_tunnel_enabled", "struct___remmina_applet_menu_item.html#ae8b3b5815f1b5b665129ab1256cb77e3", null ]
];