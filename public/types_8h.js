var types_8h =
[
    [ "_RemminaProtocolFeature", "struct___remmina_protocol_feature.html", "struct___remmina_protocol_feature" ],
    [ "_RemminaPluginScreenshotData", "struct___remmina_plugin_screenshot_data.html", "struct___remmina_plugin_screenshot_data" ],
    [ "_RemminaProtocolSetting", "struct___remmina_protocol_setting.html", "struct___remmina_protocol_setting" ],
    [ "RemminaFile", "types_8h.html#a112d7cc6c755209cabb68cf2a62ad515", null ],
    [ "RemminaPluginScreenshotData", "types_8h.html#a91dea08e189fea64419802752202ac7b", null ],
    [ "RemminaProtocolFeature", "types_8h.html#a0901f7c713611718dba2b7b8c540cde1", null ],
    [ "RemminaProtocolSetting", "types_8h.html#a7075630418af3142af80108f46e5f7d7", null ],
    [ "RemminaProtocolWidget", "types_8h.html#a3fc3247f5a12b842032241d0cdde9cf4", null ],
    [ "RemminaProtocolWidgetClass", "types_8h.html#ae8d4366a0a98ae5f9b20ead637e73ac8", null ],
    [ "RemminaTunnelInitFunc", "types_8h.html#a1ace68e95e55a30fcf8b991f52332501", null ],
    [ "RemminaXPortTunnelInitFunc", "types_8h.html#afae2f546eaf9ef8a816a804e830055bf", null ],
    [ "RemminaAuthpwdType", "types_8h.html#a72194e64fa2cf72405bda49a9ea3e8e5", [
      [ "REMMINA_AUTHPWD_TYPE_PROTOCOL", "types_8h.html#a72194e64fa2cf72405bda49a9ea3e8e5aa2fd1ccd641986130bf9333b760d7ff2", null ],
      [ "REMMINA_AUTHPWD_TYPE_SSH_PWD", "types_8h.html#a72194e64fa2cf72405bda49a9ea3e8e5a0f06852a7bb0c4d6611a5eb259b1130d", null ],
      [ "REMMINA_AUTHPWD_TYPE_SSH_PRIVKEY", "types_8h.html#a72194e64fa2cf72405bda49a9ea3e8e5a28a07360db0262bf2b4777bc5a36c920", null ]
    ] ],
    [ "RemminaMessagePanelFlags", "types_8h.html#a341a0a8b6509633809d99348cc3ba486", [
      [ "REMMINA_MESSAGE_PANEL_FLAG_USERNAME", "types_8h.html#a341a0a8b6509633809d99348cc3ba486a10be0730fc3ea326856019ae139a922a", null ],
      [ "REMMINA_MESSAGE_PANEL_FLAG_USERNAME_READONLY", "types_8h.html#a341a0a8b6509633809d99348cc3ba486a1f63bf86b8f6da71137cf0a28ebf024a", null ],
      [ "REMMINA_MESSAGE_PANEL_FLAG_DOMAIN", "types_8h.html#a341a0a8b6509633809d99348cc3ba486a46ac68f2dea71082d090ae63c5c4505c", null ],
      [ "REMMINA_MESSAGE_PANEL_FLAG_SAVEPASSWORD", "types_8h.html#a341a0a8b6509633809d99348cc3ba486a9b734c324722cf2982768feaeeb15c7c", null ]
    ] ],
    [ "RemminaProtocolFeatureType", "types_8h.html#a77ae46c4fd739dcd8e819fe9cdae27c5", [
      [ "REMMINA_PROTOCOL_FEATURE_TYPE_END", "types_8h.html#a77ae46c4fd739dcd8e819fe9cdae27c5a311d2a8dae8d3d7b9045b4b704f94fe4", null ],
      [ "REMMINA_PROTOCOL_FEATURE_TYPE_PREF", "types_8h.html#a77ae46c4fd739dcd8e819fe9cdae27c5a26774624e955f451f7e4270c36622527", null ],
      [ "REMMINA_PROTOCOL_FEATURE_TYPE_TOOL", "types_8h.html#a77ae46c4fd739dcd8e819fe9cdae27c5a645d9a32b55325b3672e265a64d0a507", null ],
      [ "REMMINA_PROTOCOL_FEATURE_TYPE_UNFOCUS", "types_8h.html#a77ae46c4fd739dcd8e819fe9cdae27c5a0658dd6adcfaf5cb52d6d99c97c352d1", null ],
      [ "REMMINA_PROTOCOL_FEATURE_TYPE_SCALE", "types_8h.html#a77ae46c4fd739dcd8e819fe9cdae27c5affe302f5d3151bfe99607edf308c3c9f", null ],
      [ "REMMINA_PROTOCOL_FEATURE_TYPE_DYNRESUPDATE", "types_8h.html#a77ae46c4fd739dcd8e819fe9cdae27c5a23e854b5f2eb0c8d3efa54cac0acb6cf", null ],
      [ "REMMINA_PROTOCOL_FEATURE_TYPE_MULTIMON", "types_8h.html#a77ae46c4fd739dcd8e819fe9cdae27c5af727b997d174217f949eb7364704f501", null ],
      [ "REMMINA_PROTOCOL_FEATURE_TYPE_GTKSOCKET", "types_8h.html#a77ae46c4fd739dcd8e819fe9cdae27c5a4a9b04df122e3a20c3ae51d90280fea7", null ]
    ] ],
    [ "RemminaProtocolSettingType", "types_8h.html#aee1a3e130e6991978acd907b52204350", [
      [ "REMMINA_PROTOCOL_SETTING_TYPE_END", "types_8h.html#aee1a3e130e6991978acd907b52204350ad898beb6ae625dee1ae27de75f1ab4e3", null ],
      [ "REMMINA_PROTOCOL_SETTING_TYPE_SERVER", "types_8h.html#aee1a3e130e6991978acd907b52204350a579ed0f0f6624dd55a25665693c92a92", null ],
      [ "REMMINA_PROTOCOL_SETTING_TYPE_PASSWORD", "types_8h.html#aee1a3e130e6991978acd907b52204350ae76d06b6e313971e5f420dd6e7f4c5aa", null ],
      [ "REMMINA_PROTOCOL_SETTING_TYPE_RESOLUTION", "types_8h.html#aee1a3e130e6991978acd907b52204350aa045109309280606ff11a9de1862e2ba", null ],
      [ "REMMINA_PROTOCOL_SETTING_TYPE_ASSISTANCE", "types_8h.html#aee1a3e130e6991978acd907b52204350a16a83c00dba8d929cce531d5a0181a2d", null ],
      [ "REMMINA_PROTOCOL_SETTING_TYPE_KEYMAP", "types_8h.html#aee1a3e130e6991978acd907b52204350ad9418344a7f597351e3cc5639be78425", null ],
      [ "REMMINA_PROTOCOL_SETTING_TYPE_TEXT", "types_8h.html#aee1a3e130e6991978acd907b52204350a8be3f210c609fde31f68e607c41de296", null ],
      [ "REMMINA_PROTOCOL_SETTING_TYPE_TEXTAREA", "types_8h.html#aee1a3e130e6991978acd907b52204350aadb70e5d52a6eccc1aaf6f1428c1fb11", null ],
      [ "REMMINA_PROTOCOL_SETTING_TYPE_SELECT", "types_8h.html#aee1a3e130e6991978acd907b52204350a04da01eead7904a55d1c813950609b2d", null ],
      [ "REMMINA_PROTOCOL_SETTING_TYPE_COMBO", "types_8h.html#aee1a3e130e6991978acd907b52204350ae2237632b23d3c90a842bc5bc8302ec5", null ],
      [ "REMMINA_PROTOCOL_SETTING_TYPE_CHECK", "types_8h.html#aee1a3e130e6991978acd907b52204350a52a80f3f9b84e9e0d0bf3335cfd7527f", null ],
      [ "REMMINA_PROTOCOL_SETTING_TYPE_FILE", "types_8h.html#aee1a3e130e6991978acd907b52204350a31ee66fffaa245ae411aba0c296ad9da", null ],
      [ "REMMINA_PROTOCOL_SETTING_TYPE_FOLDER", "types_8h.html#aee1a3e130e6991978acd907b52204350a68b440d346168aadb6fd728ea394607f", null ],
      [ "REMMINA_PROTOCOL_SETTING_TYPE_INT", "types_8h.html#aee1a3e130e6991978acd907b52204350ab4c68b7df42ebd08e953cc72c5b97583", null ],
      [ "REMMINA_PROTOCOL_SETTING_TYPE_DOUBLE", "types_8h.html#aee1a3e130e6991978acd907b52204350a25c4af05ecfc7053db40ed0997389b91", null ]
    ] ],
    [ "RemminaProtocolSSHSetting", "types_8h.html#a64c62354cc2b7baa50e1d73dcd3df9c7", [
      [ "REMMINA_PROTOCOL_SSH_SETTING_NONE", "types_8h.html#a64c62354cc2b7baa50e1d73dcd3df9c7a8f910597287a73453a5f09ca0037d093", null ],
      [ "REMMINA_PROTOCOL_SSH_SETTING_TUNNEL", "types_8h.html#a64c62354cc2b7baa50e1d73dcd3df9c7ab0449f571d03e46b99b301792a5d8e1a", null ],
      [ "REMMINA_PROTOCOL_SSH_SETTING_SSH", "types_8h.html#a64c62354cc2b7baa50e1d73dcd3df9c7a113aff9a0339ebe347ddf738d8c1f371", null ],
      [ "REMMINA_PROTOCOL_SSH_SETTING_REVERSE_TUNNEL", "types_8h.html#a64c62354cc2b7baa50e1d73dcd3df9c7a2f71ac2987eeec94430308f6f36840cd", null ],
      [ "REMMINA_PROTOCOL_SSH_SETTING_SFTP", "types_8h.html#a64c62354cc2b7baa50e1d73dcd3df9c7aef0536e2d0186b8cb8d8ba7a4a1a51c3", null ]
    ] ],
    [ "RemminaProtocolWidgetResolutionMode", "types_8h.html#a373ddef8221457d86c64830c26d3f78a", [
      [ "RES_INVALID", "types_8h.html#a373ddef8221457d86c64830c26d3f78aa415945752acf7689df7cb602ae4e1724", null ],
      [ "RES_USE_CUSTOM", "types_8h.html#a373ddef8221457d86c64830c26d3f78aafe10effd553a16b9bac60b6d9b64c2cd", null ],
      [ "RES_USE_CLIENT", "types_8h.html#a373ddef8221457d86c64830c26d3f78aaf5ff7e2cd480eb2acc5e1520e409202d", null ],
      [ "RES_USE_INITIAL_WINDOW_SIZE", "types_8h.html#a373ddef8221457d86c64830c26d3f78aa60de6896fa451ca072d23fab21dbfeef", null ]
    ] ],
    [ "RemminaScaleMode", "types_8h.html#aeea5ee1e675368413b6f6ab09883e7de", [
      [ "REMMINA_PROTOCOL_WIDGET_SCALE_MODE_NONE", "types_8h.html#aeea5ee1e675368413b6f6ab09883e7dead50ff64ad1e14caf78956850f03871bd", null ],
      [ "REMMINA_PROTOCOL_WIDGET_SCALE_MODE_SCALED", "types_8h.html#aeea5ee1e675368413b6f6ab09883e7deac7786e2cea7d9bf95026069ed774ea20", null ],
      [ "REMMINA_PROTOCOL_WIDGET_SCALE_MODE_DYNRES", "types_8h.html#aeea5ee1e675368413b6f6ab09883e7dea4d22c6eed721b661d618098159a20946", null ]
    ] ],
    [ "RemminaTypeHint", "types_8h.html#ab0b29804e77812ca42752bf4f379a288", [
      [ "REMMINA_TYPEHINT_STRING", "types_8h.html#ab0b29804e77812ca42752bf4f379a288a0f7883d48c9106883a2b7c9c021ebdcd", null ],
      [ "REMMINA_TYPEHINT_SIGNED", "types_8h.html#ab0b29804e77812ca42752bf4f379a288af7ab6943dcd5d24c6f19733da16d0f18", null ],
      [ "REMMINA_TYPEHINT_UNSIGNED", "types_8h.html#ab0b29804e77812ca42752bf4f379a288a50903e485f8d4a2c2f98d7dfe4be8047", null ],
      [ "REMMINA_TYPEHINT_BOOLEAN", "types_8h.html#ab0b29804e77812ca42752bf4f379a288a136d17c74813391126815f7c08d6697e", null ],
      [ "REMMINA_TYPEHINT_CPOINTER", "types_8h.html#ab0b29804e77812ca42752bf4f379a288a46086c0047e2ca3b353bc6f0944fecd4", null ],
      [ "REMMINA_TYPEHINT_RAW", "types_8h.html#ab0b29804e77812ca42752bf4f379a288a80694dd04b4a5f6b2c3af24e71851ce3", null ],
      [ "REMMINA_TYPEHINT_TUPLE", "types_8h.html#ab0b29804e77812ca42752bf4f379a288a65727cbc0caeb3702cfa98f6c57d9718", null ],
      [ "REMMINA_TYPEHINT_UNDEFINED", "types_8h.html#ab0b29804e77812ca42752bf4f379a288ab774d380798d305b72f4301143d3f440", null ]
    ] ]
];