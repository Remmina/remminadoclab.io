var remmina__sftp__client_8h =
[
    [ "_RemminaSFTPClient", "struct___remmina_s_f_t_p_client.html", "struct___remmina_s_f_t_p_client" ],
    [ "_RemminaSFTPClientClass", "struct___remmina_s_f_t_p_client_class.html", "struct___remmina_s_f_t_p_client_class" ],
    [ "RemminaSFTPClient", "remmina__sftp__client_8h.html#aa9ea566d4443fab51ae1e3b9b30993a3", null ],
    [ "RemminaSFTPClientClass", "remmina__sftp__client_8h.html#a18f294b8a3ac7f9c804a8c7f4e2b8bd4", null ],
    [ "remmina_sftp_client_confirm_resume", "remmina__sftp__client_8h.html#a138904893cb9c7f6535b8dfc7ba17e33", null ],
    [ "remmina_sftp_client_get_type", "remmina__sftp__client_8h.html#acc0b1ad0ee4ba202095b0ec1e961555d", null ],
    [ "remmina_sftp_client_new", "remmina__sftp__client_8h.html#a0a3d678ad99a04a8b6aa3d958d60dc7d", null ],
    [ "remmina_sftp_client_open", "remmina__sftp__client_8h.html#a9fb6d25e4676b2fa574f958a5ba271a1", null ]
];