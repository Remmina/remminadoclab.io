var plugin_8h =
[
    [ "_RemminaPlugin", "struct___remmina_plugin.html", "struct___remmina_plugin" ],
    [ "_RemminaServerPluginResponse", "struct___remmina_server_plugin_response.html", "struct___remmina_server_plugin_response" ],
    [ "_RemminaProtocolPlugin", "struct___remmina_protocol_plugin.html", "struct___remmina_protocol_plugin" ],
    [ "_RemminaEntryPlugin", "struct___remmina_entry_plugin.html", "struct___remmina_entry_plugin" ],
    [ "_RemminaFilePlugin", "struct___remmina_file_plugin.html", "struct___remmina_file_plugin" ],
    [ "_RemminaToolPlugin", "struct___remmina_tool_plugin.html", "struct___remmina_tool_plugin" ],
    [ "_RemminaPrefPlugin", "struct___remmina_pref_plugin.html", "struct___remmina_pref_plugin" ],
    [ "_RemminaSecretPlugin", "struct___remmina_secret_plugin.html", "struct___remmina_secret_plugin" ],
    [ "_RemminaLanguageWrapperPlugin", "struct___remmina_language_wrapper_plugin.html", "struct___remmina_language_wrapper_plugin" ],
    [ "_RemminaPluginService", "struct___remmina_plugin_service.html", "struct___remmina_plugin_service" ],
    [ "_RemminaProtocolPlugin", "plugin_8h.html#acbccbfd421baf8ea0137c3930370bed6", null ],
    [ "RemminaEntryPlugin", "plugin_8h.html#a9eea8e0359540de545e954f157166110", null ],
    [ "RemminaFilePlugin", "plugin_8h.html#ae721019b1e5988310ccca7d5ff4ff4cc", null ],
    [ "RemminaLanguageWrapperPlugin", "plugin_8h.html#adf74ebaf2ed91455472c344ba9d18d01", null ],
    [ "RemminaPlugin", "plugin_8h.html#afbcbd9f627662ac6ef9dcfcc067f1cfa", null ],
    [ "RemminaPluginEntryFunc", "plugin_8h.html#a59fee7dbde41c8d29059ca64622d1948", null ],
    [ "RemminaPluginService", "plugin_8h.html#aee2c53570ba43317f1b773d37ef1eb47", null ],
    [ "RemminaPrefPlugin", "plugin_8h.html#a50643ce6e515c45975faccbd77bc3643", null ],
    [ "RemminaProtocolPlugin", "plugin_8h.html#a2dbd7c9006318dd027d5a5f0b4f59f26", null ],
    [ "RemminaSecretPlugin", "plugin_8h.html#aa1c8923af661bf0c5bb4b0fb979ed640", null ],
    [ "RemminaServerPluginResponse", "plugin_8h.html#a841bb0276fbf2b37ef9a718bfbf225f3", null ],
    [ "RemminaToolPlugin", "plugin_8h.html#aca07196a077cf5562a9001d8a57178b7", null ],
    [ "RemminaPluginType", "plugin_8h.html#aaf53c620d115a4642130227cff4a624a", [
      [ "REMMINA_PLUGIN_TYPE_PROTOCOL", "plugin_8h.html#aaf53c620d115a4642130227cff4a624aa45e094c248384c12cdafd9d088e4aa24", null ],
      [ "REMMINA_PLUGIN_TYPE_ENTRY", "plugin_8h.html#aaf53c620d115a4642130227cff4a624aae2648710d57bdd48a15a95e9c4cc234e", null ],
      [ "REMMINA_PLUGIN_TYPE_FILE", "plugin_8h.html#aaf53c620d115a4642130227cff4a624aa9af260da7aa2ad0ee90e8dae5439dbac", null ],
      [ "REMMINA_PLUGIN_TYPE_TOOL", "plugin_8h.html#aaf53c620d115a4642130227cff4a624aadd037352a609e2dc890595f9d95bd1ac", null ],
      [ "REMMINA_PLUGIN_TYPE_PREF", "plugin_8h.html#aaf53c620d115a4642130227cff4a624aa682ab42b3f4af3dd93452efdb76b8757", null ],
      [ "REMMINA_PLUGIN_TYPE_SECRET", "plugin_8h.html#aaf53c620d115a4642130227cff4a624aa0d17e841a53921a7a6156f5d30e9f09c", null ],
      [ "REMMINA_PLUGIN_TYPE_LANGUAGE_WRAPPER", "plugin_8h.html#aaf53c620d115a4642130227cff4a624aa93a45ba624f83d17a6d8619933a54b3e", null ]
    ] ]
];