var python__wrapper__protocol_8c =
[
    [ "python_wrapper_create_protocol_plugin", "python__wrapper__protocol_8c.html#aaa90f5541895f5c3dedaa16c15d80de7", null ],
    [ "python_wrapper_protocol_init", "python__wrapper__protocol_8c.html#ab223d3563d16258e33e8dfae0ee41e5d", null ],
    [ "remmina_protocol_call_feature_wrapper", "python__wrapper__protocol_8c.html#a2c51e3afd8d05632330497c567e527a1", null ],
    [ "remmina_protocol_close_connection_wrapper", "python__wrapper__protocol_8c.html#a714b3762602a93651f3a1f6c35cc97a8", null ],
    [ "remmina_protocol_get_plugin_screenshot_wrapper", "python__wrapper__protocol_8c.html#a541be6857deef27ac09c2caf7ae57099", null ],
    [ "remmina_protocol_init_wrapper", "python__wrapper__protocol_8c.html#aad6b25cbb7627180e6f3904a43ae7b89", null ],
    [ "remmina_protocol_map_event_wrapper", "python__wrapper__protocol_8c.html#a076f2ca9db40272512ad682a45eda571", null ],
    [ "remmina_protocol_open_connection_wrapper", "python__wrapper__protocol_8c.html#a9ebbb58ee514013f3bbb2ffa60efb630", null ],
    [ "remmina_protocol_query_feature_wrapper", "python__wrapper__protocol_8c.html#a5931951ae96e22e972c4a2a54c28af23", null ],
    [ "remmina_protocol_send_keytrokes_wrapper", "python__wrapper__protocol_8c.html#a162c9587302a7a4788f3c45329f56f0c", null ],
    [ "remmina_protocol_unmap_event_wrapper", "python__wrapper__protocol_8c.html#a4ad3d2c8c8a65d787389b134ab716807", null ]
];