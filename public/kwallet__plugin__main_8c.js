var kwallet__plugin__main_8c =
[
    [ "build_kwallet_key", "kwallet__plugin__main_8c.html#a864de43408740eb3b6ef58c820773e2a", null ],
    [ "remmina_plugin_entry", "kwallet__plugin__main_8c.html#a56e5020de11446dbc23f3480e313f208", null ],
    [ "remmina_plugin_kwallet_delete_password", "kwallet__plugin__main_8c.html#a83bdf5934c5337fc04d62b2de17df241", null ],
    [ "remmina_plugin_kwallet_get_password", "kwallet__plugin__main_8c.html#afdb8e800165c9d92fc386d6df2d54f94", null ],
    [ "remmina_plugin_kwallet_init", "kwallet__plugin__main_8c.html#a72cef27408cc093654b7646ece53a571", null ],
    [ "remmina_plugin_kwallet_is_service_available", "kwallet__plugin__main_8c.html#ad7fda02861e62116380fbff6b4115ac9", null ],
    [ "remmina_plugin_kwallet_store_password", "kwallet__plugin__main_8c.html#a1c9f5d83ff90e4a36768cd2e146b3783", null ],
    [ "remmina_plugin_kwallet", "kwallet__plugin__main_8c.html#af31524d2a9ee0747edc915947283b469", null ],
    [ "remmina_plugin_service", "kwallet__plugin__main_8c.html#a9493664f6bdafe3f5b593c3e5e1eacc7", null ]
];