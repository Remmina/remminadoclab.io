var struct___remmina_language_wrapper_plugin =
[
    [ "description", "struct___remmina_language_wrapper_plugin.html#af62b3426fe2b8ba6ab7a0d62cd0e8204", null ],
    [ "domain", "struct___remmina_language_wrapper_plugin.html#a122aa4942c59646f92c18214cbb6cff0", null ],
    [ "init", "struct___remmina_language_wrapper_plugin.html#ae7b05207fb8e60cb02be30997c309e28", null ],
    [ "load", "struct___remmina_language_wrapper_plugin.html#a1270e29b6e988d6b320ef3f0465b6272", null ],
    [ "name", "struct___remmina_language_wrapper_plugin.html#a30ebbfe51f50274ee613d64455a468b4", null ],
    [ "supported_extentions", "struct___remmina_language_wrapper_plugin.html#aea49b9e057254020e13f615331ef239b", null ],
    [ "type", "struct___remmina_language_wrapper_plugin.html#ade9917bdf8e926b342900280b984fded", null ],
    [ "version", "struct___remmina_language_wrapper_plugin.html#a6029aee6c0f7b30a182f6797145ab096", null ]
];