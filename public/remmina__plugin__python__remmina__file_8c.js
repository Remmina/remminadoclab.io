var remmina__plugin__python__remmina__file_8c =
[
    [ "file_dealloc", "remmina__plugin__python__remmina__file_8c.html#a8fd4a10a550a8d5dc690a90e7418d681", null ],
    [ "file_get_path", "remmina__plugin__python__remmina__file_8c.html#a65c614921391fcc29c4c59790a24da54", null ],
    [ "file_get_secret", "remmina__plugin__python__remmina__file_8c.html#a87a581308279a06a69b7f113077b575b", null ],
    [ "file_get_setting", "remmina__plugin__python__remmina__file_8c.html#add1d056f0ded3f5dc9557dd7112a4f19", null ],
    [ "file_set_setting", "remmina__plugin__python__remmina__file_8c.html#a96395d2137130b51e9530d4aa99501ed", null ],
    [ "file_unsave_passwords", "remmina__plugin__python__remmina__file_8c.html#ab4229f3335c43efd45356880fee4d0c7", null ],
    [ "remmina_plugin_python_remmina_file_to_python", "remmina__plugin__python__remmina__file_8c.html#adec44b3ef868e3bd88e780f3e5cbe44d", null ],
    [ "remmina_plugin_python_remmina_init_types", "remmina__plugin__python__remmina__file_8c.html#adfb6b8d872190dada22b0757a1ad678c", null ],
    [ "python_remmina_file_type", "remmina__plugin__python__remmina__file_8c.html#aab1773c1a8186f0827dede3fa430ea12", null ],
    [ "python_remmina_file_type_methods", "remmina__plugin__python__remmina__file_8c.html#ab66f0239baad771626c734253bb7b402", null ]
];