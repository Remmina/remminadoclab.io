var remmina__masterthread__exec_8h =
[
    [ "remmina_masterthread_exec_data", "structremmina__masterthread__exec__data.html", "structremmina__masterthread__exec__data" ],
    [ "RemminaMTExecData", "remmina__masterthread__exec_8h.html#a0d17e0671b899b5621020bc29125b6a7", null ],
    [ "remmina_masterthread_exec_and_wait", "remmina__masterthread__exec_8h.html#a14628c2470cac50e87916a70ea05c97d", null ],
    [ "remmina_masterthread_exec_is_main_thread", "remmina__masterthread__exec_8h.html#a7b90b9539fd1bffa1fdace50f9caf29c", null ],
    [ "remmina_masterthread_exec_save_main_thread_id", "remmina__masterthread__exec_8h.html#a3d6fe9efdc6b84c16d99809003cfc850", null ]
];