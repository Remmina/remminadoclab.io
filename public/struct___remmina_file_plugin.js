var struct___remmina_file_plugin =
[
    [ "description", "struct___remmina_file_plugin.html#a36c131a8e8c0180950faa80f497f23c1", null ],
    [ "domain", "struct___remmina_file_plugin.html#a1de86485b3c4db138b0fcd264884483f", null ],
    [ "export_func", "struct___remmina_file_plugin.html#a8bf634ff09caba624ecf93c42e12af78", null ],
    [ "export_hints", "struct___remmina_file_plugin.html#ac4f2b0118490cede0577aaee9f2e0b5f", null ],
    [ "export_test_func", "struct___remmina_file_plugin.html#a0a3d5cd1b41d206d01ba52ab5ea554b6", null ],
    [ "import_func", "struct___remmina_file_plugin.html#a597178a88c755ebe0614897b7aa279b1", null ],
    [ "import_test_func", "struct___remmina_file_plugin.html#a5cbc050a2735ee5d896a110a25e91ca9", null ],
    [ "name", "struct___remmina_file_plugin.html#a8ef191449f269aec279e9097cadb7ee6", null ],
    [ "type", "struct___remmina_file_plugin.html#a3ff85e56bf902817630aafea67e3e494", null ],
    [ "version", "struct___remmina_file_plugin.html#a57bf1f4e71c86165cbac2563309e35b8", null ]
];