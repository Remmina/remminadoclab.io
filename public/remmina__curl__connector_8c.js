var remmina__curl__connector_8c =
[
    [ "curl_msg", "structcurl__msg.html", "structcurl__msg" ],
    [ "ShowValue", "remmina__curl__connector_8c.html#abf33c3f2811f8f11e305782917e7f7d2", [
      [ "ShowNews", "remmina__curl__connector_8c.html#abf33c3f2811f8f11e305782917e7f7d2a7f464bcebe497dae601c227e05e4436e", null ],
      [ "ShowTip", "remmina__curl__connector_8c.html#abf33c3f2811f8f11e305782917e7f7d2a73112275afea8a9151a9e7b23676fe4e", null ],
      [ "ShowNone", "remmina__curl__connector_8c.html#abf33c3f2811f8f11e305782917e7f7d2adcf1843c7c43a1405166db01f9fedb51", null ]
    ] ],
    [ "handle_resp", "remmina__curl__connector_8c.html#aa6d9f6b65939e65c0f4106076efa2e55", null ],
    [ "remmina_curl_compose_message", "remmina__curl__connector_8c.html#ada22f210bf08b2e1af7d88687390bd0a", null ],
    [ "remmina_curl_process_response", "remmina__curl__connector_8c.html#a493d32a5cc65ecc2be203888caa42019", null ],
    [ "remmina_curl_send_message", "remmina__curl__connector_8c.html#a34f8d2e9b0144113f826142d3add1b97", null ],
    [ "info_disable_news", "remmina__curl__connector_8c.html#a89958e11377c2a123a326d616f4817a5", null ],
    [ "info_disable_stats", "remmina__curl__connector_8c.html#a9ff8cbcfefe4a784e797ba29d3ebfeef", null ],
    [ "info_disable_tip", "remmina__curl__connector_8c.html#a7f0a96fbbe30b2fb6f5d65d17ca11c15", null ]
];