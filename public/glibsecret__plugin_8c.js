var glibsecret__plugin_8c =
[
    [ "remmina_plugin_entry", "glibsecret__plugin_8c.html#a56e5020de11446dbc23f3480e313f208", null ],
    [ "remmina_plugin_glibsecret_delete_password", "glibsecret__plugin_8c.html#a42b917ccaef5cf81d569063b85cf1060", null ],
    [ "remmina_plugin_glibsecret_get_password", "glibsecret__plugin_8c.html#a0b98d0f6403f96364cb511286ae15983", null ],
    [ "remmina_plugin_glibsecret_init", "glibsecret__plugin_8c.html#aeb734bc33fdb93eb8dd42fa5d72783d0", null ],
    [ "remmina_plugin_glibsecret_is_service_available", "glibsecret__plugin_8c.html#a032cf783610084ff842e398fc0f11b0a", null ],
    [ "remmina_plugin_glibsecret_store_password", "glibsecret__plugin_8c.html#a60261babe631f4883ef3032c3b224c1c", null ],
    [ "remmina_plugin_glibsecret_unlock_secret_service", "glibsecret__plugin_8c.html#a12ac811821984f8b0f3ca16f38ecb642", null ],
    [ "defaultcollection", "glibsecret__plugin_8c.html#af10df16880b8942af051968748935d72", null ],
    [ "remmina_file_secret_schema", "glibsecret__plugin_8c.html#a4adccd38b5ef09344624c0ba2ac7046f", null ],
    [ "remmina_plugin_glibsecret", "glibsecret__plugin_8c.html#a1b5617378d3f4d61a5150cff19d551dd", null ],
    [ "remmina_plugin_service", "glibsecret__plugin_8c.html#a9493664f6bdafe3f5b593c3e5e1eacc7", null ],
    [ "secretservice", "glibsecret__plugin_8c.html#a2bc831bbd1013a039ff4b31a340b3ec7", null ]
];